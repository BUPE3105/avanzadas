package com.uady.sapra.sapradesign;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements  AdapterView.OnItemClickListener {


    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQ_ENABLE_BT  =   10;
    private static final int BT_BOUNDED     =   21;
    private static final int BT_SEARCH      =   22;
    private static final int REQUEST_CODE_LOC = 1 ;

    private static final int STATE_CHANGE       = 30;
    private static final int OPEN_GATE_ONE      = 31;
    private static final int OPEN_GATE_TWO      = 32;
    private static final int OPEN_GATE_THREE      = 33;
    private static final int STATE_SENSOR2 = 34;
    private static final int STATE_SENSOR         = 35;
    private static final int CONTROL_SYSTEM       = 36;




    ImageView home_iv_sapra;
    ImageView home_iv_separator;
    TextView home_txt_welcome;


    Button conectButton;


    private StringBuffer sbConsole;
    private StringBuffer sbConsole2;
    private ImageButton updateBt;
    private String var;
    private String var2;
    private TextView textSensor1;
    private  TextView textCheat;
    private TextView textSensor2;


    private ToggleButton evA;
    private ToggleButton evB;
    private ToggleButton evC;
    private ToggleButton evE;

    private TextView txt_nivel;


    private BottomNavigationView bottomNav;

    private ListView listBtDevices;
    private BluetoothAdapter bluetoothAdapter;
    private BtlistAdapter listAdapter;
    private ArrayList<BluetoothDevice> bluetoothDevices;
    private ProgressDialog progressDialog;

    private String command;
    private BufferedInputStream bis;



    private  ProgressBar progressBarHumedadSensor1;
    private  ProgressBar progressBarHumedadSensor2;




    private ConnectThread connectThread;
    private ConnectedThread connectedThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        bottomNav = findViewById(R.id.bottom_navigation);


        home_iv_sapra = findViewById(R.id.iv_sapraicon);
        home_txt_welcome = findViewById(R.id.txt_welcome);
        home_iv_separator = findViewById(R.id.separator_line);
        conectButton = findViewById(R.id.bt_connect);





        textSensor1 = findViewById(R.id.data_text);
        updateBt = findViewById(R.id.update_data);
        textSensor2 = findViewById(R.id.data_text2);
        textCheat = findViewById(R.id.text_cheat);

        progressBarHumedadSensor1=findViewById(R.id.staus_pb_data);
        progressBarHumedadSensor2 = findViewById(R.id.staus_pb_data2);


        evA = findViewById(R.id.state_evA);
        evB = findViewById(R.id.state_evB);
        evC = findViewById(R.id.state_evC);
        evE = findViewById(R.id.state_eve);

        txt_nivel = findViewById(R.id.txt_nivel);


        bottomNav.setOnNavigationItemSelectedListener(navListener);


        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        bluetoothDevices =  new ArrayList<>();




        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        registerReceiver(receiver, filter);

        /*Observa el estado de la conexion de bluetooth
         * */

        if (bluetoothAdapter == null) {
            Toast.makeText(this, getString(R.string.bluetooth_not_supported), Toast.LENGTH_SHORT).show();
            Log.d(TAG, "onCreate: " + getString(R.string.bluetooth_not_supported));
            finish();
        }

        if (bluetoothAdapter.isEnabled()) {
            setListAdapter(BT_BOUNDED);
        }


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(receiver);

        if(connectThread != null){
            connectThread.cancel();
        }

        if (connectedThread != null){
            connectedThread.cancel();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        bluetoothAdapter.disable();
    }





    public void ConectBt(View view){

    enableBt(true);
    setListAdapter(BT_BOUNDED);
    conectButton.setText("Desconectar");



    }



    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    @Nullable Intent data) {
        if (requestCode == REQ_ENABLE_BT) {
            if (resultCode == RESULT_OK && bluetoothAdapter.isEnabled()) {
                setListAdapter(BT_BOUNDED);
            } else if (resultCode == RESULT_CANCELED) {
                enableBt(true);
            }
        }
    }

    public void UpdateBt(View view){


        readBtData(STATE_SENSOR);
        textCheat.setText(var);
        var2 = textCheat.getText().toString();
        if (!var2.isEmpty()){
            String val[] = var2.split("");
            int n = Integer.parseInt(val[1]);
            int n1 = Integer.parseInt(val[2]);
            int n2 = Integer.parseInt(val[3]);

            int total = (n*100)+ (n1*10) + n2;

            textSensor1.setText(String.valueOf(total) + " %");
            progressBarHumedadSensor1.setProgress(total);
            int m = Integer.parseInt(val[4]);
            int m1 = Integer.parseInt(val[5]);
            int m2 = Integer.parseInt(val[6]);

            int total2 = (m*100)+ (m1*10) + m2;

            textSensor2.setText(String.valueOf(total2)+" %");
            progressBarHumedadSensor2.setProgress(total2);

            int level = Integer.valueOf(val[7]);

            if(level == 1){

                txt_nivel.setText("Alto");
            }

            else if(level == 2){
                txt_nivel.setText("Medio");

            }

            else if(level == 3){
                txt_nivel.setText("Bajo");

            }

            else if(level == 4){
                txt_nivel.setText("Vacio");

            }

            else if(level == 5){
                txt_nivel.setText("Problema con el tanque");

            }


            int a = Integer.valueOf(val[8]);

            if( a == 1){
                evA.setChecked(true);
            }

            else
                evA.setChecked(false);

            int b = Integer.valueOf(val[9]);

            if( b == 1){
                evB.setChecked(true);
            }

            else
                evB.setChecked(false);

            int c = Integer.valueOf(val[10]);

            if( c == 1){
                evC.setChecked(true);
            }

            else
                evC.setChecked(false);

            int e = Integer.valueOf(val[11]);

            if( e == 1){
                evE.setChecked(true);
            }

            else
                evE.setChecked(false);


        }
        sbConsole.delete(0,sbConsole.length());




    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent.equals(listBtDevices)){
            BluetoothDevice device = bluetoothDevices.get(position);
            if (device != null){
                connectThread = new ConnectThread(device);
                connectThread.start();
            }
        }
    }

    private void enableBt(boolean flag) {
        if (flag) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQ_ENABLE_BT);
        } else {
            bluetoothAdapter.disable();
        }
    }

    private  void  setListAdapter (int type){

        bluetoothDevices.clear();
        int iconType = R.drawable.ic_bluetooth_bounded_device;

        switch ( type){
            case   BT_BOUNDED:
                bluetoothDevices = getBoundedDevices();
                iconType = R.drawable.ic_bluetooth_bounded_device;
                break;

            case BT_SEARCH:
                iconType = R.drawable.ic_bluetooth_search_device;
                break;
        }
        listAdapter = new BtlistAdapter(this,bluetoothDevices, iconType);
        listBtDevices.setAdapter(listAdapter);

    }


    private  ArrayList<BluetoothDevice> getBoundedDevices(){
        Set<BluetoothDevice> deviceSet = bluetoothAdapter.getBondedDevices();
        ArrayList<BluetoothDevice> tmpArrayList = new ArrayList<>();
        if (deviceSet.size()>0){
            for (BluetoothDevice device : deviceSet ){
                tmpArrayList.add(device );

            }
        }

        return tmpArrayList;



    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final   String action = intent.getAction();

            switch (action){
                case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                    setListAdapter(BT_SEARCH);
                    break;

                case  BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                    break;

                case  BluetoothDevice.ACTION_FOUND:
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (device != null){
                        bluetoothDevices.add(device);
                        listAdapter.notifyDataSetChanged();

                    }
                    break;
            }
        }
    };


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_LOC:

                if (grantResults.length > 0) {
                    for (int gr : grantResults) {
                        // Check if request is granted or not
                        if (gr != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                    }
                }
                break;
            default:
                return;
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment = null;

            switch (menuItem.getItemId()){
                case R.id.nav_home:
                    selectedFragment = new HomeFragment();
                    break;

                case R.id.nav_control:
                    selectedFragment = new ControlFragment();
                    break;
                case R.id.nav_control2:
                    selectedFragment = new ControlFragment2();
                    break;

                case R.id.nav_info:
                    selectedFragment = new InfoFragment();
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    selectedFragment).commit();

            return true;
        }
    };

    private class ConnectThread  extends Thread{

        private BluetoothSocket bluetoothSocket = null;
        private  Boolean  success = false;

        public  ConnectThread(BluetoothDevice device){
            try{
                Method method = device.getClass().getMethod("createRfcommSocket",
                        new Class[]{int.class});
                bluetoothSocket = (BluetoothSocket) method.invoke(device, 1);

                progressDialog.show();
            } catch (Exception e){
                e.printStackTrace();
            }

        }

        @Override
        public void run() {
            try {
                bluetoothSocket.connect();
                success =  true;
                progressDialog.dismiss();
            }catch (IOException e){
                e.printStackTrace();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        Toast.makeText(MainActivity.this, "No se puede conectar",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                cancel();
            }
            if ( success){

                connectedThread = new ConnectedThread(bluetoothSocket);
                connectedThread.start();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    }
                });
            }
        }

        public  boolean isConnect(){
            return  bluetoothSocket.isConnected();
        }

        public  void  cancel(){
            try {
                bluetoothSocket.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }



    private void readBtData(int textCommand) {
        if (connectedThread != null && connectThread.isConnect()){
            command = "";
            switch(textCommand){
                case STATE_CHANGE:
                    command = "manual#" ;
                    break;
                case OPEN_GATE_ONE:
                    command =  "compuerta1 cerrar#";
                    break;
                case OPEN_GATE_TWO:
                    command =  "compuerta2 cerrar#";
                    break;
                case OPEN_GATE_THREE:
                    command =  "compuerta3 cerrar#";
                    break;
                case STATE_SENSOR2:
                    command =  "data sensor2#";
                    break;
                case STATE_SENSOR:
                    command =  "data sensor#";

                    break;
                case CONTROL_SYSTEM:
                    command = "apagado#";
                    break;

            }

            connectedThread.write(command);
        }
    }


    private  class ConnectedThread extends Thread{
        private final InputStream inputStream;
        private final OutputStream outputStream;
        private  boolean isConnected = false;

        private ConnectedThread(BluetoothSocket bluetoothSocket) {
            InputStream inputStream = null;
            OutputStream outputStream = null;

            try{
                inputStream = bluetoothSocket.getInputStream();
                outputStream = bluetoothSocket.getOutputStream();
            }catch (IOException e){
                e.printStackTrace();
            }

            this.inputStream= inputStream;
            this.outputStream = outputStream;
            isConnected= true;
        }

        @Override
        public void run() {

            bis = new BufferedInputStream(inputStream);
            StringBuffer buffer = new StringBuffer();
            sbConsole = new StringBuffer();

            while (isConnected){
                try {
                    int bytes = bis.read();
                    buffer.append((char) bytes);
                    sbConsole.append(buffer.toString());
                    buffer.delete(0, buffer.length());
                    var = sbConsole.toString();


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


            try {
                bis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        public  void  write(String command){
            byte[] bytes = command.getBytes();
            if (outputStream != null){
                try{
                    outputStream.write(bytes);
                    outputStream.flush();
                }catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        public  void  cancel(){
            try {
                isConnected =  false;
                inputStream.close();
                outputStream.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }


}
