package mx.com.uady.sapra;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.jar.Attributes;

public class LoginActivity extends AppCompatActivity {

    private ProgressBar pb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent =  new Intent(LoginActivity.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        },800);
        }

    }


