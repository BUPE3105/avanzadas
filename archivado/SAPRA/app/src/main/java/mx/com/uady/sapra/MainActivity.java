package mx.com.uady.sapra;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;


public class MainActivity extends AppCompatActivity implements
        CompoundButton.OnCheckedChangeListener ,
        AdapterView.OnItemClickListener,
        View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQ_ENABLE_BT  =   10;
    private static final int BT_BOUNDED     =   21;
    private static final int BT_SEARCH      =   22;
    private static final int REQUEST_CODE_LOC = 1 ;

    private static final int STATE_CHANGE       = 30;
    private static final int OPEN_GATE_ONE      = 31;
    private static final int OPEN_GATE_TWO      = 32;
    private static final int OPEN_GATE_THREE      = 33;
    private static final int STATE_SENSOR2 = 34;
    private static final int STATE_SENSOR         = 35;
    private static final int CONTROL_SYSTEM       = 36;


    private RelativeLayout frameMessage;
    private LinearLayout frameControls;

    private RelativeLayout frameLedControl;
    private RelativeLayout frameLedControl2;
    private String command;

    private ImageView levelImage;



    private StringBuffer sbConsole;
    private ImageButton updateBt;
    private ImageButton updateBt2;
    private String var;
    private String var2;
    private TextView textSensor1;
    private  TextView textCheat;
    private TextView textSensor2;


    private ToggleButton status_evA;
    private ToggleButton status_evB;
    private ToggleButton status_evC;
    private ToggleButton status_evD;

    private TextView txt_nivel;



    private BufferedInputStream bis;



    private Switch switcEnableBt;
    private ProgressBar pbProgress;
    private ListView listBtDevices;

    private BluetoothAdapter bluetoothAdapter;
    private BtlistAdapter listAdapter;
    private ArrayList<BluetoothDevice> bluetoothDevices;


    private ConnectThread connectThread;
    private ConnectedThread connectedThread;

    private ProgressDialog progressDialog;
    private  ProgressBar progressBarHumedadSensor1;
    private  ProgressBar progressBarHumedadSensor2;

    private  TextView textStatusEvA;
    private  TextView textStatusEvB;
    private  TextView textStatusEvC;
    private  TextView textStatusEvD;

    private  TextView textLastUpdate;
    private  TextView textLastUpdate2;


    private ImageButton nextFragment;
    private ImageButton lastFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        frameMessage  = findViewById(R.id.frame_message);
        frameControls = findViewById(R.id.bounded_bt_devices);
        frameLedControl = findViewById(R.id.frameLedControl);
        frameLedControl2 = findViewById(R.id.frameLedControl2);

        switcEnableBt = findViewById(R.id.switch_enable_bt);
        listBtDevices    = findViewById(R.id.lv_bt_device);


        textStatusEvA = findViewById(R.id.status_On_Off_ev_A);
        textStatusEvB = findViewById(R.id.status_On_Off_ev_B);
        textStatusEvC = findViewById(R.id.status_On_Off_evC);
        textStatusEvD = findViewById(R.id.status_On_Off_evD);

        textLastUpdate = findViewById(R.id.status_update_date);
        textLastUpdate2 = findViewById(R.id.status_update_date2);


        textSensor1 = findViewById(R.id.data_text);
        updateBt = findViewById(R.id.update_data);
        updateBt2 = findViewById(R.id.update_data1);
        textSensor2 = findViewById(R.id.data_text2);
        textCheat = findViewById(R.id.text_cheat);

        progressBarHumedadSensor1=findViewById(R.id.staus_pb_data);
        progressBarHumedadSensor2 = findViewById(R.id.staus_pb_data2);

        levelImage = findViewById(R.id.iv_nivel);

        status_evA = findViewById(R.id.status_eV_a);
        status_evB = findViewById(R.id.state_evB);
        status_evC = findViewById(R.id.state_evC);
        status_evD = findViewById(R.id.state_eve);

        txt_nivel = findViewById(R.id.txt_nivel);

        nextFragment = findViewById(R.id.next_Fragment);
        lastFragment = findViewById(R.id.last_fragment);

        switcEnableBt.setOnCheckedChangeListener(this);


        listBtDevices.setOnItemClickListener(this);


        updateBt.setOnClickListener(this);
        updateBt2.setOnClickListener(this);
        textCheat.setOnClickListener(this);
        nextFragment.setOnClickListener(this);
        lastFragment.setOnClickListener(this);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        bluetoothDevices =  new ArrayList<>();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(getString(R.string.connecting));
        progressDialog.setMessage(getString(R.string.wait));



        /*
        * Brujeria
        * */

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        registerReceiver(receiver, filter);

      /*Observa el estado de la conexion de bluetooth
      * */

        if (bluetoothAdapter == null) {
            Toast.makeText(this, getString(R.string.bluetooth_not_supported), Toast.LENGTH_SHORT).show();
            Log.d(TAG, "onCreate: " + getString(R.string.bluetooth_not_supported));
            finish();
        }

        if (bluetoothAdapter.isEnabled()) {
            showFrameControls();
            setListAdapter(BT_BOUNDED);
        }

        if (switcEnableBt.isChecked()){
            switcEnableBt.setChecked(false);
        }


    }

    /*Metodos basicos de android estudio
    * */

    /*
    * onDestroy cierra el metodo de comunicacion entre la app y algun modulo de bluetooth constante*/

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(receiver);

        if(connectThread != null){
            connectThread.cancel();
        }

        if (connectedThread != null){
            connectedThread.cancel();
        }
        showFrameMessage();

    }

    @Override
    protected void onPause() {
        super.onPause();
        bluetoothAdapter.disable();
    }




    /*
    * OnClick permite el uso del boton search (buscar dispostivos cercanos) y el de desconectar (Desconectar sirve se usa en la parte de la comunicacion del arduino)
    * */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.infoAction){
        }
        if (id == R.id.ActionDisconnect){
            if (connectedThread != null){
                connectedThread.cancel();
                switcEnableBt.setChecked(false);
            }

            if (connectThread != null){
                connectThread.cancel();
            }

            showFrameMessage();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        if (v.equals(nextFragment)){
            showMoreFrameControls();
        }
        if (v.equals(lastFragment)){
            showLastFrameControls();
        }


        if (v.equals(updateBt)){

            updateMethod();

        }

        if (v.equals(updateBt2)){
            updateMethod();
        }
    }


public  void updateMethod(){

    readBtData(STATE_SENSOR);
    textCheat.setText(var);
    var2 = textCheat.getText().toString();
    if (!var2.isEmpty()){
        String val[] = var2.split("");
        int n = Integer.parseInt(val[1]);
        int n1 = Integer.parseInt(val[2]);
        int n2 = Integer.parseInt(val[3]);

        int total = (n*100)+ (n1*10) + n2;

        textSensor1.setText(String.valueOf(total) + " %");
        progressBarHumedadSensor1.setProgress(total);
        int m = Integer.parseInt(val[4]);
        int m1 = Integer.parseInt(val[5]);
        int m2 = Integer.parseInt(val[6]);

        int total2 = (m*100)+ (m1*10) + m2;

        textSensor2.setText(String.valueOf(total2)+" %");
        progressBarHumedadSensor2.setProgress(total2);

        int level = Integer.valueOf(val[7]);

        if(level == 1){

            txt_nivel.setText("Alto");
            levelImage.setImageResource(R.drawable.level_3);
        }

        else if(level == 2){
            txt_nivel.setText("Medio");
            levelImage.setImageResource(R.drawable.level_2);

        }

        else if(level == 3){
            txt_nivel.setText("Bajo");
            levelImage.setImageResource(R.drawable.level_1);

        }

        else if(level == 4){
            txt_nivel.setText("Vacio");
            levelImage.setImageResource(R.drawable.level_0);

        }

        else if(level == 5){
            txt_nivel.setText("Problema con el tanque");
            levelImage.setImageResource(R.drawable.level_error);

        }


        int a = Integer.valueOf(val[8]);

        if( a == 1){
            status_evA.setChecked(true);
            textStatusEvA.setText("Prendido");
            status_evA.setBackgroundResource(R.drawable.on_icon);
        }

        else {
            status_evA.setChecked(false);
            textStatusEvA.setText("Apagado");
            status_evC.setBackgroundResource(R.drawable.off_icon);

        }

        int b = Integer.valueOf(val[9]);

        if( b == 1){
            status_evB.setBackgroundResource(R.drawable.on_icon);
            status_evB.setChecked(true);
            textStatusEvB.setText("Prendido");

        }else {
            status_evB.setChecked(false);
            textStatusEvB.setText("Apagado");
            status_evC.setBackgroundResource(R.drawable.off_icon);

        }

        int c = Integer.valueOf(val[10]);

        if( c == 1){
            status_evC.setBackgroundResource(R.drawable.on_icon);
            status_evC.setChecked(true);
            textStatusEvC.setText("Prendido");

        }
        else {
            status_evC.setChecked(false);
            textStatusEvC.setText("Apagado");
            status_evC.setBackgroundResource(R.drawable.off_icon);

        }

        int e = Integer.valueOf(val[11]);

        if( e == 1){
            status_evD.setBackgroundResource(R.drawable.on_icon);
            status_evD.setChecked(true);
            textStatusEvD.setText("Prendido");

        } else {
            status_evD.setChecked(false);
            textStatusEvD.setText("Apagado");
            status_evD.setBackgroundResource(R.drawable.off_icon);

        }
        textLastUpdate.setText(new SimpleDateFormat("dd-MM-yyyy , hh:mm", Locale.US).format(new Date()));
        textLastUpdate2.setText(new SimpleDateFormat("dd-MM-yyyy , hh:mm", Locale.US).format(new Date()));

    }

    sbConsole.delete(0,sbConsole.length());

}
    /*Permite darle click al dispostivo que te aparezca en tu busqueda y unirlo ademas de gaurdarlo en tu lista de dispostivos

    Esto igual sirve para entrar a algun dispostivo si esta ya esta guardado
    * */

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (parent.equals(listBtDevices)){
                BluetoothDevice device = bluetoothDevices.get(position);
                if (device != null){
                    connectThread = new ConnectThread(device);
                    connectThread.start();
                }
            }
    }

    /*
    * Supervisa los cambios de estado de los switch y permite el prendido de los leds por le metodo
    * */

    @Override
    public void onCheckedChanged(CompoundButton buttonView,
                                 boolean isChecked) {

        if (buttonView.equals(switcEnableBt)) {

            enableBt(isChecked);
            showFrameControls();
            setListAdapter(BT_BOUNDED);

            if (!isChecked) {
                showFrameMessage();
            }
        }
    }


    /*
    * Metodo por el cual se logra la comunicacion de bluetooth
    * */

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    @Nullable Intent data) {
        if (requestCode == REQ_ENABLE_BT) {
            if (resultCode == RESULT_OK && bluetoothAdapter.isEnabled()) {
                showFrameControls();
                setListAdapter(BT_BOUNDED);
            } else if (resultCode == RESULT_CANCELED) {
                enableBt(true);
            }
        }
    }


    /*
    * Metodos de carga para las diferentes pantallas de la app
    * */

        private void showFrameMessage (){
        frameMessage.setVisibility(View.VISIBLE);
        frameControls.setVisibility(View.GONE);
        frameLedControl.setVisibility(View.GONE);
            frameLedControl2.setVisibility(View.GONE);

        }

    private void showFrameControls (){
        frameMessage.setVisibility(View.GONE);
        frameControls.setVisibility(View.VISIBLE);
        frameLedControl.setVisibility(View.GONE);
        frameLedControl2.setVisibility(View.GONE);


    }
    private void showFrameLEdControls (){
        frameMessage.setVisibility(View.GONE);
        frameControls.setVisibility(View.GONE);
        frameLedControl.setVisibility(View.VISIBLE);
        frameLedControl2.setVisibility(View.GONE);


    }  private void showMoreFrameControls(){
        frameMessage.setVisibility(View.GONE);
        frameControls.setVisibility(View.GONE);
        frameLedControl.setVisibility(View.GONE);
        frameLedControl2.setVisibility(View.VISIBLE);
    }
   private void showLastFrameControls(){
        frameMessage.setVisibility(View.GONE);
        frameControls.setVisibility(View.GONE);
        frameLedControl.setVisibility(View.VISIBLE);
        frameLedControl2.setVisibility(View.GONE);
    }

    /*
     * Te permite conectarte al bluetooth
     * */

    private void enableBt(boolean flag) {
        if (flag) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQ_ENABLE_BT);
        } else {
            bluetoothAdapter.disable();
        }
    }

    /*
    * Metodo que nos permite dibujar la lista de dispositvos conectados y las imagenes de al lado, junyo con la clase BtListAdapter
    * */

    private  void  setListAdapter (int type){

        bluetoothDevices.clear();
        int iconType = R.drawable.ic_bluetooth_bounded_device;

        switch ( type){
            case   BT_BOUNDED:
                bluetoothDevices = getBoundedDevices();
                iconType = R.drawable.ic_bluetooth_bounded_device;
                break;

            case BT_SEARCH:
                iconType = R.drawable.ic_bluetooth_search_device;
                break;
        }
        listAdapter = new BtlistAdapter(this,bluetoothDevices, iconType);
        listBtDevices.setAdapter(listAdapter);

    }

    private  ArrayList<BluetoothDevice> getBoundedDevices(){
        Set<BluetoothDevice> deviceSet = bluetoothAdapter.getBondedDevices();
        ArrayList<BluetoothDevice> tmpArrayList = new ArrayList<>();
        if (deviceSet.size()>0){
            for (BluetoothDevice device : deviceSet ){
                tmpArrayList.add(device );

            }
        }

        return tmpArrayList;
    }

    /*
    * Metodo de busqueda de dispositivos
    * */

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final   String action = intent.getAction();

            switch (action){
                case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                    setListAdapter(BT_SEARCH);
                    break;

                case  BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                    break;

                case  BluetoothDevice.ACTION_FOUND:
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (device != null){
                        bluetoothDevices.add(device);
                        listAdapter.notifyDataSetChanged();

                    }
                    break;
            }
        }
    };

    /*
    * Permisos esenciales para Marshmallow
    * */

    private void accessLocationPermission() {
        int accessCoarseLocation = this.checkSelfPermission(android.Manifest.
                                        permission.ACCESS_COARSE_LOCATION);
        int accessFineLocation   = this.checkSelfPermission(android.Manifest.
                                        permission.ACCESS_FINE_LOCATION);

        List<String> listRequestPermission = new ArrayList<String>();

        if (accessCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            listRequestPermission.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (accessFineLocation != PackageManager.PERMISSION_GRANTED) {
            listRequestPermission.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!listRequestPermission.isEmpty()) {
            String[] strRequestPermission = listRequestPermission.
                                            toArray(new String[listRequestPermission.size()]);
            this.requestPermissions(strRequestPermission, REQUEST_CODE_LOC);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_LOC:

                if (grantResults.length > 0) {
                    for (int gr : grantResults) {
                        // Check if request is granted or not
                        if (gr != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                    }
                }
                break;
            default:
                return;
        }
    }

    /*
    * Magia negra y brujeria
    * */


    /*
     * Metodo escalable para mandar informacion y permitiir que al arduino pueda prender el led o lo que se pida
     * */


    private void readBtData(int textCommand) {
        if (connectedThread != null && connectThread.isConnect()){
            command = "";
            switch(textCommand){
                case STATE_CHANGE:
                    command = "manual#" ;
                    break;
                case OPEN_GATE_ONE:
                    command =  "compuerta1 cerrar#";
                    break;
                case OPEN_GATE_TWO:
                    command =  "compuerta2 cerrar#";
                    break;
                case OPEN_GATE_THREE:
                    command =  "compuerta3 cerrar#";
                    break;
                case STATE_SENSOR2:
                    command =  "data sensor2#";
                    break;
                case STATE_SENSOR:
                    command =  "data sensor#";

                    break;
                case CONTROL_SYSTEM:
                    command = "apagado#";
                    break;

            }

            connectedThread.write(command);
        }
    }


    private class ConnectThread  extends Thread{

        private BluetoothSocket bluetoothSocket = null;
        private  Boolean  success = false;

        public  ConnectThread(BluetoothDevice device){
            try{
            Method method = device.getClass().getMethod("createRfcommSocket",
                                                            new Class[]{int.class});
            bluetoothSocket = (BluetoothSocket) method.invoke(device, 1);

            progressDialog.show();
            } catch (Exception e){
                e.printStackTrace();
            }

        }

        @Override
        public void run() {
            try {
                bluetoothSocket.connect();
                success =  true;
                progressDialog.dismiss();
            }catch (IOException e){
                e.printStackTrace();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        Toast.makeText(MainActivity.this, "No se puede conectar",
                                                                Toast.LENGTH_SHORT).show();
                    }
                });
                cancel();
            }
            if ( success){

                connectedThread = new ConnectedThread(bluetoothSocket);
                connectedThread.start();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                showFrameLEdControls();
                    }
                });
            }
        }

        public  boolean isConnect(){
            return  bluetoothSocket.isConnected();
        }

        public  void  cancel(){
            try {
                bluetoothSocket.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    private  class ConnectedThread extends Thread{
        private final InputStream inputStream;
        private final OutputStream outputStream;
        private  boolean isConnected = false;

        private ConnectedThread(BluetoothSocket bluetoothSocket) {
            InputStream inputStream = null;
            OutputStream outputStream = null;

            try{
                inputStream = bluetoothSocket.getInputStream();
                outputStream = bluetoothSocket.getOutputStream();
            }catch (IOException e){
                e.printStackTrace();
            }

            this.inputStream= inputStream;
            this.outputStream = outputStream;
            isConnected= true;
        }

        @Override
        public void run() {

            bis = new BufferedInputStream(inputStream);
            StringBuffer buffer = new StringBuffer();
            sbConsole = new StringBuffer();

            while (isConnected){
                try {
                    int bytes = bis.read();
                    buffer.append((char) bytes);
                    sbConsole.append(buffer.toString());
                    buffer.delete(0, buffer.length());
                    var = sbConsole.toString();


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


            try {
                bis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        public  void  write(String command){
            byte[] bytes = command.getBytes();
            if (outputStream != null){
                try{
                    outputStream.write(bytes);
                    outputStream.flush();
                }catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        public  void  cancel(){
            try {
                isConnected =  false;
                inputStream.close();
                outputStream.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }





}
