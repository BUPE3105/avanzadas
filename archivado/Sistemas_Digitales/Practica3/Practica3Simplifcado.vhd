----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:28:20 02/09/2019 
-- Design Name: 
-- Module Name:    Practica3Simplifcado - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Practica3Simplifcado is
    Port ( a : in  STD_LOGIC;
           b : in  STD_LOGIC;
           c : in  STD_LOGIC;
           f0 : out  STD_LOGIC;
           f1 : out  STD_LOGIC;
           f2 : out  STD_LOGIC;
           f3 : out  STD_LOGIC;
           f4 : out  STD_LOGIC;
           f5 : out  STD_LOGIC);
end Practica3Simplifcado;

architecture Behavioral of Practica3Simplifcado is

	signal funcionBC : std_logic;
	signal funcionAC : std_logic;
	signal funcionAB : std_logic;
	signal funcionABC : std_logic;
	signal siempre : std_logic;
	
begin

	funcionBC <= ((not a and b) and c);
	funcionAC <= ((a and not b) and c);
	funcionAB <= (a and b);
	siempre <= '0';
	
	f0 <= funcionAB;
	f1 <= (a and not b) or (a and c);
	f2 <=  funcionBC or funcionAC;
	f3 <= (b and not c);
	f4 <= siempre;
	f5 <= c and ((a xor b) or (a xnor b));

end Behavioral;

