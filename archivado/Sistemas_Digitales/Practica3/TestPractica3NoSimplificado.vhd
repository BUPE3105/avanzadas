--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   22:08:59 02/09/2019
-- Design Name:   
-- Module Name:   C:/Users/asus/avanzadas/Sistemas_Digitales/Practica3/TestPractica3NoSimplificado.vhd
-- Project Name:  Practica3
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Practica3_No_Simplificado
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TestPractica3NoSimplificado IS
END TestPractica3NoSimplificado;
 
ARCHITECTURE behavior OF TestPractica3NoSimplificado IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Practica3_No_Simplificado
    PORT(
         a : IN  std_logic;
         b : IN  std_logic;
         c : IN  std_logic;
         f0 : OUT  std_logic;
         f1 : OUT  std_logic;
         f2 : OUT  std_logic;
         f3 : OUT  std_logic;
         f4 : OUT  std_logic;
         f5 : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic := '0';
   signal b : std_logic := '0';
   signal c : std_logic := '0';

 	--Outputs
   signal f0 : std_logic;
   signal f1 : std_logic;
   signal f2 : std_logic;
   signal f3 : std_logic;
   signal f4 : std_logic;
   signal f5 : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Practica3_No_Simplificado PORT MAP (
          a => a,
          b => b,
          c => c,
          f0 => f0,
          f1 => f1,
          f2 => f2,
          f3 => f3,
          f4 => f4,
          f5 => f5
        );
	
	a <= '0',
		'1' after 200 ns;
	b <= '0',
		'1' after 100 ns,
		'0' after 200 ns,
		'1' after 300 ns;
	c <= '0',
		'1' after 50 ns,
		'0' after 100 ns,
		'1' after 150 ns,
		'0' after 200 ns,
		'1' after 250 ns,
		'0' after 300 ns,
		'1' after 350 ns;
	
	
END;
