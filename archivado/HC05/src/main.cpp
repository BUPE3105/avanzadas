#include <Arduino.h>

String command;
int compuerta1   = 6;
int compuerta2   = 7;
int compuerta3   = 8;
int bomba        = 9;
int cambioEstado = 10;
int sensor       = 11;
int prendido    = 12;
int val          = 0;
int ledPrueba   = 13;

void setup() {
  Serial.begin(9600);
  pinMode(compuerta1,OUTPUT);
  pinMode(compuerta2, OUTPUT);
  pinMode(compuerta3, OUTPUT);
  pinMode(bomba, OUTPUT);
  pinMode(cambioEstado, OUTPUT);
  pinMode(sensor, OUTPUT);
  pinMode(prendido, OUTPUT);
}

void encendido ();
void apagado();
void manual() ;
void automatico();
void execute();
void bluetooth();


void loop() {
  digitalWrite(ledPrueba,HIGH);
  bluetooth();
}


void encendido() {
  digitalWrite(compuerta1, HIGH);
  digitalWrite(compuerta2, HIGH);
  digitalWrite(compuerta3, HIGH);
  digitalWrite(bomba, HIGH);
  digitalWrite(sensor, HIGH);
  digitalWrite(prendido, HIGH);
  val = 1;
}


void apagado(/* arguments */) {
  digitalWrite(compuerta1, LOW);
  digitalWrite(compuerta2, LOW);
  digitalWrite(compuerta3, LOW);
  digitalWrite(bomba, LOW);
  digitalWrite(sensor, LOW);
  digitalWrite(prendido, LOW);
}


void  manual(/* arguments */) {
  digitalWrite(compuerta1, LOW);
  digitalWrite(compuerta2, LOW);
  digitalWrite(compuerta3, LOW);
  digitalWrite(bomba, LOW);
}


void automatico(/* arguments */) {
  digitalWrite(compuerta1, LOW);
  digitalWrite(compuerta2, LOW);
  digitalWrite(compuerta3, LOW);
  digitalWrite(bomba, LOW);
}


void execute() {
    if (command.equals("manual")) {
      Serial.println("Modo manual");
      digitalWrite(cambioEstado, HIGH);
      manual();

    }else if (command.equals("automatico")) {
      Serial.println("Modo automatico");
      digitalWrite(cambioEstado,LOW);
      automatico();

    }else if (command.equals("data sensor") && val == 1) {
      Serial.println("Sensor Data");

    }else if (command.equals("inicio")) {
      Serial.println("Se ha iniciado el sistema");
      encendido();

    }else if (command.equals("apagado")) {
      Serial.println("Se ha apagado el sistema");
      apagado();

    }

    if (command.equals("compuerta1 abrir")){
      digitalWrite(compuerta1,HIGH);
    }else if(command.equals("compuerta1 cerrar")){
      digitalWrite(compuerta1,LOW);
    }else if (command.equals("compuerta2 abrir")){
      digitalWrite(compuerta2,HIGH);
    }else if(command.equals("compuerta2 cerrar")){
      digitalWrite(compuerta2,LOW);
    }else if(command.equals("compuerta3 abrir")){
      digitalWrite(compuerta3,HIGH);
    }else if(command.equals("compuerta3 cerrar")){
      digitalWrite(compuerta3,LOW);
    }else if(command.equals("Bomba abrir")){
          digitalWrite(bomba,HIGH);
    }else if(command.equals("Bomba cerrar")){
          digitalWrite(bomba,LOW);
    }

}


void bluetooth() {

  if (Serial.available()>0) {
    char tmp= Serial.read();

    if (tmp=='#') {
      execute();
      command="";
    }else{
      command += tmp;
    }
  }
}
