package mx.com.uady.pc_shop.DB.OrderAssemblies;

import android.support.annotation.NonNull;

public class MissingProducts {

    private String description;


    private String products;

    private int stock;

    private int qty;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @NonNull
    @Override
    public String toString() {
        return products;
    }
}
