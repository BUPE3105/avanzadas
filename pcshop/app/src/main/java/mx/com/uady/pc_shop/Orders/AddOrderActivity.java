package mx.com.uady.pc_shop.Orders;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.List;

import mx.com.uady.pc_shop.Adapters.AdapterInfoAssembliesPrices;
import mx.com.uady.pc_shop.Adapters.OrdersAdapter;
import mx.com.uady.pc_shop.DB.AssembliesProducts.Asemblies_Productos_Qty_Price;
import mx.com.uady.pc_shop.DB.AssembliesProducts.AssembliesAndProducts;
import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.R;
import mx.com.uady.pc_shop.ViewModel.AssembliesModel;
import mx.com.uady.pc_shop.ViewModel.ClientModel;
import mx.com.uady.pc_shop.ViewModel.OrdersModel;

public class AddOrderActivity extends AppCompatActivity {

    private ClientModel model2;
    private AssembliesModel model;
    private Spinner clientspinner;
    private AdapterInfoAssembliesPrices adapter;
    private int clientid;
    private RecyclerView recyclerView;
    private List<Asemblies_Productos_Qty_Price> asemblies_productos_qty_prices2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_order);
        Toolbar toolbar = findViewById(R.id.toolbar_add_orders);
        setSupportActionBar(toolbar);
        adapter = new AdapterInfoAssembliesPrices();
        clientspinner = findViewById(R.id.client_choose_add);

        recyclerView = findViewById(R.id.recyler_view_add_orders);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        setTitle("Ordenes");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        model = ViewModelProviders.of(this).get(AssembliesModel.class);
        model.getAssembliesprice().observe(this, new Observer<List<Asemblies_Productos_Qty_Price>>() {
            @Override
            public void onChanged(@Nullable List<Asemblies_Productos_Qty_Price> asemblies_productos_qty_prices) {
                adapter.submitList(asemblies_productos_qty_prices);


            }
        });

        model2 = ViewModelProviders.of(this).get(ClientModel.class);
        model2.getAllClients().observe(this, new Observer<List<Clients>>() {
            @Override
            public void onChanged(@Nullable List<Clients> clients) {
                ArrayAdapter<Clients> clientsArrayAdapter = new ArrayAdapter<>(AddOrderActivity.this,
                        R.layout.support_simple_spinner_dropdown_item,
                        clients);
                clientsArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                clientspinner.setAdapter(clientsArrayAdapter);
                clientspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Clients parentSelectedItem = (Clients) parent.getSelectedItem();
                        clientid = parentSelectedItem.getClientId();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }
        });

    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

}
