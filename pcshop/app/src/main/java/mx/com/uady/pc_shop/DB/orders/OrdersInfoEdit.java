package mx.com.uady.pc_shop.DB.orders;

import android.arch.persistence.room.ColumnInfo;
import android.support.annotation.NonNull;

public class OrdersInfoEdit {

    @ColumnInfo(name = "first_name")
    private String first_name;
    @ColumnInfo(name = "last_name")
    private String last_name;
    @ColumnInfo(name = "descripcion")
    private String descripcion;
    @ColumnInfo(name = "assembly_id")
    private int assembly_id;
    @ColumnInfo(name = "customer_id")
    private int customer_id;
    @ColumnInfo(name = "assembly_qty")
    private int assembly_qty;
    @ColumnInfo(name = "product_assemblies")
    private int product_assemblies;
    @ColumnInfo(name = "total_price")
    private int total_price;
    @ColumnInfo(name = "default_id")
    private int default_id;
    @ColumnInfo(name = "id")
    private int id;

    public int getAssembly_id() {
        return assembly_id;
    }

    public void setAssembly_id(int assembly_id) {
        this.assembly_id = assembly_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getAssembly_qty() {
        return assembly_qty;
    }

    public void setAssembly_qty(int assembly_qty) {
        this.assembly_qty = assembly_qty;
    }

    public int getProduct_assemblies() {
        return product_assemblies;
    }

    public void setProduct_assemblies(int product_assemblies) {
        this.product_assemblies = product_assemblies;
    }

    public int getTotal_price() {
        return total_price;
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }

    public int getDefault_id() {
        return default_id;
    }

    public void setDefault_id(int default_id) {
        this.default_id = default_id;
    }

    @NonNull
    @Override
    public String toString() {
        return descripcion;
    }
}