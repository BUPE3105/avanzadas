package mx.com.uady.pc_shop.Report;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.List;

import mx.com.uady.pc_shop.Adapters.AdapterMonthlySales;
import mx.com.uady.pc_shop.AssembliesActivity;
import mx.com.uady.pc_shop.DB.orders.ReportMonthlySales;
import mx.com.uady.pc_shop.R;
import mx.com.uady.pc_shop.ViewModel.OrdersModel;
import mx.com.uady.pc_shop.ViewModel.ProductModel;

import static maes.tech.intentanim.CustomIntent.customType;

public class MonthlySales extends AppCompatActivity {

    private OrdersModel model;
    private RecyclerView recyclerView;
    private AdapterMonthlySales adapter;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_sales);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Ventas por mes");

        model = ViewModelProviders.of(this).get(OrdersModel.class);

        model.getMonthlySales().observe(this, new Observer<List<ReportMonthlySales>>() {
            @Override
            public void onChanged(@Nullable List<ReportMonthlySales> reportMonthlySales) {
                adapter.submitList(reportMonthlySales);
                adapter.setOnItemClickListener(new AdapterMonthlySales.OnItemClicklistener() {
                    @Override
                    public void onItemClick(ReportMonthlySales reportMonthlySales, View view) {
                       intent = new Intent(MonthlySales.this, MonthSalesActivity.class);
                       intent.putExtra(MonthSalesActivity.DATA_ID, reportMonthlySales.getId());
                        startActivity(intent);
                        customType(MonthlySales.this, "left-to-right");
                    }
                });
            }
        });
         adapter= new AdapterMonthlySales();

        recyclerView = findViewById(R.id.sales_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

}
