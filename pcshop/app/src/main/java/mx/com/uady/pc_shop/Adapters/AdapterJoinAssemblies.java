package mx.com.uady.pc_shop.Adapters;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import mx.com.uady.pc_shop.DB.Assemblies.Assemblies;
import mx.com.uady.pc_shop.DB.AssembliesProducts.AssembliesAndProducts;
import mx.com.uady.pc_shop.DB.Products.Products;
import mx.com.uady.pc_shop.R;

public class AdapterJoinAssemblies extends ListAdapter<AssembliesAndProducts, AdapterJoinAssemblies.Holder> {

    public AdapterJoinAssemblies() {
        super(DIFF_CALLBACK);
    }

    private OnItemClicklistener mlistener;
    private OnLongItemClickListner listener;


    private static final DiffUtil.ItemCallback<AssembliesAndProducts> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<AssembliesAndProducts>() {
                @Override
                public boolean areItemsTheSame(@NonNull AssembliesAndProducts oldItem, @NonNull AssembliesAndProducts newItem) {
                    return oldItem.getDescription() == newItem.getDescription();
                }

                @Override
                public boolean areContentsTheSame(@NonNull AssembliesAndProducts oldItem, @NonNull AssembliesAndProducts newItem) {
                    return oldItem.getQty() == (newItem.getQty()) &&
                            oldItem.getDescription().equals(newItem.getDescription())&&
                                    oldItem.getId() == (newItem.getId());
                }
            };

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.assemblies_products_item,
                parent,false);
        return new AdapterJoinAssemblies.Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterJoinAssemblies.Holder holder, int position) {
        AssembliesAndProducts current =  getItem(position);
        String qty = Integer.toString(current.getQty());
        holder.descripcion.setText(current.getDescription());
        holder.qty.setText("Necesita: "+qty);


    }

    class Holder extends RecyclerView.ViewHolder{
        private TextView descripcion;
        private TextView qty;

        public Holder(@NonNull final View itemView) {
            super(itemView);
            descripcion = itemView.findViewById(R.id.description_item_assemblies_products);
            qty = itemView.findViewById(R.id.stock_item_assemblies_products);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mlistener != null && position != RecyclerView.NO_POSITION) {
                        mlistener.onItemClick(getItem(position), itemView);
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.OnLongClick(getItem(position), itemView);
                    }
                    return true;
                }
            });

        }
    }


    public interface OnItemClicklistener {
        void onItemClick(AssembliesAndProducts assembliesAndProducts, View view);

    }

    public  interface  OnLongItemClickListner{
        void OnLongClick(AssembliesAndProducts assembliesAndProducts, View view);
    }

    public void setOnLongItemClickListner(OnLongItemClickListner Llistner){
        this.listener = Llistner;
    }

    public void setOnItemClickListener(OnItemClicklistener clicklistener) {
        this.mlistener = clicklistener;
    }

}
