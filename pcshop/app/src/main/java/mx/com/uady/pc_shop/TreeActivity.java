package mx.com.uady.pc_shop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import mx.com.uady.pc_shop.Clients.ClientActivity;
import mx.com.uady.pc_shop.Orders.OrdersActivity;

public class TreeActivity extends AppCompatActivity {

    private  Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tree);

        ImageButton button = findViewById(R.id.button_productos);
        ImageButton button2 = findViewById(R.id.button_paquetes);
        ImageButton button3 = findViewById(R.id.button_clientes);
        ImageButton button4 = findViewById(R.id.button_orders);
        ImageButton button5 = findViewById(R.id.button_reportes);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 intent = new Intent(TreeActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(TreeActivity.this, ClientActivity.class);
                startActivity(intent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 intent = new Intent(TreeActivity.this, AssembliesActivity.class);
                startActivity(intent);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(TreeActivity.this, OrdersActivity.class);
                startActivity(intent);
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(TreeActivity.this, ReportActivity.class);
                startActivity(intent);
            }
        });
    }
}
