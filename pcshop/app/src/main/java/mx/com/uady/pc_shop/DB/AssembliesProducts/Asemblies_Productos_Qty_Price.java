package mx.com.uady.pc_shop.DB.AssembliesProducts;

import android.arch.persistence.room.ColumnInfo;

public class Asemblies_Productos_Qty_Price {
    @ColumnInfo(name = "Assembly_Id")
    private  int assembly_id;
    @ColumnInfo(name = "Assembly")
    private  String assembly;
    @ColumnInfo(name = "Product_qty")
    private  int product_Qty;
    @ColumnInfo(name = "Assembly_price")
    private  int assembly_price;

    public int getAssembly_id() {
        return assembly_id;
    }

    public void setAssembly_id(int assembly_id) {
        this.assembly_id = assembly_id;
    }

    public String getAssembly() {
        return assembly;
    }

    public void setAssembly(String assembly) {
        this.assembly = assembly;
    }

    public int getProduct_Qty() {
        return product_Qty;
    }

    public void setProduct_Qty(int product_Qty) {
        this.product_Qty = product_Qty;
    }

    public int getAssembly_price() {
        return assembly_price;
    }

    public void setAssembly_price(int assembly_price) {
        this.assembly_price = assembly_price;
    }
}
