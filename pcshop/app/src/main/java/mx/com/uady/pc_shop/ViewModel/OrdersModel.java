package mx.com.uady.pc_shop.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.List;

import mx.com.uady.pc_shop.DB.AppDatabase;
import mx.com.uady.pc_shop.DB.AppRepository;
import mx.com.uady.pc_shop.DB.Clients.ClientDAO;
import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.DB.OrderAssemblies.OrderAssemblies;
import mx.com.uady.pc_shop.DB.OrderAssemblies.OrderAssemblyDAO;
import mx.com.uady.pc_shop.DB.orders.OrdersDAO;
import mx.com.uady.pc_shop.DB.orders.OrdersInfoEdit;
import mx.com.uady.pc_shop.DB.orders.Orders_Clients;
import mx.com.uady.pc_shop.DB.orders.ReportMonthlySales;
import mx.com.uady.pc_shop.DB.orders.ReportMonthlySalesDetails;

public class OrdersModel extends AndroidViewModel {
    private AppRepository ordersrepository;
    private OrdersDAO ordersDAO;
    private OrderAssemblyDAO orderAssemblyDAO;
    private AppDatabase ordersDb;


    public OrdersModel(@NonNull Application application) {
        super(application);
        ordersrepository = new AppRepository(application);
        ordersDb = AppDatabase.getInstance(application);
        orderAssemblyDAO = ordersDb.orderAssemblyDAO();
        ordersDAO = ordersDb.ordersDAO();

    }

    public LiveData<List<ReportMonthlySales>> getMonthlySales() {
        return ordersrepository.monthlySales();
    }

    public LiveData<List<ReportMonthlySalesDetails>> getMonthlySales(int id) {
        return ordersrepository.monthlySalesDetails(id);
    }

    public LiveData<List<Orders_Clients>> getclientsorders() {
        return ordersrepository.clients_orders();
    }

    public LiveData<List<Orders_Clients>> getclientsordersbyid(int clientid) {
        return ordersrepository.clients_ordersbyid(clientid);
    }

    public LiveData<List<Orders_Clients>> getclientsordersbystatus(int staus) {
        return ordersrepository.clients_ordersbystatus(staus);
    }

    public LiveData<List<OrdersInfoEdit>> getInfoEditOrders(int id) {
        return ordersrepository.getOrdersinfoEdit(id);
    }

    public LiveData<OrderAssemblies> getAllById(int id){
        return  ordersrepository.getAllById(id);
    }

    public void delete(OrderAssemblies orderAssemblies) {
        new DeleteAsyncTask(orderAssemblyDAO).execute(orderAssemblies);
    }
    public void update(OrderAssemblies orderAssemblies) {
        new UpdateAsyncTask(orderAssemblyDAO).execute(orderAssemblies);
    }
    public void insert(OrderAssemblies orderAssemblies) {
        new InsertAsyncTask(orderAssemblyDAO).execute(orderAssemblies);
    }

    public  LiveData<Integer> getDefaultId(){
        return  ordersrepository.getDefaultId();
    }

    public  LiveData<Integer> getNormalId(int id){
        return  ordersrepository.getNormalId(id);
    }


    private class OperationsAsyncTask extends AsyncTask<OrderAssemblies, Void, Void> {

        OrderAssemblyDAO mAsyncTaskDao;

        OperationsAsyncTask(OrderAssemblyDAO dao) {
            this.mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(OrderAssemblies... orderAssemblies) {
            return null;
        }
    }


    private class DeleteAsyncTask extends OperationsAsyncTask {

        public DeleteAsyncTask(OrderAssemblyDAO dao) {
            super(dao);
        }

        @Override
        protected Void doInBackground(OrderAssemblies... orderAssemblies) {
            mAsyncTaskDao.delete(orderAssemblies[0]);
            return null;
        }
    }
    private class UpdateAsyncTask extends OperationsAsyncTask {

        public UpdateAsyncTask(OrderAssemblyDAO dao) {
            super(dao);
        }

        @Override
        protected Void doInBackground(OrderAssemblies... orderAssemblies) {
            mAsyncTaskDao.update(orderAssemblies[0]);
            return null;
        }
    }

    private class InsertAsyncTask extends OperationsAsyncTask {

        InsertAsyncTask(OrderAssemblyDAO dao) {
            super(dao);
        }

        @Override
        protected Void doInBackground(OrderAssemblies... orderAssemblies) {
            mAsyncTaskDao.insert(orderAssemblies[0]);
            return null;
        }
    }


}
