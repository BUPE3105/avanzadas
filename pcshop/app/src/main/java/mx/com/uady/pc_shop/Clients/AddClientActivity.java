package mx.com.uady.pc_shop.Clients;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.R;
import mx.com.uady.pc_shop.ViewModel.ClientModel;

public class AddClientActivity extends AppCompatActivity {

    public static  int id1 ;
    public  static final String id2 = "id";
    public static final String first_name2 = "first";
    public static final String last_name2 = "last";
    public static final String email12 = "email";
    public static final String phone12 = "phone1";
    public static final String phone22= "phone2";
    public static final String phone32 = "phone3";
    public static final String phone12_5 = "phone1_5";
    public static final String phone22_5= "phone2_5";
    public static final String phone32_5 = "phone3_5";
    public static final String direccion1 = "phone332";
    public static final String EXTRA_ID = "mx.com.uady.EXTRA_CLIENTS_ID";
    private EditText firstedit;
    private EditText lastedit;
    private EditText emailedit;
    private EditText phone1edit;
    private EditText phone2edit;
    private EditText phone3edit;
    private EditText phone1_5edit;
    private EditText phone2_5edit;
    private EditText phone3_5edit;
    private EditText direccion;
    private CheckBox phone2check;
    private CheckBox phone3check;
    private CheckBox emailcheck;
    private ClientModel model;
    boolean doubleBackToExitPressedOnce = false;
    private FloatingActionButton save;
    public LiveData<Clients>clients;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_client);

        Toolbar toolbar = findViewById(R.id.toolbar_add_client);

        firstedit = findViewById(R.id.first_name_add);
        lastedit = findViewById(R.id.last_name_add);
        emailedit = findViewById(R.id.email_add);
        phone1edit = findViewById(R.id.phone1_add);
        phone2edit = findViewById(R.id.phone2_add);
        phone3edit = findViewById(R.id.phone3_add);
        phone1_5edit = findViewById(R.id.phone1_5_add);
        phone2_5edit = findViewById(R.id.phone2_5_add);
        phone3_5edit = findViewById(R.id.phone3_5_add);
        phone2check = findViewById(R.id.phone2_add_checkbox);
        phone3check = findViewById(R.id.iphone3_add_checkbox);
        emailcheck = findViewById(R.id.email_add_checkbox);
        direccion = findViewById(R.id.direccion_add);
        save = findViewById(R.id.save_button);
        setTitle("Agregar Cliente");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        phone2edit.setEnabled(false);
        phone3edit.setEnabled(false);
        phone2_5edit.setEnabled(false);
        phone3_5edit.setEnabled(false);
        emailedit.setEnabled(false);
        save.setEnabled(false);
        firstedit.addTextChangedListener(loginTextWatcher);
        lastedit.addTextChangedListener(loginTextWatcher);
        phone1edit.addTextChangedListener(loginTextWatcher);
        direccion.addTextChangedListener(loginTextWatcher);



        phone2check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    phone2edit.setEnabled(true);
                    phone2_5edit.setEnabled(true);
                }else{

                    phone2edit.setEnabled(false);
                    phone2_5edit.setEnabled(false);
                }
            }
        });
        phone3check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    phone3edit.setEnabled(true);
                    phone3_5edit.setEnabled(true);
                }else{

                    phone3edit.setEnabled(false);
                    phone3_5edit.setEnabled(true);
                }
            }
        });

        emailcheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    emailedit.setEnabled(true);
                }else{

                    emailedit.setEnabled(false);
                }
            }
        });
        phone2edit.setEnabled(false);
        phone3edit.setEnabled(false);
        emailedit.setEnabled(false);




        model = ViewModelProviders.of(this).get(ClientModel.class);

        Random rand = new Random();
        id1 = rand.nextInt(100);




    }
    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String firstInput = firstedit.getText().toString().trim();
            String lastInput = lastedit.getText().toString().trim();
            String phoneInput = phone1edit.getText().toString().trim();
            String phone_5Input = phone1_5edit.getText().toString().trim();
            String direccionInput = direccion.getText().toString().trim();

            save.setEnabled(!firstInput.isEmpty() && !lastInput.isEmpty()&& !phoneInput.isEmpty()
                    && !direccionInput.isEmpty()&& !phone_5Input.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    public void AddClient(View view){

            String updatedfirstname = firstedit.getText().toString();
            String updatedlasttname = lastedit.getText().toString();
            String updatedemail = emailedit.getText().toString();
            String updatedphone1 = phone1edit.getText().toString();
            String updatedphone2 = phone2edit.getText().toString();
            String updatedphone3= phone3edit.getText().toString();
            String updatedphone1_5 = phone1_5edit.getText().toString();
            String updatedphone2_5 = phone2_5edit.getText().toString();
            String updatedphone3_5= phone3_5edit.getText().toString();
            String updatedireccion = direccion.getText().toString();
            Intent resultIntent = new Intent();
            resultIntent.putExtra(id2, id1);
            resultIntent.putExtra(first_name2, updatedfirstname);
            resultIntent.putExtra(last_name2, updatedlasttname);
            resultIntent.putExtra(email12, updatedemail);
            resultIntent.putExtra(phone12, updatedphone1);
            resultIntent.putExtra(phone22, updatedphone2);
            resultIntent.putExtra(phone32, updatedphone3);
            resultIntent.putExtra(email12, updatedemail);
            resultIntent.putExtra(phone12_5, updatedphone1_5);
            resultIntent.putExtra(phone22_5, updatedphone2_5);
            resultIntent.putExtra(phone32_5, updatedphone3_5);
            resultIntent.putExtra(direccion1,updatedireccion);
            setResult(RESULT_OK, resultIntent);
            finish();


    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Esta seguro de salir sin guardar", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
