package mx.com.uady.pc_shop.Orders;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import mx.com.uady.pc_shop.Adapters.AssembliesAdapter;
import mx.com.uady.pc_shop.AssemblyDetails;
import mx.com.uady.pc_shop.DB.Assemblies.AssemblyProductsPrice;
import mx.com.uady.pc_shop.DB.OrderAssemblies.OrderAssemblies;
import mx.com.uady.pc_shop.R;
import mx.com.uady.pc_shop.ViewModel.AssembliesModel;
import mx.com.uady.pc_shop.ViewModel.OrdersModel;

public class AddAssemblytoOrderActivity extends AppCompatActivity {

    private AssembliesModel model;
    private RecyclerView recyclerView;
    private AssembliesAdapter adapter;
    private EditText editText;
    private Editable Query;
    private  int default_id;

    private  int assemblyId;
    private  int normal_id;

    private OrdersModel insert;

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_assemblyto_order);
        Toolbar toolbar = findViewById(R.id.toolbar_add_assembly_order);
        setSupportActionBar(toolbar);

        setTitle("Agregar ensambles");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        intent = getIntent();

        editText = findViewById(R.id.add_assembly_order_edit_text);

        model = ViewModelProviders.of(this).get(AssembliesModel.class);
        setAdapter();

        insert = ViewModelProviders.of(this).get(OrdersModel.class);

        registerForContextMenu(recyclerView);
    }

    void setAdapter() {
        adapter = new AssembliesAdapter();
        recyclerView = findViewById(R.id.add_assembly_order_recycler_view);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

        }
        recyclerView.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                Query = editText.getText();
                if (Query == null || Query.toString().isEmpty()) {
                    model.getDescriptionAndPrice().observe(AddAssemblytoOrderActivity.this, new Observer<List<AssemblyProductsPrice>>() {
                        @Override
                        public void onChanged(@Nullable List<AssemblyProductsPrice> assemblyProductsPrices) {
                            adapter.submitList(assemblyProductsPrices);
                            adapter.setOnLongItemClickListner(new AssembliesAdapter.OnLongItemClickListner() {
                                @Override
                                public void OnLongClick(AssemblyProductsPrice assemblyProductsPrice, View view) {
                                    assemblyId= assemblyProductsPrice.getId();
                                    insert.getDefaultId().observe(AddAssemblytoOrderActivity.this, new Observer<Integer>() {
                                        @Override
                                        public void onChanged(@Nullable Integer integer) {
                                            default_id = integer+1;
                                            insert.getNormalId(intent.getIntExtra("test", 0)).observe(AddAssemblytoOrderActivity.this, new Observer<Integer>() {
                                                @Override
                                                public void onChanged(@Nullable Integer integer) {
                                                    normal_id = integer;
                                                }
                                            });
                                        }



                                    });
                                    openContextMenu(view);
                                }
                            });
                        }
                    });
                } else {
                    model.getDescriptionAndPriceByString("%" + Query + "%").observe(AddAssemblytoOrderActivity.this, new Observer<List<AssemblyProductsPrice>>() {
                        @Override
                        public void onChanged(@Nullable List<AssemblyProductsPrice> assemblyProductsPrices) {
                            adapter.submitList(assemblyProductsPrices);
                            adapter.setOnItemClickListener(new AssembliesAdapter.OnItemClicklistener() {
                                @Override
                                public void onItemClick(AssemblyProductsPrice assemblyProductsPrice, View view) {
                                    assemblyId= assemblyProductsPrice.getId();
                                    insert.getDefaultId().observe(AddAssemblytoOrderActivity.this, new Observer<Integer>() {
                                        @Override
                                        public void onChanged(@Nullable Integer integer) {
                                            default_id = integer+1;
                                            insert.getNormalId(intent.getIntExtra("test", 0)).observe(AddAssemblytoOrderActivity.this, new Observer<Integer>() {
                                                @Override
                                                public void onChanged(@Nullable Integer integer) {
                                                    normal_id = integer;
                                                }
                                            });
                                        }



                                    });                                    openContextMenu(view);
                                }
                            });
                        }
                    });
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Opciones: ");
        menu.add(0, 721, 0, "Detalles...");
        menu.add(0, 722, 0, "Agregar ensamble a la orden.");
        menu.setHeaderIcon(R.drawable.ic_storage_black_24dp);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 721:
                Intent details = new Intent(AddAssemblytoOrderActivity.this, AssemblyDetails.class);
                details.putExtra(AssemblyDetails.DATA_ID, assemblyId);
                startActivity(details);

                break;
            case 722:
                OrderAssemblies orderAssemblies = new OrderAssemblies(default_id,normal_id,assemblyId,1  );
                insert.insert(orderAssemblies);
                Toast.makeText(this, "Se ha agregado el assembly", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }

}
