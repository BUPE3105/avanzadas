package mx.com.uady.pc_shop.DB.orders;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "orders")
public class Orders {
    @PrimaryKey
    private int id;
    @ColumnInfo(name = "status_id")
    private  int statusId;
    @ColumnInfo(name = "customer_id")
    private int customerId;
    @ColumnInfo(name = "year")
    private  int year;
    @ColumnInfo(name = "month")
    private int month;
    @ColumnInfo(name = "day")
    private int day;
    @ColumnInfo(name = "change_log")
    private String change_log;

    public Orders(int id, int statusId, int customerId, int year, int month, int day, String change_log) {
        this.id = id;
        this.statusId = statusId;
        this.customerId = customerId;
        this.year = year;
        this.month = month;
        this.day = day;
        this.change_log = change_log;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getChange_log() {
        return change_log;
    }

    public void setChange_log(String change_log) {
        this.change_log = change_log;
    }

    @Override
    public String toString() {
        return change_log;
    }
}
