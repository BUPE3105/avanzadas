package mx.com.uady.pc_shop.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import mx.com.uady.pc_shop.DB.AppRepository;
import mx.com.uady.pc_shop.DB.Categories.Categories;

public class CategoriesModel extends AndroidViewModel {

    private AppRepository categoriesRepository;
    private LiveData<List<Categories>> getAll;

    public  CategoriesModel (Application application){
        super(application);

        categoriesRepository =  new AppRepository(application);
        getAll = categoriesRepository.getAllCategorias();
   }

   public LiveData<List<Categories>> getALLCategories (){
        return getAll;
   }




}
