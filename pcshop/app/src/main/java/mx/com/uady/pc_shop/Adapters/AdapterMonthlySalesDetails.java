package mx.com.uady.pc_shop.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.DB.orders.ReportMonthlySales;
import mx.com.uady.pc_shop.DB.orders.ReportMonthlySalesDetails;
import mx.com.uady.pc_shop.R;

public class AdapterMonthlySalesDetails extends ListAdapter <ReportMonthlySalesDetails, AdapterMonthlySalesDetails.Holder>  {
    private OnItemClicklistener mlistener;

    public AdapterMonthlySalesDetails() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<ReportMonthlySalesDetails> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<ReportMonthlySalesDetails>() {
                @Override
                public boolean areItemsTheSame(@NonNull ReportMonthlySalesDetails oldItem, @NonNull ReportMonthlySalesDetails newItem) {
                    return oldItem.getFirstName().equals(newItem.getFirstName());
                }

                @Override
                public boolean areContentsTheSame(@NonNull ReportMonthlySalesDetails oldItem, @NonNull ReportMonthlySalesDetails newItem) {
                    return oldItem.getStatusDescription().equals(newItem.getStatusDescription()) &&
                            oldItem.getMonth()== (newItem.getMonth()) &&
                            oldItem.getFirstName().equals( newItem.getFirstName()) &&
                            oldItem.getYear() == (newItem.getYear());
                }
            };

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.details_monthly_sales_item,
                parent,false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        ReportMonthlySalesDetails current =  getItem(position);

        holder.client.setText(current.getLastName()+" , "+ current.getFirstName());
        holder.Assembly.setText(current.getDescription());
        holder.Status.setText(current.getStatusDescription());
        String year= Integer.toString(current.getYear());
        String day = Integer.toString(current.getDay());
        holder.Date.setText(day+"/"+current.getMonths()+"/"+year);

    }

    public ReportMonthlySalesDetails getAssembliesAt(int position) {
        return getItem(position);
    }

    class Holder extends RecyclerView.ViewHolder {

        private TextView client;
        private TextView Assembly;
        private TextView Date;
        private TextView Status;

        public Holder(@NonNull final View itemView) {
            super(itemView);
            client= itemView.findViewById(R.id.name_client_sales);
            Assembly= itemView.findViewById(R.id.fecha_details_sales);
            Date= itemView.findViewById(R.id.fecha_details_sales);
            Status= itemView.findViewById(R.id.status_details_sales);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mlistener != null && position != RecyclerView.NO_POSITION) {
                        mlistener.onItemClick(getItem(position), itemView);
                    }
                }
            });

        }

    }

    @Override
    public void onViewRecycled(@NonNull Holder holder) {
        super.onViewRecycled(holder);
    }


    public interface OnItemClicklistener {
        void onItemClick(ReportMonthlySalesDetails reportMonthlySalesDetails, View view);

    }

    public void setOnItemClickListener(OnItemClicklistener clicklistener) {
        this.mlistener = clicklistener;
    }

}
