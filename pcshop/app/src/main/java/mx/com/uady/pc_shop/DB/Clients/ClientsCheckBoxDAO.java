package mx.com.uady.pc_shop.DB.Clients;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ClientsCheckBoxDAO {

    @Query("SELECT * from clients_checkbox ORDER BY id Asc")
    LiveData<List<ClientsCheckBox>> getAllClientsCheckBox();

    @Query("Select * from clients_checkbox where id like (:id)")
    LiveData<List<ClientsCheckBox>> getClientCheckBoxById(Integer id);


}
