package mx.com.uady.pc_shop.DB.orders;

import android.arch.persistence.room.ColumnInfo;
import android.support.annotation.NonNull;

public class ReportMonthlySalesDetails {

    @ColumnInfo(name = "FirstName")
    private  String FirstName;
    @ColumnInfo(name = "LastName")
    private  String LastName;
    @ColumnInfo (name = "Month")
    private  int  month;
    @ColumnInfo(name = "year")
    private  int year;
    @ColumnInfo(name = "StatusDescription")
    private  String statusDescription;
    @ColumnInfo(name = "assemblies")
    private  String description;
    @ColumnInfo(name ="months" )
    private String months;
    @ColumnInfo(name = "day")
    private  int day;

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMonths() {
        return months;
    }

    public void setMonths(String months) {
        this.months = months;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }



    @NonNull
@Override
    public String toString() {
        return description;
    }
}
