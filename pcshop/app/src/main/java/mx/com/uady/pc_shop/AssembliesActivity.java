package mx.com.uady.pc_shop;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import mx.com.uady.pc_shop.Adapters.AdapterJoinAssemblies;
import mx.com.uady.pc_shop.Adapters.AssembliesAdapter;
import mx.com.uady.pc_shop.DB.Assemblies.Assemblies;
import mx.com.uady.pc_shop.DB.Assemblies.AssemblyProductsPrice;
import mx.com.uady.pc_shop.DB.AssembliesProducts.AssembliesAndProducts;
import mx.com.uady.pc_shop.ViewModel.AssembliesModel;

import static maes.tech.intentanim.CustomIntent.customType;

public class AssembliesActivity extends AppCompatActivity {

    private AssembliesModel model;
    private AssembliesAdapter adapter;
    private RecyclerView recyclerView;
    private EditText editText;
    private Editable Query;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assemblies);
        editText = findViewById(R.id.edit_text_to_query_assembly);
        model = ViewModelProviders.of(this).get(AssembliesModel.class);
        Toolbar toolbar = findViewById(R.id.toolabar_assemblies);

        setTitle("Ensamblados");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setAdapter();
        toolbar.setTitleTextColor(Color.WHITE);

        registerForContextMenu(recyclerView);
    }

    void setAdapter() {
        adapter = new AssembliesAdapter();

        recyclerView = findViewById(R.id.recyler_view_assemblies);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                Query = editText.getText();
                if (Query == null || Query.toString().isEmpty()) {
                    model.getDescriptionAndPrice().observe(AssembliesActivity.this, new Observer<List<AssemblyProductsPrice>>() {
                        @Override
                        public void onChanged(@Nullable List<AssemblyProductsPrice> assemblyProductsPrices) {
                            adapter.submitList(assemblyProductsPrices);
                            adapter.setOnItemClickListener(new AssembliesAdapter.OnItemClicklistener() {
                                @Override
                                public void onItemClick(AssemblyProductsPrice assemblyProductsPrice, View view) {
                                    intent = new Intent(AssembliesActivity.this, AssemblyDetails.class);
                                    intent.putExtra(AssemblyDetails.DATA_DES, assemblyProductsPrice.getDescripcion());
                                    intent.putExtra(AssemblyDetails.DATA_ID, assemblyProductsPrice.getId());
                                    openContextMenu(view);
                                }
                            });
                        }
                    });
                } else {
                    model.getDescriptionAndPriceByString("%" + Query + "%").observe(AssembliesActivity.this, new Observer<List<AssemblyProductsPrice>>() {
                        @Override
                        public void onChanged(@Nullable List<AssemblyProductsPrice> assemblyProductsPrices) {
                            adapter.submitList(assemblyProductsPrices);
                            adapter.setOnItemClickListener(new AssembliesAdapter.OnItemClicklistener() {
                                @Override
                                public void onItemClick(AssemblyProductsPrice assemblyProductsPrice, View view) {
                                    intent = new Intent(AssembliesActivity.this, AssemblyDetails.class);
                                    intent.putExtra(AssemblyDetails.DATA_DES, assemblyProductsPrice.getDescripcion());
                                    intent.putExtra(AssemblyDetails.DATA_ID, assemblyProductsPrice.getId());
                                    openContextMenu(view);
                                }
                            });
                        }
                    });
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Opciones: ");
        menu.add(0, 123, 0, "Detalles");
        menu.setHeaderIcon(R.drawable.ic_storage_black_24dp);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 123:
                startActivity(intent);
                customType(AssembliesActivity.this, "left-to-right");
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {

        finish();
        return true;
    }
}
