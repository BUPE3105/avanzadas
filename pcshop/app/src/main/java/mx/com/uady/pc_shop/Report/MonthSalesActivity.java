package mx.com.uady.pc_shop.Report;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import mx.com.uady.pc_shop.Adapters.AdapterJoinAssemblies;
import mx.com.uady.pc_shop.Adapters.AdapterMonthlySales;
import mx.com.uady.pc_shop.Adapters.AdapterMonthlySalesDetails;
import mx.com.uady.pc_shop.DB.orders.ReportMonthlySalesDetails;
import mx.com.uady.pc_shop.R;
import mx.com.uady.pc_shop.ViewModel.AssembliesModel;
import mx.com.uady.pc_shop.ViewModel.OrdersModel;

public class MonthSalesActivity extends AppCompatActivity {

    public static String DATA_ID = "mx.com.uady.pc_shop.Report.ID";
    private OrdersModel model;
    private RecyclerView recyclerView;
    private AdapterMonthlySalesDetails adapter;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_sales);

        recyclerView = findViewById(R.id.sales_recycler_view);

        intent = getIntent();
        model = ViewModelProviders.of(this).get(OrdersModel.class);

        model.getMonthlySales(intent.getIntExtra(DATA_ID, 1)).observe(this, new Observer<List<ReportMonthlySalesDetails>>() {
            @Override
            public void onChanged(@Nullable List<ReportMonthlySalesDetails> reportMonthlySalesDetails) {
                adapter.submitList(reportMonthlySalesDetails);
            }
        });

        adapter = new AdapterMonthlySalesDetails ();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);


    }
}
