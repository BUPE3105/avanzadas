package mx.com.uady.pc_shop.DB.ordersStauts;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "order_status")
public class OrdersStatus {

    @PrimaryKey
    private int id;
    @ColumnInfo(name = "description")
    private String description;
    @ColumnInfo(name = "editable")
    private int editable;
    @ColumnInfo(name = "previous")
    private String previous;
    @ColumnInfo(name = "next")
    private String next;

    public OrdersStatus(int id, String description, int editable, String previous, String next) {
        this.id = id;
        this.description = description;
        this.editable = editable;
        this.previous = previous;
        this.next = next;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getEditable() {
        return editable;
    }

    public void setEditable(int editable) {
        this.editable = editable;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

@NonNull
@Override
    public String toString() {
        return description;
    }
}
