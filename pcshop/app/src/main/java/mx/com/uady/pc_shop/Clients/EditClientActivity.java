package mx.com.uady.pc_shop.Clients;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.R;
import mx.com.uady.pc_shop.ViewModel.ClientModel;

public class EditClientActivity extends AppCompatActivity {

    private static String number2;
    private static  String number3;
    private static String email2;
    public static  int id1 ;
    public  static final String id = "id";
    public static final String first_name = "first";
    public static final String last_name = "last";
    public static final String email1 = "email";
    public static final String phone1 = "phone1";
    public static final String phone2 = "phone2";
    public static final String phone3 = "phone3";
    public static final String phone1_5 = "phone1_5";
    public static final String phone2_5 = "phone2_5";
    public static final String phone3_5 = "phone3_5";
    public static final String direccion1 = "kñll";
    public static final String EXTRA_ID = "mx.com.uady.EXTRA_CLIENTS_ID";
    private EditText firstedit;
    private EditText lastedit;
    private EditText emailedit;
    private EditText phone1edit;
    private EditText phone2edit;
    private EditText phone3edit;
    private EditText phone1_5edit;
    private EditText phone2_5edit;
    private EditText phone3_5edit;
    private EditText direccion;
    private CheckBox phone2check;
    private CheckBox phone3check;
    private CheckBox emailcheck;
    private ClientModel model;
    private FloatingActionButton save;
    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_client);
        Toolbar toolbar = findViewById(R.id.toolbar_edit_client);

        firstedit = findViewById(R.id.first_name_edit);
        lastedit = findViewById(R.id.last_name_edit);
        emailedit = findViewById(R.id.email_edit);
        phone1edit = findViewById(R.id.phone1_edit);
        phone2edit = findViewById(R.id.phone2_edit);
        phone3edit = findViewById(R.id.phone3_edit);
        phone1_5edit = findViewById(R.id.phone1_5_edit);
        phone2_5edit = findViewById(R.id.phone2_5_edit);
        phone3_5edit = findViewById(R.id.phone3_5_edit);
        phone2check = findViewById(R.id.phone2_check_Edit);
        phone3check = findViewById(R.id.phone3_check_Edit);
        emailcheck = findViewById(R.id.email_edit_checkbox);
        direccion = findViewById(R.id.direccion_edti);
        save = findViewById(R.id.save_button_edit);


        setTitle("Editar Cliente");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        phone2edit.setEnabled(false);
        phone3edit.setEnabled(false);
        phone2_5edit.setEnabled(false);
        phone3_5edit.setEnabled(false);
        emailedit.setEnabled(false);
        firstedit.addTextChangedListener(loginTextWatcher);
        lastedit.addTextChangedListener(loginTextWatcher);
        phone1edit.addTextChangedListener(loginTextWatcher);
        direccion.addTextChangedListener(loginTextWatcher);

        save.setEnabled(false);


        phone2check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    phone2_5edit.setEnabled(true);
                    phone2edit.setEnabled(true);
                }else{
                    phone2_5edit.setEnabled(false);
                    phone2edit.setEnabled(false);
                }
            }
        });
        phone3check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    phone3_5edit.setEnabled(true);
                    phone3edit.setEnabled(true);
                }else{
                    phone3_5edit.setEnabled(false);
                    phone3edit.setEnabled(false);
                }
            }
        });

        emailcheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    emailedit.setEnabled(true);
                }else{

                    emailedit.setEnabled(false);
                }
            }
        });
        model = ViewModelProviders.of(this).get(ClientModel.class);
        Intent intent = getIntent();
        if(intent.hasExtra(EXTRA_ID)){
            model.getClientById(intent.getIntExtra(EXTRA_ID, 0)).observe(this, new Observer<Clients>() {
                @Override
                public void onChanged(@Nullable Clients clients) {

                    firstedit.setText(clients.getFirst_name());
                    lastedit.setText(clients.getLast_name());
                    emailedit.setText(clients.getEmail());



                    String phone1 = clients.getPhone1();
                    if(phone1.equals("")||phone1.equals(null)){
                        phone1_5edit.setText("");
                        phone1edit.setText("");
                    }else{
                        String[]phone_1 = phone1.split("-");
                        phone1_5edit.setText(phone_1[0]);
                        phone1edit.setText(phone_1[1]);
                    }

                    String phone2 = clients.getPhone2();
                    if(phone2.equals("")||phone2.equals(null)){
                        phone2_5edit.setText("");
                        phone2edit.setText("");
                    }else{
                        String[]phone_2 = phone2.split("-");
                        phone2_5edit.setText(phone_2[0]);
                        phone2edit.setText(phone_2[1]);
                    }

                    String phone3 = clients.getPhone3();
                    if(phone3.equals("")||phone3.equals(null)){
                        phone3_5edit.setText("");
                        phone3edit.setText("");
                    }else{
                        String[]phone_3 = phone3.split("-");
                        phone3_5edit.setText(phone_3[0]);
                        phone3edit.setText(phone_3[1]);

                    }

                    direccion.setText(clients.getDireccion());
                    id1 = clients.getClientId();
                }
            });


        }

    }
    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String firstInput = firstedit.getText().toString().trim();
            String lastInput = lastedit.getText().toString().trim();
            String phoneInput = phone1edit.getText().toString().trim();
            String phone_5Input = phone1_5edit.getText().toString().trim();
            String direccionInput = direccion.getText().toString().trim();

            save.setEnabled(!firstInput.isEmpty() && !lastInput.isEmpty()&& !phoneInput.isEmpty()
                    && !direccionInput.isEmpty()&& !phone_5Input.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Esta seguro de salir sin guardar", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
    public void updateClient(View view){
        String updatedfirstname = firstedit.getText().toString();
        String updatedlasttname = lastedit.getText().toString();
        String updatedemail = emailedit.getText().toString();
        String updatedphone1 = phone1edit.getText().toString();
        String updatedphone2 = phone2edit.getText().toString();
        String updatedphone3= phone3edit.getText().toString();
        String updatedphone1_5 = phone1_5edit.getText().toString();
        String updatedphone2_5 = phone2_5edit.getText().toString();
        String updatedphone3_5= phone3_5edit.getText().toString();
        String updatedireccion = direccion.getText().toString();
        Intent resultIntent = new Intent();
        resultIntent.putExtra(id, id1);
        resultIntent.putExtra(first_name, updatedfirstname);
        resultIntent.putExtra(last_name, updatedlasttname);
        resultIntent.putExtra(email1, updatedemail);
        resultIntent.putExtra(phone1, updatedphone1);
        resultIntent.putExtra(phone2, updatedphone2);
        resultIntent.putExtra(phone3, updatedphone3);
        resultIntent.putExtra(phone1_5, updatedphone1_5);
        resultIntent.putExtra(phone2_5, updatedphone2_5);
        resultIntent.putExtra(phone3_5, updatedphone3_5);
        resultIntent.putExtra(direccion1,updatedireccion);
        setResult(RESULT_OK, resultIntent);
        finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
