package mx.com.uady.pc_shop.Adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import mx.com.uady.pc_shop.DB.AssembliesProducts.Asemblies_Productos_Qty_Price;
import mx.com.uady.pc_shop.DB.orders.Orders_Clients;
import mx.com.uady.pc_shop.R;

public class AdapterInfoAssembliesPrices extends ListAdapter<Asemblies_Productos_Qty_Price, AdapterInfoAssembliesPrices.Holder> {

    private AdapterInfoAssembliesPrices.OnItemClicklistener mlistener;
    private AdapterInfoAssembliesPrices.OnLongItemClickListner listener;

    public AdapterInfoAssembliesPrices() {

        super(DIFF_CALLBACK);
    }
    private static final DiffUtil.ItemCallback<Asemblies_Productos_Qty_Price> DIFF_CALLBACK = new DiffUtil.ItemCallback<Asemblies_Productos_Qty_Price>() {
        @Override
        public boolean areItemsTheSame(@NonNull Asemblies_Productos_Qty_Price oldItem, @NonNull Asemblies_Productos_Qty_Price newItem) {
            return oldItem.getAssembly_id() == newItem.getAssembly_id();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Asemblies_Productos_Qty_Price oldItem, @NonNull Asemblies_Productos_Qty_Price newItem) {
            return oldItem.getAssembly_id() == (newItem.getAssembly_id()) &&
                    oldItem.getAssembly().equals(newItem.getAssembly()) &&
                    oldItem.getProduct_Qty()==(newItem.getProduct_Qty())
                    && oldItem.getAssembly_price()==(newItem.getAssembly_price())
                    ;
        }
    };
    @NonNull
    @Override
    public AdapterInfoAssembliesPrices.Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_asembly
                , parent, false);
        return new AdapterInfoAssembliesPrices.Holder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull AdapterInfoAssembliesPrices.Holder holder, int position) {
        Asemblies_Productos_Qty_Price current = getItem(position);

        holder.assemblyqty.setMinValue(1);
        holder.assemblyqty.setMaxValue(30);
        holder.description.setText(current.getAssembly());

        int number = (current.getAssembly_price()*holder.assemblyqty.getValue())/100;
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator('\'');
        symbols.setDecimalSeparator(',');

        DecimalFormat decimalFormat = new DecimalFormat("$ #,###.00", symbols);

        String precio  = decimalFormat.format(number);
        holder.cost.setText("Costo: "+precio);

        String productqt = Integer.toString(current.getProduct_Qty());
        holder.productqty.setText("Productos requeridos: "+productqt);




    }
    class Holder extends RecyclerView.ViewHolder {

        private TextView description;
        private TextView productqty;
        private NumberPicker assemblyqty;

        private TextView cost;

        public Holder(@NonNull final View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.assemblie_description);
            productqty = itemView.findViewById(R.id.assembly_productqty);
            assemblyqty = itemView.findViewById(R.id.order_assembles_qty);

            cost = itemView.findViewById(R.id.cost_order_assemblies);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mlistener != null && position != RecyclerView.NO_POSITION) {
                        mlistener.onItemClick(getItem(position), itemView);
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.OnLongClick(getItem(position), itemView);
                    }
                    return true;
                }
            });

        }
    }

    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }
    public interface OnItemClicklistener {
        void onItemClick(Asemblies_Productos_Qty_Price asemblies_productos_qty_price, View view);

    }

    public void setOnItemClickListener(AdapterInfoAssembliesPrices.OnItemClicklistener clicklistener) {
        this.mlistener = clicklistener;
    }
    public  interface  OnLongItemClickListner{
        void OnLongClick(Asemblies_Productos_Qty_Price asemblies_productos_qty_price, View view);
    }

    public void setOnLongItemClickListner(AdapterInfoAssembliesPrices.OnLongItemClickListner Llistner){
        this.listener = Llistner;
    }
}
