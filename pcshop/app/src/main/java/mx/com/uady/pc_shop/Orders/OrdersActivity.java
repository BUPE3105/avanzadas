package mx.com.uady.pc_shop.Orders;

import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;

import android.content.res.Configuration;

import android.content.Intent;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import mx.com.uady.pc_shop.Adapters.ClientsAdapter;
import mx.com.uady.pc_shop.Adapters.OrdersAdapter;
import mx.com.uady.pc_shop.AssembliesActivity;
import mx.com.uady.pc_shop.AssemblyDetails;
import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.DB.orders.Orders_Clients;
import mx.com.uady.pc_shop.R;
import mx.com.uady.pc_shop.ViewModel.ClientModel;
import mx.com.uady.pc_shop.ViewModel.OrdersModel;

import static maes.tech.intentanim.CustomIntent.customType;

public class OrdersActivity extends AppCompatActivity {

    private static final int NEW_ORDER_ACTIVITY_REQUEST_CODE = 1;
    public static final int UPDATE_NOTE_ACTIVITY_REQUEST_CODE = 2;
    private OrdersAdapter adapter;
    private RecyclerView recyclerView;
    private OrdersModel model;
    private ClientModel model2;
    private Spinner cliensspinner;
    private FloatingActionButton addorder;
    private int client_id;
    private String clientfullname;
    private static boolean state1;
    private static boolean state2;
    private static boolean state3;
    private static boolean state4 ;
    private static boolean state5 ;
    private static int  state_id;
    private  Intent intent;
    private static int pending;

    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        Toolbar toolbar = findViewById(R.id.toolbar_orders);
        setSupportActionBar(toolbar);
        adapter = new OrdersAdapter();
        cliensspinner = findViewById(R.id.client_choose);
        button  = findViewById(R.id.multi_spinner_estado);
        addorder  =findViewById(R.id.add_button_orders);


        recyclerView = findViewById(R.id.recyler_view_orders);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        setTitle("Ordenes");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        registerForContextMenu(recyclerView);
        addorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(OrdersActivity.this, AddOrderActivity.class);
                startActivityForResult(intent,NEW_ORDER_ACTIVITY_REQUEST_CODE);

            }
        });

        model2 = ViewModelProviders.of(this).get(ClientModel.class);
        model2.getAllClients().observe(this, new Observer<List<Clients>>() {
            @Override
            public void onChanged(@Nullable List<Clients> clients) {
                ArrayAdapter<Clients> clientsArrayAdapter = new ArrayAdapter<>(OrdersActivity.this,
                        R.layout.support_simple_spinner_dropdown_item,
                        clients);
                Clients clients2 = new Clients(35000,"Todos","","","","","","");
                clientsArrayAdapter.add(clients2);
                clientsArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                cliensspinner.setAdapter(clientsArrayAdapter);
                cliensspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Clients parentSelectedItem = (Clients) parent.getSelectedItem();
                        client_id = parentSelectedItem.getClientId();
                        clientfullname = parentSelectedItem.getFirst_name()+" "+parentSelectedItem.getLast_name();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }
        });

        model = ViewModelProviders.of(this).get(OrdersModel.class);


    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
    public void OnTopicState(View v){

        final int aIndex = 0;
        final int bIndex = 1;
        final int cIndex = 2;
        final int dIndex = 3;
        final int eIndex = 4;



        final CharSequence[] items = {"Pendiente" ,
                "Cancelado" ,
                "Confirmado" ,
                "En tránsito",
                "Finalizado"
        };

        final Boolean[] state = new Boolean[items.length];
        state[aIndex] = false;
        state[bIndex] = false;
        state[cIndex] = false;
        state[dIndex] = false;
        state[eIndex] = false;



                AlertDialog.Builder builder = new AlertDialog.Builder(OrdersActivity.this);
                builder.setTitle("Estados");

                builder.setMultiChoiceItems(items, new boolean[]{state1,state2,state3,state4,state5},
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                                state[which] = isChecked;
                                if (state[0]) {
                                    state_id = 0;
                                    state1 = true;
                                }else{state1 = false;}
                                if (state[1]) {
                                    state_id = 1;
                                    state2 = true;
                                }else{state2 = false;}
                                if (state[2]) {
                                    state_id = 2;
                                    state3 = true;
                                }
                                if (state[3]) {
                                    state_id = 3;

                                    state4 = true;
                                }else{state4 = false;}
                                if (state[4]) {
                                    state_id = 4;
                                    state5 = true;
                                }else{state5 = false;}

                            }});

                builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Opciones: ");
        menu.add(0, 123, 0, "Detalles");
        menu.add(0, 456, 1, "Regresar estado a...");
        menu.add(0, 789, 2, "Avanzar estado a...");
        menu.add(0, 567, 3, "Editar orden");
        menu.setHeaderIcon(R.drawable.ic_storage_black_24dp);

    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_search:

                if(state1||state2 ||state3||state4||state5){
                    model.getclientsordersbystatus(state_id).observe(this, new Observer<List<Orders_Clients>>() {
                        @Override
                        public void onChanged(@Nullable List<Orders_Clients> orders_clients) {
                            adapter.submitList(orders_clients);
                            adapter.setOnLongItemClickListner(new OrdersAdapter.OnLongItemClickListner() {
                                @Override
                                public void OnLongClick(Orders_Clients orders_clients, View view) {

                                }
                            });
                        }
                    });
                }

                if(client_id == 35000){
                    model.getclientsorders().observe(this, new Observer<List<Orders_Clients>>() {
                        @Override
                        public void onChanged(@Nullable List<Orders_Clients> orders_clients) {
                            adapter.submitList(orders_clients);
                            adapter.setOnLongItemClickListner(new OrdersAdapter.OnLongItemClickListner() {
                                @Override
                                public void OnLongClick(Orders_Clients orders_clients, View view) {

                                }
                            });

                        }
                    });
                }else{
                    model.getclientsordersbyid(client_id).observe(this, new Observer<List<Orders_Clients>>() {
                        @Override
                        public void onChanged(@Nullable List<Orders_Clients> orders_clients) {
                            adapter.submitList(orders_clients);
                            adapter.setOnLongItemClickListner(new OrdersAdapter.OnLongItemClickListner() {
                                @Override
                                public void OnLongClick(Orders_Clients orders_clients, View view) {
                                 pending = orders_clients.getStatusid();
                                 intent = new Intent(OrdersActivity.this, EditOrderActivity.class);
                                 intent.putExtra(EditOrderActivity.EXTRA_ID, orders_clients.getCustomer_id() );
                                    openContextMenu(view);

                                }
                            });

                        }
                    });
                }



            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 567:
                if (pending !=0){
                    Toast.makeText(this, "No se puede editar", Toast.LENGTH_SHORT).show();
                }else {

                    startActivity(intent);
                    customType(OrdersActivity.this, "left-to-right");
                }
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }
}
