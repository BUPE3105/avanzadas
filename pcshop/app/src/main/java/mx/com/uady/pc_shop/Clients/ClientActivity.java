package mx.com.uady.pc_shop.Clients;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.List;

import mx.com.uady.pc_shop.Adapters.ClientsAdapter;
import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.DB.Clients.ClientsCheckBox;
import mx.com.uady.pc_shop.DB.orders.Orders_Clients;
import mx.com.uady.pc_shop.MultiSelectionSpinner;
import mx.com.uady.pc_shop.R;
import mx.com.uady.pc_shop.ViewModel.ClientModel;
import mx.com.uady.pc_shop.ViewModel.OrdersModel;

import static maes.tech.intentanim.CustomIntent.customType;
import static mx.com.uady.pc_shop.Clients.ClientDetailsActivity.EXTRA_ID;

public class ClientActivity extends AppCompatActivity {


    private static final int NEW_NOTE_ACTIVITY_REQUEST_CODE = 1;
    public static final int UPDATE_NOTE_ACTIVITY_REQUEST_CODE = 2;
    private MultiSelectionSpinner multiSpinner;
    private Button spinner;
    private EditText editText;
    private ClientModel model;
    private ClientsAdapter adapter;
    private int clients_id;
    private String full_name_client;
    private RecyclerView recyclerView;
    private Editable Query;
    private static  String button;
    private static  String code;
    private FloatingActionButton add_button;
    private static boolean state1;
    private static boolean state2;
    private static boolean state3;
    private static boolean state4 ;
    private Intent intent;
    private Intent intent2;
    private Intent intent3;
    private Context mContext;
    private OnLongItemClickListner listener;
    private List<Clients> clients1;
    private Clients client1;
    private int position;
    private OrdersModel model2;
    //private  static Boolean deletestate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        Stetho.initializeWithDefaults(this);
        Toolbar toolbar = findViewById(R.id.toolbar_clients);

        setTitle("Clientes");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Stetho.initializeWithDefaults(this);
        adapter = new ClientsAdapter();

        recyclerView = findViewById(R.id.recyler_view_clients);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        spinner = findViewById(R.id.multi_spinner);
        //spinner = findViewById(R.id.type_choose);
        editText = findViewById(R.id.edit_text_to_query_clients);
        add_button = findViewById(R.id.add_button);
        model = ViewModelProviders.of(this).get(ClientModel.class);
        model2 = ViewModelProviders.of(this).get(OrdersModel.class);




        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ClientActivity.this, AddClientActivity.class);
                startActivityForResult(intent,NEW_NOTE_ACTIVITY_REQUEST_CODE);

            }
        });

        Query = editText.getText();
        registerForContextMenu(recyclerView);


    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Opciones: ");
        menu.add(0, 123, 0, "Detalles");
        menu.add(0, 456, 1, "Editar");
        /*model2.getclientsordersbyid(client1.getClientId()).observe(this, new Observer<List<Orders_Clients>>() {
            @Override
            public void onChanged(@Nullable List<Orders_Clients> orders_clients) {
                for(Orders_Clients orders_clients1: orders_clients){
                    if(orders_clients1.getStatusid() == 3||orders_clients1.getStatusid() == 4){
                        deletestate = true;
                        break;
                    }else{
                        deletestate = false;
                    }
                }
            }
        });
        if(!deletestate){*/
            menu.add(0, 789, 2, "Eliminar");
        //}
        menu.setHeaderIcon(R.drawable.ic_storage_black_24dp);

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_search:

                if(state1){
                    model.getClientesByFirstName("%" + Query + "%").observe(ClientActivity.this,
                            new Observer<List<Clients>>() {
                                @Override
                                public void onChanged(@Nullable final List<Clients> clients) {
                                    adapter.submitList(clients);
                                    adapter.setOnLongItemClickListner(new ClientsAdapter.OnLongItemClickListner() {
                                        @Override
                                        public void OnLongClick(Clients clients, View view) {
                                            intent = new Intent(ClientActivity.this, ClientDetailsActivity.class);
                                            intent.putExtra(EXTRA_ID, clients.getClientId());
                                            intent2 = new Intent(ClientActivity.this, EditClientActivity.class);
                                            intent2.putExtra(EditClientActivity.EXTRA_ID,clients.getClientId());
                                            client1 = clients;
                                            openContextMenu(view);
                                        }
                                    });

                                }
                            });



                }else if(state2){
                    model.getClientesByLastName("%" + Query + "%").observe(ClientActivity.this,
                            new Observer<List<Clients>>() {
                                @Override
                                public void onChanged(@Nullable final List<Clients> clients) {
                                    adapter.submitList(clients);
                                    adapter.setOnLongItemClickListner(new ClientsAdapter.OnLongItemClickListner() {
                                        @Override
                                        public void OnLongClick(Clients clients, View view) {
                                            intent = new Intent(ClientActivity.this, ClientDetailsActivity.class);
                                            intent.putExtra(EXTRA_ID, clients.getClientId());
                                            intent2 = new Intent(ClientActivity.this, EditClientActivity.class);
                                            intent2.putExtra(EditClientActivity.EXTRA_ID,clients.getClientId());
                                            client1 = clients;
                                            openContextMenu(view);
                                        }
                                    });

                                }
                            });

                }else if(state3){
                    model.getClientesByEmail("%" + Query + "%").observe(ClientActivity.this,
                            new Observer<List<Clients>>() {
                                @Override
                                public void onChanged(@Nullable final List<Clients> clients) {
                                    adapter.submitList(clients);
                                    adapter.setOnLongItemClickListner(new ClientsAdapter.OnLongItemClickListner() {
                                        @Override
                                        public void OnLongClick(Clients clients, View view) {
                                            intent = new Intent(ClientActivity.this, ClientDetailsActivity.class);
                                            intent.putExtra(EXTRA_ID, clients.getClientId());
                                            intent2 = new Intent(ClientActivity.this, EditClientActivity.class);
                                            intent2.putExtra(EditClientActivity.EXTRA_ID,clients.getClientId());
                                            client1 = clients;
                                            openContextMenu(view);
                                        }
                                    });

                                }
                            });

                }else if(state4){
                    model.getCLientesByNumber("%" + Query + "%").observe(ClientActivity.this,
                            new Observer<List<Clients>>() {
                                @Override
                                public void onChanged(@Nullable final List<Clients> clients) {
                                    adapter.submitList(clients);
                                    adapter.setOnLongItemClickListner(new ClientsAdapter.OnLongItemClickListner() {
                                        @Override
                                        public void OnLongClick(Clients clients, View view) {
                                            intent = new Intent(ClientActivity.this, ClientDetailsActivity.class);
                                            intent.putExtra(EXTRA_ID, clients.getClientId());
                                            intent2 = new Intent(ClientActivity.this, EditClientActivity.class);
                                            intent2.putExtra(EditClientActivity.EXTRA_ID,clients.getClientId());
                                            client1 = clients;

                                            openContextMenu(view);
                                        }
                                    });

                                }
                            });

                }else{
                    if((Query == null || Query.toString().isEmpty()&&state4||state1||state2||state3)
                            ||(Query == null || Query.toString().isEmpty()&&!state4||!state1||!state2||!state3)
                            ||(Query == null || Query.toString().isEmpty()&&state4||!state1||!state2||!state3)
                            ||(Query == null || Query.toString().isEmpty()&&state4||state1||!state2||!state3)
                            ||(Query == null || Query.toString().isEmpty()&&state4||state1||state2||!state3)
                            ||(Query == null || Query.toString().isEmpty()&&!state4||state1||!state2||!state3)
                            ||(Query == null || Query.toString().isEmpty()&&!state4||state1||state2||!state3)
                            ||(Query == null || Query.toString().isEmpty()&&state4||state1||!state2||state3)){
                        model.getAllClients().observe(ClientActivity.this, new Observer<List<Clients>>() {
                            @Override
                            public void onChanged(@Nullable List<Clients> clients) {
                                adapter.submitList(clients);
                                adapter.setOnLongItemClickListner(new ClientsAdapter.OnLongItemClickListner() {
                                    @Override
                                    public void OnLongClick(Clients clients, View view) {
                                        intent = new Intent(ClientActivity.this, ClientDetailsActivity.class);
                                        intent.putExtra(EXTRA_ID, clients.getClientId());
                                        intent2 = new Intent(ClientActivity.this, EditClientActivity.class);
                                        intent2.putExtra(EditClientActivity.EXTRA_ID,clients.getClientId());
                                        client1 = clients;
                                        openContextMenu(view);
                                    }
                                });
                            }
                        });
                    }else{
                        model.getAllClients().observe(ClientActivity.this, new Observer<List<Clients>>() {
                            @Override
                            public void onChanged(@Nullable List<Clients> clients) {
                                adapter.submitList(clients);
                                adapter.setOnLongItemClickListner(new ClientsAdapter.OnLongItemClickListner() {
                                    @Override
                                    public void OnLongClick(Clients clients, View view) {
                                        intent = new Intent(ClientActivity.this, ClientDetailsActivity.class);
                                        intent.putExtra(EXTRA_ID, clients.getClientId());
                                        intent2 = new Intent(ClientActivity.this, EditClientActivity.class);
                                        intent2.putExtra(EditClientActivity.EXTRA_ID,clients.getClientId());
                                        client1 = clients;
                                        openContextMenu(view);
                                    }
                                });
                            }
                        });
                    }

                }

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }




    public void OnTopicChange(View v){

        final int aIndex = 0;
        final int bIndex = 1;
        final int cIndex = 2;
        final int dIndex = 3;
        final CharSequence[] items = {"Nombre" ,
                "Apellido" ,
                "Email" ,
                "Numero   "
                };

        final Boolean[] state = new Boolean[items.length];
        state[aIndex] = false;
        state[bIndex] = false;
        state[cIndex] = false;
        state[dIndex] = false;

        model.getGetAlClientsCheckBox().observe(ClientActivity.this, new Observer<List<ClientsCheckBox>>() {
            @Override
            public void onChanged(@Nullable List<ClientsCheckBox> clientsCheckBoxes) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ClientActivity.this);
                builder.setTitle("Opciones");

                builder.setMultiChoiceItems(items, new boolean[]{state1,state2,state3,state4},
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                button = "";
                                state[which] = isChecked;
                                if (state[0]) {
                                    Toast.makeText(ClientActivity.this, "Ha seleccionado Nombre", Toast.LENGTH_SHORT).show();
                                    button += "nombre, ";
                                    state1 = true;
                                }else{state1 = false;}
                                if (state[1]) {
                                    Toast.makeText(ClientActivity.this, "Ha seleccionado Apellido", Toast.LENGTH_SHORT).show();
                                    button += "Apellido, ";
                                    state2 = true;
                                }else{state2 = false;}
                                if (state[2]) {
                                    Toast.makeText(ClientActivity.this, "Ha seleccionado Email", Toast.LENGTH_SHORT).show();
                                    button += "Email, ";
                                    state3 = true;
                                }
                                if (state[3]) {
                                    Toast.makeText(ClientActivity.this, "Ha seleccionado Numero", Toast.LENGTH_SHORT).show();
                                    button += "Numero, ";
                                    state4 = true;
                                }else{state4 = false;}

                            }});

                builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        spinner.setText(button);
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });


    }
    public  interface  OnLongItemClickListner{
        void OnLongClick(Clients clients, View view);
    }

    public void setOnLongItemClickListner(OnLongItemClickListner Llistner){
        this.listener = Llistner;
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);


    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 123:
                startActivityForResult(intent,ClientActivity.NEW_NOTE_ACTIVITY_REQUEST_CODE);
                customType(ClientActivity.this, "bottom-to-up");
                break;
            case 456:
                startActivityForResult(intent2, ClientActivity.UPDATE_NOTE_ACTIVITY_REQUEST_CODE);
                customType(ClientActivity.this, "bottom-to-up");
                break;
            case 789:
                AlertDialog.Builder builder = new AlertDialog.Builder(ClientActivity.this,R.style.AlertDialog);
                builder.setTitle("Confirmación");
                builder.setMessage("Esta seguro de eliminar este cliente");
                builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        model.delete(client1);
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            default:
                break;
        }
        return super.onContextItemSelected(item);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_NOTE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {

            // Code to insert note
            int id =data.getIntExtra(AddClientActivity.id2,0);
            String firstName= data.getStringExtra(AddClientActivity.first_name2);
            String lastName = data.getStringExtra(AddClientActivity.last_name2);
            String phone1_2= data.getStringExtra(AddClientActivity.phone12) ;
            String phone2_2=data.getStringExtra(AddClientActivity.phone22);
            String phone3_2=data.getStringExtra(AddClientActivity.phone32);
            String phone1_5= data.getStringExtra(AddClientActivity.phone12_5) ;
            String phone2_5=data.getStringExtra(AddClientActivity.phone22_5);
            String phone3_5=data.getStringExtra(AddClientActivity.phone32_5);
            String email =data.getStringExtra(AddClientActivity.email12);
            String direccion = data.getStringExtra(AddClientActivity.direccion1);
            String phone1;
            if(phone1_5.equals("")&&phone1_2.equals("")){
                phone1 = "";
            }else{
                phone1 = phone1_5+"-"+phone1_2;
            }


            String phone2;
            if(phone2_5.equals("")&&phone2_2.equals("")){
                phone2 = "";
            }else{
                phone2 = phone2_5+"-"+phone2_2;
            }

            String phone3;
            if(phone3_5.equals("")&&phone3_2.equals("")){
                phone3 = "";
            }else{
                phone3 = phone3_5+"-"+phone3_2;
            }
            Clients clients = new Clients(id, firstName,lastName,direccion, phone1, phone2, phone3, email);
            model.insert(clients);

            Toast.makeText(
                    getApplicationContext(),
                    "Cliente guardado"
,                    Toast.LENGTH_LONG).show();

        }
        else if (requestCode == UPDATE_NOTE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {

            // Code to update the note
            int id =data.getIntExtra(EditClientActivity.id,0);
            String firstName= data.getStringExtra(EditClientActivity.first_name);
            String lastName = data.getStringExtra(EditClientActivity.last_name);
            String phone1_3= data.getStringExtra(EditClientActivity.phone1) ;
            String phone2_3=data.getStringExtra(EditClientActivity.phone2);
            String phone3_3=data.getStringExtra(EditClientActivity.phone3);
            String phone1_5= data.getStringExtra(EditClientActivity.phone1_5) ;
            String phone2_5=data.getStringExtra(EditClientActivity.phone2_5);
            String phone3_5=data.getStringExtra(EditClientActivity.phone3_5);
            String email =data.getStringExtra(EditClientActivity.email1);
            String direccion  =data.getStringExtra(EditClientActivity.direccion1);
            String phone1;
            if(phone1_5.equals("")&&phone1_3.equals("")){
                phone1 = "";
            }else{
                phone1 = phone1_5+"-"+phone1_3;
            }


            String phone2;
            if(phone2_5.equals("")&&phone2_3.equals("")){
                phone2 = "";
            }else{
                phone2 = phone2_5+"-"+phone2_3;
            }

            String phone3;
            if(phone3_5.equals("")&&phone3_3.equals("")){
                phone3 = "";
            }else{
                phone3 = phone3_5+"-"+phone3_3;
            }


            Clients clients = new Clients(id, firstName,lastName,direccion, phone1, phone2, phone3, email);
            model.update(clients);

            Toast.makeText(
                    getApplicationContext(),
                    "Cliente Actualizado",
                    Toast.LENGTH_LONG).show();

        } else if(resultCode != RESULT_OK &&requestCode == NEW_NOTE_ACTIVITY_REQUEST_CODE || resultCode != RESULT_OK &&requestCode == UPDATE_NOTE_ACTIVITY_REQUEST_CODE){
            Toast.makeText(
                    getApplicationContext(),
                    "Cliente no editado",
                    Toast.LENGTH_LONG).show();
        }
    }

}
