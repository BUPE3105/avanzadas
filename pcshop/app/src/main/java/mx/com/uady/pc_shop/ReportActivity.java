package mx.com.uady.pc_shop;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import mx.com.uady.pc_shop.Report.MonthlySales;
import mx.com.uady.pc_shop.Report.missingActivity;

import static maes.tech.intentanim.CustomIntent.customType;

public class ReportActivity extends AppCompatActivity {

    private Intent intent;
    private ImageButton buttonMissing;
    private ImageButton monthly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        buttonMissing = findViewById(R.id.button_faltantes);
        buttonMissing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ReportActivity.this, missingActivity.class);
                startActivity(intent);
                customType(ReportActivity.this, "left-to-right");
            }
        });

        monthly = findViewById(R.id.button_ventas);
        monthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ReportActivity.this, MonthlySales.class);
                startActivity(intent);
                customType(ReportActivity.this, "left-to-right");
            }
        });

        Toolbar toolbar= findViewById(R.id.toolbar_report);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Reportes");
        toolbar.setTitleTextColor(Color.WHITE);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
