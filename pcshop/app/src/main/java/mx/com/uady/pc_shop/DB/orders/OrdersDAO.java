package mx.com.uady.pc_shop.DB.orders;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Query;

import java.util.List;

import mx.com.uady.pc_shop.DB.OrderAssemblies.OrderAssemblies;

@Dao
public interface OrdersDAO {



    @Query("select  clients.first_name as FirstName, clients.last_name as LastName, orders.month as Month, orders.year as year,  " +
            "order_status.description as StatusDescription, assemblies.description as assemblies ,month.month as months, orders.day as day " +
            "from orders " +
            "inner join order_assemblies  on (order_assemblies.id=orders.id)" +
            "inner join  clients on (clients.id=orders.customer_id)" +
            "inner join assemblies  on (order_assemblies.assembly_id =assemblies.id)" +
            "inner join order_status  on (orders.status_id = order_status.id) " +
            "inner join  month  on (month.id = orders.month)" +
            "where orders.month = (:monthId)" +
            "order by orders.year")
    LiveData<List<ReportMonthlySalesDetails>> reportSales(int monthId);

    @Query("select  orders.month  as IdMonth, Count (orders.month) as SalesCounter, orders.year as year , month.month as months " +
            "from orders " +
            "inner join order_assemblies  on (order_assemblies.id=orders.id)" +
            "inner join clients  on (clients.id=orders.customer_id)" +
            "inner join assemblies  on (order_assemblies.assembly_id =assemblies.id)" +
            "inner join order_status  on (orders.status_id = order_status.id)" +
            "inner join month  on (orders.month = month.id)" +
            "group by orders.month " +
            "order by orders.year")
    LiveData<List<ReportMonthlySales>> monthlySales ();

    @Query("select  clients.id as Customer_id,clients.first_name as FirstName, clients.last_name as LastName, " +
            "orders.id as order_id, orders.status_id as status_id, orders.month as Month, orders.year as year,orders.day as day," +
            "sum(order_assemblies.qty) as qty_assemblies ,sum(products.price* assembly_products.qty*order_assemblies.qty) as total_price\n" +
            "from orders \n" +
            "inner join clients  on(clients.id = orders.customer_id)\n" +
            "inner join order_assemblies on (order_assemblies.id=orders.id)\n" +
            "inner join assembly_products on (assembly_products.id = order_assemblies.assembly_id)\n" +
            "inner join products  on ( products.id = assembly_products.product_id)\n" +
            "group by orders.id\n" +
            "order by orders.year desc, orders.month desc, orders.day desc")
    LiveData<List<Orders_Clients>> orderclients();

    @Query("select  clients.id as Customer_id,clients.first_name as FirstName, clients.last_name as LastName, " +
            "orders.id as order_id, orders.status_id as status_id, orders.month as Month, orders.year as year,orders.day as day," +
            "sum(order_assemblies.qty) as qty_assemblies ,sum(products.price* assembly_products.qty*order_assemblies.qty) as total_price\n" +
            "from orders \n" +
            "inner join clients  on(clients.id = orders.customer_id)\n" +
            "inner join order_assemblies on (order_assemblies.id=orders.id)\n" +
            "inner join assembly_products on (assembly_products.id = order_assemblies.assembly_id)\n" +
            "inner join products  on ( products.id = assembly_products.product_id)\n" +
            "where orders.customer_id like  (:clientId)\n" +
            "group by orders.id\n" +
            "order by orders.year desc, orders.month desc, orders.day desc")
    LiveData<List<Orders_Clients>> orderclientsbyid(int clientId);

    @Query("select  clients.id as Customer_id,clients.first_name as FirstName, clients.last_name as LastName, " +
            "orders.id as order_id, orders.status_id as status_id, orders.month as Month, orders.year as year,orders.day as day," +
            "sum(order_assemblies.qty) as qty_assemblies ,sum(products.price* assembly_products.qty*order_assemblies.qty) as total_price\n" +
            "from orders \n" +
            "inner join clients  on(clients.id = orders.customer_id)\n" +
            "inner join order_assemblies on (order_assemblies.id=orders.id)\n" +
            "inner join assembly_products on (assembly_products.id = order_assemblies.assembly_id)\n" +
            "inner join products  on ( products.id = assembly_products.product_id)\n" +
            "where orders.status_id like  (:status)\n" +
            "group by orders.id\n" +
            "order by orders.year desc, orders.month desc, orders.day desc")
    LiveData<List<Orders_Clients>> orderclientsbystatus(int status);

    @Query("select \n" +
            "       clients.first_name as first_name,  \n" +
            "       clients.last_name as last_name,\n" +
            "       assemblies.description as descripcion, \n" +
            "       order_assemblies.qty as assembly_qty ," +
            "  assemblies.id as assembly_id," +
            "clients.id as customer_id,\n" +
            "       sum(assembly_products.qty*order_assemblies.qty) as product_assemblies,\n" +
            "       sum(products.price* assembly_products.qty*order_assemblies.qty)/100 as total_price," +
            "order_assemblies.default_id as default_id, \n" +
            "order_assemblies.id as id \n" +
            "            from orders \n" +
            "            inner join clients   on(clients.id = orders.customer_id)\n" +
            "            inner join order_assemblies on (order_assemblies.id=orders.id)\n" +
            "            inner join assembly_products on (assembly_products.id = order_assemblies.assembly_id)\n" +
            "            inner join products  on ( products.id = assembly_products.product_id)\n" +
            "            inner join assemblies  on (order_assemblies.assembly_id =assemblies.id)\n" +
            "            where orders.status_id =  0 and clients.id = (:id)\n" +
            "            group by assemblies.id\n" +
            "            order by  assemblies.description desc ")
    LiveData<List<OrdersInfoEdit>> ordersInfoEdit(int id);


}
