package mx.com.uady.pc_shop.DB.OrderAssemblies;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface OrderAssemblyDAO {

    @Query("SELECT  assemblies.description,products.description as products, products.qty as stock," +
            " sum (assembly_products.qty*order_assemblies.qty)-products.qty as qty  FROM order_assemblies " +
            "inner join assemblies on (order_assemblies.assembly_id = assemblies.id ) " +
            "inner join assembly_products  on ( assemblies.id = assembly_products.id)" +
            "inner join products  on (assembly_products.product_id = products.id)" +
            "inner join orders  on (orders.id = order_assemblies.id)" +
            "where (products.qty < assembly_products.qty*order_assemblies.qty) AND (orders.status_id =0)" +
            "group by products.description " +
            "order by assemblies.description asc")
    LiveData<List<MissingProducts>> missingProducts();

    @Delete
    void delete(OrderAssemblies orderAssemblies);

    @Query("select * from order_assemblies where default_id = :id")
    public LiveData<OrderAssemblies> getByid(int id);

    @Update
    void  update(OrderAssemblies ... orderAssemblies);

    @Insert
    void  insert (OrderAssemblies orderAssemblies);

    @Query("SELECT default_id FROM order_assemblies ORDER BY default_id DESC LIMIT 1")
    LiveData<Integer> getdefaultId ();



    @Query("select order_assemblies.id from orders \n" +
            "inner join clients  on (clients.id = orders.customer_id)\n" +
            "inner join order_assemblies  on (order_assemblies.id = orders.id)\n" +
            "where clients.id = (:id) and orders.status_id = 0\n" +
            "limit 1")
    LiveData<Integer> getNormalId (int id);
 }
