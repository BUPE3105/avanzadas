package mx.com.uady.pc_shop.Orders;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.util.List;

import mx.com.uady.pc_shop.Adapters.AdapterInfoEditOrders;
import mx.com.uady.pc_shop.DB.OrderAssemblies.OrderAssemblies;
import mx.com.uady.pc_shop.DB.orders.OrdersInfoEdit;
import mx.com.uady.pc_shop.R;
import mx.com.uady.pc_shop.ViewModel.OrdersModel;

import static maes.tech.intentanim.CustomIntent.customType;

public class EditOrderActivity extends AppCompatActivity {

    private OrdersModel model;
    private AdapterInfoEditOrders adapter;
    private RecyclerView recyclerView;
    private  int position;
    private  int qty_assembly;
    private int  id_default;
    private  int assembly_id;
    private int id;
    private Intent intent;

    private OrderAssemblies delete;

    private FloatingActionButton fab;

    public static  final String EXTRA_ID ="mx.com.uady.pc_shop.Orders.EXTRA_QTY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_order);
        Toolbar toolbar = findViewById(R.id.toolbar_edit_order);
       fab = findViewById(R.id.add_assembly_orders);
        intent = getIntent();

       fab.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               Intent data = new Intent(EditOrderActivity.this, AddAssemblytoOrderActivity.class);
               data.putExtra("test", intent.getIntExtra(EXTRA_ID, 0));
               startActivity(data);
               customType(EditOrderActivity.this, "left-to-right");
           }
       });
        setSupportActionBar(toolbar);
        setTitle("Editar ordenes");

        setAdapter();

        model = ViewModelProviders.of(this).get(OrdersModel.class);

        model.getInfoEditOrders(intent.getIntExtra(EXTRA_ID, 0)).observe(this, new Observer<List<OrdersInfoEdit>>() {
            @Override
            public void onChanged(@Nullable List<OrdersInfoEdit> ordersInfoEdits) {
                adapter.submitList(ordersInfoEdits);

                adapter.setOnItemClickListener(new AdapterInfoEditOrders.OnItemClicklistener() {
                    @Override
                    public void onItemClick(OrdersInfoEdit ordersInfoEdit, View view) {
                        qty_assembly = ordersInfoEdit.getAssembly_qty();
                        id_default = ordersInfoEdit.getDefault_id();
                        assembly_id = ordersInfoEdit.getAssembly_id();
                        id = ordersInfoEdit.getId();
                        NumberPickerOnDialog();
                    }
                });

                adapter.setOnLongItemClickListner(new AdapterInfoEditOrders.OnLongItemClickListner() {
                    @Override
                    public void OnLongClick(OrdersInfoEdit ordersInfoEdit, View view) {
                        position = ordersInfoEdit.getDefault_id();
                        model.getAllById(position).observe(EditOrderActivity.this, new Observer<OrderAssemblies>() {
                            @Override
                            public void onChanged(@Nullable OrderAssemblies orderAssemblies) {
                                delete = orderAssemblies;
                            }
                        });
                        openContextMenu(view);
                    }
                });
            }
        });
        registerForContextMenu(recyclerView);

    }

    public  void setAdapter(){
        adapter = new AdapterInfoEditOrders();
         recyclerView = findViewById(R.id.edit_order_recycler);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        }else {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

        }
        recyclerView.setAdapter(adapter);
    }

    private  void  NumberPickerOnDialog(){

        final Dialog builder = new Dialog(EditOrderActivity.this);
                    builder.setContentView(R.layout.alert_dialog);
        NumberPicker  test  = builder.findViewById(R.id.dialog_alert_number);
        Button PositiveButton = builder.findViewById(R.id.dialog_alert_positive);
        Button NegativeButton = builder.findViewById(R.id.dialog_alert_negative);
        test.setMaxValue(99);
        test.setMinValue(1);
        test.setValue(qty_assembly);
        test.setWrapSelectorWheel(false);
        NumberPicker.OnValueChangeListener listener = new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                qty_assembly = picker.getValue();
            }
        };
        test.setOnValueChangedListener(listener);

        PositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderAssemblies orderAssemblies = new OrderAssemblies(id_default, id, assembly_id, qty_assembly);
                model.update(orderAssemblies);
                builder.cancel();
            }
        });
        NegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.cancel();
            }
        });
        builder.setTitle("Cuantos se escogieron");
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_second, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                String test = Integer.toString(qty_assembly);
                String test1 = Integer.toString(id_default);
                String test2 = Integer.toString(assembly_id);
                String test3 = Integer.toString(id);
                Toast.makeText(this,test1+test3+test2+test, Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Opciones: ");
        menu.add(0, 451, 0, "Eliminar");
        menu.setHeaderIcon(R.drawable.ic_storage_black_24dp);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 451:
                model.delete(delete);
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }

}
