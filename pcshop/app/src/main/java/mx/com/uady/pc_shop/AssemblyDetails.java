package mx.com.uady.pc_shop;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import org.fabiomsr.moneytextview.MoneyTextView;

import java.util.List;

import mx.com.uady.pc_shop.Adapters.AdapterJoinAssemblies;
import mx.com.uady.pc_shop.DB.AssembliesProducts.AssembliesAndProducts;
import mx.com.uady.pc_shop.DB.AssembliesProducts.AssembliesProductPrice;
import mx.com.uady.pc_shop.ViewModel.AssembliesModel;
import static maes.tech.intentanim.CustomIntent.customType;

public class AssemblyDetails extends AppCompatActivity {

    public static String DATA_DES = "mx.com.uady.pc_shop.DATA_DES";
    public static String DATA_ID = "mx.com.uady.pc_shop.DATA_ID";

    private AdapterJoinAssemblies adapterJoinAssemblies;
    private RecyclerView joinAssembliesproducts;
    private MoneyTextView price;
    private TextView categorias;
    private AssembliesModel model;
    private Intent intent;
    private int total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assembly_details);
       intent = getIntent();
        setAdapter();
        categorias = findViewById(R.id.categoria);
        price = findViewById(R.id.money_text_price_principal);
        Toolbar toolbar=findViewById(R.id.toolbar_details_assembly);
        setTitle("Contenido del ensamblado");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);

        categorias.setText(intent.getStringExtra(DATA_DES));
        registerForContextMenu(joinAssembliesproducts);
        model = ViewModelProviders.of(this).get(AssembliesModel.class);

        model.getProductsAndQty(intent.getIntExtra(DATA_ID,0)).observe(this, new Observer<List<AssembliesAndProducts>>() {
            @Override
            public void onChanged(@Nullable List<AssembliesAndProducts> assembliesAndProducts) {
                adapterJoinAssemblies.submitList(assembliesAndProducts);
                adapterJoinAssemblies.setOnItemClickListener(new AdapterJoinAssemblies.OnItemClicklistener() {
                    @Override
                    public void onItemClick(AssembliesAndProducts assembliesAndProducts, View view) {
                        intent = new Intent(AssemblyDetails.this, DetailsActitvity.class);
                        intent.putExtra(DetailsActitvity.EXTRA_ID,assembliesAndProducts.getId() );
                        openContextMenu(view);
                    }
                });


            }
        });


                model.getPriceTotal(intent.getIntExtra(DATA_ID, 0)).observe(this, new Observer<AssembliesProductPrice>() {
            @Override
            public void onChanged(@Nullable AssembliesProductPrice assembliesProductPrice) {
                total = assembliesProductPrice.getTotal()/100;
                price.setAmount(total,"$");
            }
        });



    }

    public  void setAdapter(){
        adapterJoinAssemblies = new AdapterJoinAssemblies();
        joinAssembliesproducts = findViewById(R.id.recycler_view_assemblies_products);
        joinAssembliesproducts.setLayoutManager(new LinearLayoutManager(this));
        joinAssembliesproducts.setAdapter(adapterJoinAssemblies);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Opciones: ");
        menu.add(0, 123, 0, "Detalles");
        menu.setHeaderIcon(R.drawable.ic_storage_black_24dp);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 123:
                startActivity(intent);
                customType(AssemblyDetails.this, "left-to-right");
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }
}
