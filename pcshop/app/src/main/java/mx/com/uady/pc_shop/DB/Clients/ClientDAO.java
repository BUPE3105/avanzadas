package mx.com.uady.pc_shop.DB.Clients;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import mx.com.uady.pc_shop.DB.Products.Products;

@Dao
public interface ClientDAO {

    @Query("SELECT * from clients ORDER BY first_name Asc")
    LiveData<List<Clients>> getAllClients();

    @Query("Select * from clients where id like (:id)")
    LiveData<Clients> getClientById(int id);

    @Query("Select * from clients where first_name like (:query)  order by first_name Asc")
    LiveData<List<Clients>> getClientsByFirstName(String query);

    @Query("Select * from clients where last_name like (:query)  order by last_name Asc")
    LiveData<List<Clients>> getClientsByLastName(String query);

    @Query("Select * from clients where email like (:query)  order by first_name Asc")
    LiveData<List<Clients>> getClientsByemail(String query);

    @Query("Select * from clients where direccion like (:query)  order by first_name Asc")
    LiveData<List<Clients>> getClientsBydireccion(String query);

    @Query("Select * from clients where phone1 like(:phone )or phone2 like(:phone) or phone3 like (:phone)  order by first_name Asc")
    LiveData<List<Clients>> getClientsByphone( String phone);



    @Update
    void update (Clients clients);
    @Delete
    int delete (Clients clients);
    @Insert
    void insert(Clients clients);
}
