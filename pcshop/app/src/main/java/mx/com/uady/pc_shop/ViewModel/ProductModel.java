package mx.com.uady.pc_shop.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import mx.com.uady.pc_shop.DB.AppRepository;
import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.DB.Products.Products;
import mx.com.uady.pc_shop.DB.Products.ProductsAndCategories;

public class ProductModel extends AndroidViewModel {

    private AppRepository productsrepository;
    private LiveData<List<Products>> getAllProdcuts;

    public ProductModel(@NonNull Application application) {
        super(application);
        productsrepository = new AppRepository(application);
        getAllProdcuts = productsrepository.getALLProducts();
    }

    public  LiveData<List<Products>> getAllProdcuts(){
        return  getAllProdcuts;
    }

    public  LiveData<List<Products>> getByCategoryId(int categoryId){
        return  productsrepository.getByCategoryId(categoryId);
    }


    public  LiveData<List<Products>> getByDescriptionAndCategoryId( String description,int categoryId){
        return productsrepository.getByDescriptionAndCategoryId(description,categoryId);
    }

    public  LiveData<List<Products>> getByDescription(String description){
        return productsrepository.getByDescription(description);
    }
    public  LiveData<ProductsAndCategories> getProductAndCategory(int id){
        return productsrepository.getproductAndCategory(id);
    }



}
