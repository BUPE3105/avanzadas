package mx.com.uady.pc_shop.Adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import mx.com.uady.pc_shop.DB.orders.Orders_Clients;
import mx.com.uady.pc_shop.R;

public class OrdersAdapter extends ListAdapter<Orders_Clients, OrdersAdapter.Holder> {
    private OrdersAdapter.OnItemClicklistener mlistener;
    private OrdersAdapter.OnLongItemClickListner listener;



    public OrdersAdapter() {

        super(DIFF_CALLBACK);
    }


    private static final DiffUtil.ItemCallback<Orders_Clients> DIFF_CALLBACK = new DiffUtil.ItemCallback<Orders_Clients>() {
        @Override
        public boolean areItemsTheSame(@NonNull Orders_Clients oldItem, @NonNull Orders_Clients newItem) {
            return oldItem.getOrderid() == newItem.getOrderid();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Orders_Clients oldItem, @NonNull Orders_Clients newItem) {
            return oldItem.getCustomer_id() == (newItem.getCustomer_id()) &&
                    oldItem.getFirstName().equals(newItem.getFirstName()) &&
                    oldItem.getLastName().equals(newItem.getLastName())
                    && oldItem.getYear()==(newItem.getYear())
                    && oldItem.getMonth()== (newItem.getMonth())
                    && oldItem.getDay()==(newItem.getDay())
                    && oldItem.getStatusid()==(newItem.getStatusid())
                    && oldItem.getOrderid()== (newItem.getOrderid())
                    && oldItem.getTotalprice()==(newItem.getTotalprice());
        }
    };

    @NonNull
    @Override
    public OrdersAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_orders
                , parent, false);
        return new OrdersAdapter.Holder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull OrdersAdapter.Holder holder, int position) {
        Orders_Clients current = getItem(position);

        switch (current.getStatusid()) {
            case 0:
                holder.state.setTextColor(Color.parseColor("#ff8f00"));
                holder.state.setText("Pendiente");
                break;
            case 1:
                holder.state.setTextColor(Color.parseColor("#FF3F3F"));
                holder.state.setText("Cancelado");
                break;
            case 2:
                holder.state.setTextColor(Color.parseColor("#67FF5B"));
                holder.state.setText("Confirmado");
                break;
            case 3:
                holder.state.setTextColor(Color.parseColor("#E0BA28"));
                holder.state.setText("En tránsito");
                break;
            case 4:
                holder.state.setText("Finalizado");
            default:
                break;}
        holder.client.setText(current.getFirstName() + " " + current.getLastName());
        int number = current.getTotalprice()/100;
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator('\'');
        symbols.setDecimalSeparator(',');

        DecimalFormat decimalFormat = new DecimalFormat("$ #,###.00", symbols);
        String precio  = decimalFormat.format(number);
        holder.cost.setText("Costo: "+precio);
        String day = Integer.toString(current.getDay());
        String month = Integer.toString(current.getMonth());
        String year = Integer.toString(current.getYear());
        holder.date.setText(day +"/"+month+"/"+year);
        String assembliesqty = Integer.toString(current.getQty_assemblies());
        holder.assemblies.setText("Ensambles: "+assembliesqty);



    }




    class Holder extends RecyclerView.ViewHolder {

        private TextView client;
        private TextView state;
        private TextView date;
        private TextView assemblies;
        private TextView cost;

        public Holder(@NonNull final View itemView) {
            super(itemView);
            client = itemView.findViewById(R.id.client_order);
            state = itemView.findViewById(R.id.state_order);
            date = itemView.findViewById(R.id.date_order);
            assemblies = itemView.findViewById(R.id.assembly_order);
            cost = itemView.findViewById(R.id.cost_order);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mlistener != null && position != RecyclerView.NO_POSITION) {
                        mlistener.onItemClick(getItem(position), itemView);
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.OnLongClick(getItem(position), itemView);
                    }
                    return true;
                }
            });

        }
    }

    public interface OnItemClicklistener {
        void onItemClick(Orders_Clients orders_clients, View view);

    }

    public void setOnItemClickListener(OrdersAdapter.OnItemClicklistener clicklistener) {
        this.mlistener = clicklistener;
    }
    public  interface  OnLongItemClickListner{
        void OnLongClick(Orders_Clients orders_clients, View view);
    }

    public void setOnLongItemClickListner(OrdersAdapter.OnLongItemClickListner Llistner){
        this.listener = Llistner;
    }
}
