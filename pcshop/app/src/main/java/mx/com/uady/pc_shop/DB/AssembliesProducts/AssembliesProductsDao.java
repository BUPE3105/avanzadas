package mx.com.uady.pc_shop.DB.AssembliesProducts;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.math.BigDecimal;
import java.util.List;

import mx.com.uady.pc_shop.DB.Assemblies.Assemblies;

@Dao
public interface AssembliesProductsDao {

    @Query("Select * from assembly_products order by id Asc")
    LiveData<List<AssembliesProducts>> getAll();

    @Query("Select assembly_products.qty, products.description, products.id as id  " +
            " from assembly_products " +
            "inner join products on (products.id = assembly_products.product_id)" +
            "where assembly_products.id =(:id)")
    LiveData<List<AssembliesAndProducts>> findProductsAndQty(int id);

    @Query("Select Sum(assembly_products.qty*products.price) as total" +
            " from assembly_products " +
            "inner join products on (products.id = assembly_products.product_id)" +
            "where assembly_products.id =(:id)")
    LiveData<AssembliesProductPrice> findPrice(int id);

    @Query("select assemblies.id as Assembly_Id, assemblies.description as Assembly, sum(assembly_products.qty) as Product_qty, " +
            "sum(assembly_products.qty*products.price) as Assembly_price\n" +
            "from assemblies \n" +
            "inner join assembly_products on (assembly_products.id = assemblies.id)\n" +
            "inner join products  on (products.id = assembly_products.product_id)\n" +
            "group by assemblies.id")
    LiveData<List<Asemblies_Productos_Qty_Price>> Assembliesproductqtyprice();






}
