package mx.com.uady.pc_shop.DB.orders;

import android.arch.persistence.room.ColumnInfo;
import android.support.annotation.NonNull;

public class ReportMonthlySales {
    @ColumnInfo(name = "IdMonth")
    private  int id;
    @ColumnInfo(name = "SalesCounter")
    private int SalesCounter;
    @ColumnInfo(name = "year")
    private int year;
    @ColumnInfo(name = "months")
    private String month;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSalesCounter() {
        return SalesCounter;
    }

    public void setSalesCounter(int salesCounter) {
        SalesCounter = salesCounter;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

@NonNull
@Override
    public String toString() {
        return  month;
    }
}
