package mx.com.uady.pc_shop.DB.months;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "month")
public class Months {
    @PrimaryKey
    private  int id;
    @ColumnInfo(name = "month")
    private String month;

    public Months(int id, String month) {
        this.id = id;
        this.month = month;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

@NonNull
@Override
    public String toString() {
        return month;
    }
}
