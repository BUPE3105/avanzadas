package mx.com.uady.pc_shop.Adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import mx.com.uady.pc_shop.DB.Assemblies.Assemblies;
import mx.com.uady.pc_shop.DB.Assemblies.AssemblyProductsPrice;
import mx.com.uady.pc_shop.R;

public class AssembliesAdapter extends ListAdapter<AssemblyProductsPrice, AssembliesAdapter.Holder> {

    private OnItemClicklistener mlistener;

    private  OnLongItemClickListner listener;

    public AssembliesAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<AssemblyProductsPrice> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<AssemblyProductsPrice>() {
                @Override
                public boolean areItemsTheSame(@NonNull AssemblyProductsPrice oldItem, @NonNull AssemblyProductsPrice newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull AssemblyProductsPrice oldItem, @NonNull AssemblyProductsPrice newItem) {
                    return oldItem.getId() == (newItem.getId()) &&
                            oldItem.getDescripcion().equals(newItem.getDescripcion()) &&
                            oldItem.getContador() ==( newItem.getContador()) &&
                            oldItem.getPrecio() == (newItem.getPrecio());
                }
            };

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.assemblies_item,
                parent,false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        AssemblyProductsPrice current =  getItem(position);
        holder.descripcion.setText(current.getDescripcion());

        int number = current.getPrecio()/100;
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator('\'');
        symbols.setDecimalSeparator(',');

        DecimalFormat decimalFormat = new DecimalFormat("$ #,###.00", symbols);
        String precio  = decimalFormat.format(number);
        holder.price.setText("Precio: "+precio);

        String count = Integer.toString(current.getContador());
        holder.count.setText("Cantidad de productos: "+count);

    }

    public AssemblyProductsPrice getAssembliesAt(int position) {
        return getItem(position);
    }

    class Holder extends RecyclerView.ViewHolder {

        private TextView descripcion;
        private TextView price;
        private TextView count;

        public Holder(@NonNull final View itemView) {
            super(itemView);
            descripcion = itemView.findViewById(R.id.recycler_text_view_assemblies);
            count = itemView.findViewById(R.id.recycler_text_view_assemblies_count);
            price = itemView.findViewById(R.id.recycler_text_view_assemblies_price);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mlistener != null && position != RecyclerView.NO_POSITION) {
                        mlistener.onItemClick(getItem(position), itemView);
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.OnLongClick(getItem(position), itemView);
                    }
                    return true;
                }
            });

        }

    }

    @Override
    public void onViewRecycled(@NonNull Holder holder) {
        super.onViewRecycled(holder);
    }


    public interface OnItemClicklistener {
        void onItemClick(AssemblyProductsPrice assemblyProductsPrice, View view);

    }

    public void setOnItemClickListener(OnItemClicklistener clicklistener) {
        this.mlistener = clicklistener;
    }

    public  interface  OnLongItemClickListner{
        void OnLongClick(AssemblyProductsPrice assemblyProductsPrice, View view);
    }

    public void setOnLongItemClickListner(OnLongItemClickListner Llistner){
        this.listener = Llistner;
    }

}
