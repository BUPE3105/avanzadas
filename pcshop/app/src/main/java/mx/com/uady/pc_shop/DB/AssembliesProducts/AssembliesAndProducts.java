package mx.com.uady.pc_shop.DB.AssembliesProducts;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;
import android.support.annotation.NonNull;

import java.util.List;

import mx.com.uady.pc_shop.DB.Assemblies.Assemblies;
import mx.com.uady.pc_shop.DB.AssembliesProducts.AssembliesProducts;
import mx.com.uady.pc_shop.DB.Products.Products;

public  class AssembliesAndProducts {
    public String description;

    public int id;



    public int qty;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    @Override
    public String toString() {
        return description;
    }
}
