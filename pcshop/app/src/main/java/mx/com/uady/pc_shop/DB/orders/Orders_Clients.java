package mx.com.uady.pc_shop.DB.orders;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;


public class Orders_Clients {
    @ColumnInfo(name = "Customer_id")
    private  int customer_id;
    @ColumnInfo(name = "FirstName")
    private  String FirstName;
    @ColumnInfo(name = "LastName")
    private  String LastName;
    @ColumnInfo(name = "day")
    private  int day;
    @ColumnInfo (name = "Month")
    private  int  month;
    @ColumnInfo(name = "year")
    private  int year;
    @ColumnInfo(name = "status_id")
    private  int statusid;
    @ColumnInfo(name = "order_id")
    private  int orderid;
    @ColumnInfo(name = "total_price")
    private  int totalprice;
    @ColumnInfo(name = "qty_assemblies")
    private  int qty_assemblies;

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getStatusid() {
        return statusid;
    }

    public void setStatusid(int statusid) {
        this.statusid = statusid;
    }

    public int getOrderid() {
        return orderid;
    }

    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

    public int getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(int totalprice) {
        this.totalprice = totalprice;
    }

    public int getQty_assemblies() {
        return qty_assemblies;
    }

    public void setQty_assemblies(int qty_assemblies) {
        this.qty_assemblies = qty_assemblies;
    }
}
