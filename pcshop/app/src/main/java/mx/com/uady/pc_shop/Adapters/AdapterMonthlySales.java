package mx.com.uady.pc_shop.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import mx.com.uady.pc_shop.DB.Assemblies.AssemblyProductsPrice;
import mx.com.uady.pc_shop.DB.orders.ReportMonthlySales;
import mx.com.uady.pc_shop.R;

public class AdapterMonthlySales extends ListAdapter<ReportMonthlySales, AdapterMonthlySales.Holder> {

    private OnItemClicklistener mlistener;

    public AdapterMonthlySales() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<ReportMonthlySales> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<ReportMonthlySales>() {
                @Override
                public boolean areItemsTheSame(@NonNull ReportMonthlySales oldItem, @NonNull ReportMonthlySales newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull ReportMonthlySales oldItem, @NonNull ReportMonthlySales newItem) {
                    return oldItem.getId() == (newItem.getId()) &&
                            oldItem.getMonth().equals(newItem.getMonth()) &&
                            oldItem.getSalesCounter() ==( newItem.getSalesCounter()) &&
                            oldItem.getYear() == (newItem.getYear());
                }
            };

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_sales_monthly,
                parent,false);
        return new  Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        ReportMonthlySales current =  getItem(position);

        holder.month.setText(current.getMonth());
        String counter = Integer.toString(current.getSalesCounter());
        holder.salesCounter.setText(counter);

    }

    public ReportMonthlySales getAssembliesAt(int position) {
        return getItem(position);
    }

    class Holder extends RecyclerView.ViewHolder {
        private TextView month;
        private  TextView salesCounter;


        public Holder(@NonNull final View itemView) {
            super(itemView);

            month = itemView.findViewById(R.id.month_sales);
            salesCounter = itemView.findViewById(R.id.counter_sales);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mlistener != null && position != RecyclerView.NO_POSITION) {
                        mlistener.onItemClick(getItem(position), itemView);
                    }
                }
            });

        }

    }

    @Override
    public void onViewRecycled(@NonNull Holder holder) {
        super.onViewRecycled(holder);
    }


    public interface OnItemClicklistener {
        void onItemClick(ReportMonthlySales reportMonthlySales, View view);

    }

    public void setOnItemClickListener(OnItemClicklistener clicklistener) {
        this.mlistener = clicklistener;
    }




}
