package mx.com.uady.pc_shop.DB.Clients;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "clients_checkbox")
public class ClientsCheckBox {
    @PrimaryKey
    @ColumnInfo(name = "id")
    @NonNull
    private  int client_checkbox_Id;
    @ColumnInfo(name = "name")
    @NonNull
    private  String name;
    @ColumnInfo(name = "state")
    @NonNull
    private  Boolean state;

    public ClientsCheckBox(int client_checkbox_Id, @NonNull String name, @NonNull Boolean state) {
        this.client_checkbox_Id = client_checkbox_Id;
        this.name = name;
        this.state = state;
    }

    public int getClient_checkbox_Id() {
        return client_checkbox_Id;
    }

    public void setClient_checkbox_Id(int client_checkbox_Id) {
        this.client_checkbox_Id = client_checkbox_Id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public Boolean getState() {
        return state;
    }

    public void setState(@NonNull Boolean state) {
        this.state = state;
    }
}
