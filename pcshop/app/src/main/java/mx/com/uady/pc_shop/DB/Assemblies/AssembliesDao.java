package mx.com.uady.pc_shop.DB.Assemblies;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;
@Dao
public interface  AssembliesDao {

    @Query("SELECT * from assemblies ORDER BY description Asc")
     LiveData<List<Assemblies>> getAllAssemblies();

    @Query("DELETE FROM assemblies")
    void deleteAll();

    @Query("Select * from assemblies where description like (:query) order by description Asc ")
    LiveData<List<Assemblies>> getByDescription(String query);

    @Query("Select assemblies.id as id,assemblies.description as descripcion,sum(products.price * assembly_products.qty) as precio, Count(assembly_products.id)as contador from assemblies\n" +
            "inner join  assembly_products  on (assemblies.id = assembly_products.id)\n" +
            "inner join products  on (products.id=assembly_products.product_id)\n" +
            "where assemblies.description like (:query)\n" +
            "group by assemblies.description\n" +
            "order by assemblies.description Asc ")
    LiveData<List<AssemblyProductsPrice>> DescriptionAndPriceByString(String query);

    @Query("Select assemblies.id as id, assemblies.description as descripcion,sum(products.price * assembly_products.qty) as precio, Count(assembly_products.id)as contador from assemblies\n" +
            "inner join  assembly_products  on (assemblies.id = assembly_products.id)\n" +
            "inner join products  on (products.id=assembly_products.product_id)\n" +
            "group by assemblies.description\n" +
            "order by assemblies.description Asc ")
    LiveData<List<AssemblyProductsPrice>> DescriptionAndPrice();

}
