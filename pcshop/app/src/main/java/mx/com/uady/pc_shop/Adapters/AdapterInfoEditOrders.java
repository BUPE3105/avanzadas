package mx.com.uady.pc_shop.Adapters;

import android.app.Application;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import mx.com.uady.pc_shop.DB.AssembliesProducts.AssembliesAndProducts;
import mx.com.uady.pc_shop.DB.orders.OrdersInfoEdit;
import mx.com.uady.pc_shop.Orders.EditOrderActivity;
import mx.com.uady.pc_shop.R;

public class AdapterInfoEditOrders extends ListAdapter<OrdersInfoEdit, AdapterInfoEditOrders.Holder> {

    public AdapterInfoEditOrders() {
        super(DIFF_CALLBACK);
    }


    private OnLongItemClickListner listener;

    private OnItemClicklistener mlistener;

    private OrdersInfoEdit current;

    private static final DiffUtil.ItemCallback<OrdersInfoEdit> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<OrdersInfoEdit>() {
                @Override
                public boolean areItemsTheSame(@NonNull OrdersInfoEdit oldItem, @NonNull OrdersInfoEdit newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull OrdersInfoEdit oldItem, @NonNull OrdersInfoEdit newItem) {
                    return oldItem.getTotal_price() == (newItem.getTotal_price()) &&
                            oldItem.getFirst_name().equals(newItem.getFirst_name())&&
                            oldItem.getId() == (newItem.getId());
                }
            };

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_edit_order_assemblies,
                parent,false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int position) {
         current =  getItem(position);

        holder.customerName.setText(current.getFirst_name() + " "+ current.getLast_name());

        holder.assembly.setText(current.getDescripcion());

        String qty = Integer.toString(current.getProduct_assemblies());

        holder.productQty.setText("Contiene: "+qty+ " productos");

        int number = current.getTotal_price();
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator('\'');
        symbols.setDecimalSeparator(',');

        DecimalFormat decimalFormat = new DecimalFormat("$ #,###.00", symbols);
        String precio  = decimalFormat.format(number);
        holder.price.setText("cuesta: "+precio);

        String qty_assembly = Integer.toString(current.getAssembly_qty());

        holder.qty_assembly.setText("Se pidio: "+qty_assembly);
    }


    class Holder extends RecyclerView.ViewHolder{

        private TextView customerName;
        private TextView assembly;
        private TextView productQty;
        private TextView price;
        private TextView qty_assembly;

        public Holder(@NonNull final View itemView) {
            super(itemView);

            customerName = itemView.findViewById(R.id.edit_orders_name);
            assembly = itemView.findViewById(R.id.edit_orders_assembly);
            productQty = itemView.findViewById(R.id._edit_orders_products);
            price = itemView.findViewById(R.id.edit_orders_price);
            qty_assembly = itemView.findViewById(R.id.edit_order_assembly_qty);




            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mlistener != null && position != RecyclerView.NO_POSITION) {
                        mlistener.onItemClick(getItem(position), itemView);
                    }
                }
            });





            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.OnLongClick(getItem(position), itemView );
                    }
                    return true;
                }
            });

        }
    }



    public  interface  OnLongItemClickListner{
        void OnLongClick(OrdersInfoEdit ordersInfoEdit, View view);
    }

    public interface OnItemClicklistener {
        void onItemClick(OrdersInfoEdit ordersInfoEdit, View view );

    }

    public void setOnItemClickListener(OnItemClicklistener clicklistener) {
        this.mlistener = clicklistener;
    }

    public void setOnLongItemClickListner(OnLongItemClickListner Llistner){
        this.listener = Llistner;
    }




}
