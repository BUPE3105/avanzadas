package com.uady.hadp.pcshop.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import com.uady.hadp.pcshop.DB.AppRepository;
import com.uady.hadp.pcshop.DB.Assemblies.Assemblies;
import com.uady.hadp.pcshop.DB.AssembliesProducts.AssembliesAndProducts;
import com.uady.hadp.pcshop.DB.AssembliesProducts.AssembliesProductPrice;
import com.uady.hadp.pcshop.DB.OrderAssemblies.MissingProducts;

public class AssembliesModel extends AndroidViewModel {

    private AppRepository assembliesRepository;
    private AppRepository assembliesProductsRepository;
    private LiveData<List<Assemblies>> AllAsemblies;

    public AssembliesModel(@NonNull Application application) {
        super(application);
        assembliesRepository = new AppRepository(application);
        assembliesProductsRepository = new AppRepository(application);
        AllAsemblies = assembliesRepository.getAllAssemblies();
    }

    public LiveData<List<Assemblies>> getAllAsemblies(){
        return  AllAsemblies;
    }

    public  LiveData<List<Assemblies>> getAllAsembliesDescription(String query){
        return assembliesRepository.getAssembliesDescription(query);
    }

    public LiveData<List<AssembliesAndProducts>> getProductsAndQty(int id) {
        return assembliesProductsRepository.getProductsAndQty(id);
    }

    public LiveData<AssembliesProductPrice> getPriceTotal(int id) {
        return assembliesProductsRepository.getPriceTotal(id);
    }

    public LiveData<List<MissingProducts>> getMissiproducts(){
        return assembliesProductsRepository.getMissingProducts();
    }

}
