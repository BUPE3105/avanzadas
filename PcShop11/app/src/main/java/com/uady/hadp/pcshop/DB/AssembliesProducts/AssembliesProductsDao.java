package com.uady.hadp.pcshop.DB.AssembliesProducts;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface AssembliesProductsDao {

    @Query("Select * from assembly_products order by id Asc")
    LiveData<List<AssembliesProducts>> getAll();

    @Query("Select assembly_products.qty, products.description, products.id as id  " +
            " from assembly_products " +
            "inner join products on (products.id = assembly_products.product_id)" +
            "where assembly_products.id =(:id)")
    LiveData<List<AssembliesAndProducts>> findProductsAndQty(int id);

    @Query("Select Sum(assembly_products.qty*products.price) as total" +
            " from assembly_products " +
            "inner join products on (products.id = assembly_products.product_id)" +
            "where assembly_products.id =(:id)")
    LiveData<AssembliesProductPrice> findPrice(int id);





}
