package com.uady.hadp.pcshop.DB.Products;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "products")
public class Products {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private  int productId;
    @ColumnInfo(name = "category_id")
    private  int categoryId;
    @ColumnInfo(name = "description")
    @NonNull
    private  String description;
    @ColumnInfo(name = "price")
    private int price;
    @ColumnInfo(name =  "qty")
    private int qty;

    public Products(int productId, int categoryId, @NonNull String description, int price, int qty) {
        this.productId = productId;
        this.categoryId = categoryId;
        this.description = description;
        this.price = price;
        this.qty = qty;
    }


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @NonNull
    @Override
    public String toString() {
        return description;
    }
}
