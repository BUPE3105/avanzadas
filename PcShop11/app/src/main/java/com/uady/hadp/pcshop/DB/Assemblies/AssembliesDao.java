package com.uady.hadp.pcshop.DB.Assemblies;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface  AssembliesDao {

    @Query("SELECT * from assemblies ORDER BY description Asc")
     LiveData<List<Assemblies>> getAllAssemblies();

    @Query("DELETE FROM assemblies")
    void deleteAll();

    @Query("Select * from assemblies where description like (:query) order by description Asc ")
    LiveData<List<Assemblies>> getByDescription(String query);

}
