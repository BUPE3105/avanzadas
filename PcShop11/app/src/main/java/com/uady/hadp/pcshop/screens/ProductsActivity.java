package com.uady.hadp.pcshop.screens;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.facebook.stetho.Stetho;

import java.util.List;

import com.uady.hadp.pcshop.Adapters.ProductsAdapter;
import com.uady.hadp.pcshop.DB.Categories.Categories;
import com.uady.hadp.pcshop.DB.Products.Products;
import com.uady.hadp.pcshop.R;
import com.uady.hadp.pcshop.ViewModel.CategoriesModel;
import com.uady.hadp.pcshop.ViewModel.ProductModel;
import com.uady.hadp.pcshop.detailsactivitys.DetailsActitvity;

import static maes.tech.intentanim.CustomIntent.customType;

public class ProductsActivity extends AppCompatActivity {

    private CategoriesModel model;
    private ProductModel productModel;
    private Spinner spinner;
    private ProductsAdapter adapter;
    private EditText editText;
    private RecyclerView recyclerView;
    private int category_id;
    private Editable Query;
    private String DescriptionCategory;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        Stetho.initializeWithDefaults(this);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setTitle("Productos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setAdapter();

        spinner = findViewById(R.id.category_choose);
        editText = findViewById(R.id.edit_text_to_query);


        model = ViewModelProviders.of(this).get(CategoriesModel.class);
        productModel = ViewModelProviders.of(this).get(ProductModel.class);

        model.getALLCategories().observe(ProductsActivity.this, new Observer<List<Categories>>() {
            @Override
            public void onChanged(@Nullable final List<Categories> categories) {
                ArrayAdapter<Categories> categoriesArrayAdapter = new ArrayAdapter<>(ProductsActivity.this,
                        R.layout.support_simple_spinner_dropdown_item,
                        categories);
                categoriesArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                spinner.setAdapter(categoriesArrayAdapter);

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Categories parentSelectedItem = (Categories) parent.getSelectedItem();
                        category_id = parentSelectedItem.getId();
                        DescriptionCategory = parentSelectedItem.getDescription();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });

        Query = editText.getText();
        registerForContextMenu(recyclerView);


    }

    void setAdapter() {
        adapter = new ProductsAdapter();

        recyclerView = findViewById(R.id.recyler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_search:


                if (category_id < 7) {
                    if (Query == null || Query.toString().isEmpty()) {
                        productModel.getByCategoryId(category_id).observe(ProductsActivity.this,
                                new Observer<List<Products>>() {
                                    @Override
                                    public void onChanged(@Nullable final List<Products> products) {
                                        adapter.submitList(products);
                                        adapter.setOnItemClickListener(new ProductsAdapter.OnItemClicklistener() {
                                            @Override
                                            public void onItemClick(Products products, View view) {
                                                intent = new Intent(ProductsActivity.this, DetailsActitvity.class);
                                                intent.putExtra(DetailsActitvity.EXTRA_ID, products.getProductId());
                                                openContextMenu(view);
                                            }
                                        });
                                    }
                                });
                    } else {
                        productModel.getByDescriptionAndCategoryId("%" + Query + "%", category_id).observe(ProductsActivity.this,
                                new Observer<List<Products>>() {
                                    @Override
                                    public void onChanged(@Nullable final List<Products> products) {
                                        adapter.submitList(products);
                                        adapter.setOnItemClickListener(new ProductsAdapter.OnItemClicklistener() {
                                            @Override
                                            public void onItemClick(Products products, View view) {
                                                intent = new Intent(ProductsActivity.this, DetailsActitvity.class);
                                                intent.putExtra(DetailsActitvity.EXTRA_ID, products.getProductId());
                                                openContextMenu(view);
                                            }
                                        });
                                    }
                                });
                    }
                } else {
                    if (Query == null || Query.toString().isEmpty()) {
                        productModel.getAllProdcuts().observe(ProductsActivity.this,
                                new Observer<List<Products>>() {
                                    @Override
                                    public void onChanged(@Nullable final List<Products> products) {
                                        adapter.submitList(products);
                                        adapter.setOnItemClickListener(new ProductsAdapter.OnItemClicklistener() {
                                            @Override
                                            public void onItemClick(Products products, View view) {
                                                intent = new Intent(ProductsActivity.this, DetailsActitvity.class);
                                                intent.putExtra(DetailsActitvity.EXTRA_ID, products.getProductId());
                                                openContextMenu(view);

                                            }
                                        });
                                    }
                                });
                    } else {
                        productModel.getByDescription("%" + Query + "%").observe(ProductsActivity.this,
                                new Observer<List<Products>>() {
                                    @Override
                                    public void onChanged(@Nullable final List<Products> products) {
                                        adapter.submitList(products);
                                        adapter.setOnItemClickListener(new ProductsAdapter.OnItemClicklistener() {
                                            @Override
                                            public void onItemClick(Products products, View view) {
                                                intent = new Intent(ProductsActivity.this, DetailsActitvity.class);
                                                intent.putExtra(DetailsActitvity.EXTRA_ID, products.getProductId());
                                                openContextMenu(view);
                                            }
                                        });
                                    }
                                });
                    }

                }

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Opciones: ");
        menu.add(0, 123, 0, "Detalles");
        menu.setHeaderIcon(R.drawable.ic_storage_black_24dp);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 123:
                startActivity(intent);
                customType(ProductsActivity.this, "bottom-to-up");
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }


}


