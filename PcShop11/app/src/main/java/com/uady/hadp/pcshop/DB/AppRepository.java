package com.uady.hadp.pcshop.DB;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

import com.uady.hadp.pcshop.DB.Assemblies.Assemblies;
import com.uady.hadp.pcshop.DB.Assemblies.AssembliesDao;
import com.uady.hadp.pcshop.DB.AssembliesProducts.AssembliesAndProducts;
import com.uady.hadp.pcshop.DB.AssembliesProducts.AssembliesProductPrice;
import com.uady.hadp.pcshop.DB.AssembliesProducts.AssembliesProductsDao;
import com.uady.hadp.pcshop.DB.Categories.Categories;
import com.uady.hadp.pcshop.DB.Categories.CategoriesDAO;
import com.uady.hadp.pcshop.DB.OrderAssemblies.MissingProducts;
import com.uady.hadp.pcshop.DB.OrderAssemblies.OrderAssemblyDAO;
import com.uady.hadp.pcshop.DB.Products.Products;
import com.uady.hadp.pcshop.DB.Products.ProductsAndCategories;
import com.uady.hadp.pcshop.DB.Products.ProductsDao;

public class AppRepository {

    private CategoriesDAO categoriesDAO;
    private LiveData<List<Categories>> getAllCategorias;

    private ProductsDao productsDao;
    private LiveData<List<Products>> getALLProducts;

    private AssembliesDao assembliesDao;
    private LiveData<List<Assemblies>> AllAssemblies;

    private AssembliesProductsDao assembliesProductsDao;

    private OrderAssemblyDAO orderAssemblyDAO;
    public AppRepository(Application application) {
        AppDatabase db = AppDatabase.getInstance(application);
        productsDao = db.productsDao();
        categoriesDAO = db.categoriesDAO();
        assembliesDao = db.assembliesDao();
        assembliesProductsDao = db.assembliesProductsDao();
        orderAssemblyDAO = db.orderAssemblyDAO();

        AllAssemblies = assembliesDao.getAllAssemblies();
        getALLProducts = productsDao.getAllProduct();
        getAllCategorias = categoriesDAO.getAllProductCategories();

    }

    public LiveData<List<Categories>> getAllCategorias() {
        return getAllCategorias;
    }

    public LiveData<List<Products>> getALLProducts() {
        return getALLProducts;
    }

    public LiveData<List<Products>> getByCategoryId(int categoryId) {
        return productsDao.getByCategoryId(categoryId);
    }

    public LiveData<List<Products>> getByDescription(String description) {
        return productsDao.getByDescription(description);
    }


    public LiveData<List<Products>> getByDescriptionAndCategoryId(String description, int categoryId) {
        return productsDao.getByDescriptionAndCategoryId(description, categoryId);
    }

   public LiveData<ProductsAndCategories> getproductAndCategory( int id) {
        return productsDao.ProductItem(id);
    }

    public LiveData<List<Assemblies>> getAllAssemblies() {
        return AllAssemblies;
    }

    public LiveData<List<AssembliesAndProducts>> getProductsAndQty(int id) {
        return assembliesProductsDao.findProductsAndQty(id);
    }

    public LiveData<AssembliesProductPrice> getPriceTotal(int id) {
        return assembliesProductsDao.findPrice(id);
    }

    public LiveData<List<Assemblies>> getAssembliesDescription(String query) {
        return assembliesDao.getByDescription(query);
    }

    public  LiveData<List<MissingProducts>> getMissingProducts(){
        return orderAssemblyDAO.missingProducts();
    }


    public void nuke() {
        new AppRepository.Nuke(categoriesDAO).execute();
    }
    private static class Nuke extends AsyncTask<Void, Void, Void> {
        private CategoriesDAO categoriesDAO;

        Nuke(CategoriesDAO Dao) {
            categoriesDAO = Dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            categoriesDAO.deleteAll();

            return null;
        }
    }

    public void NukeProducts() {
        new AppRepository.NukeProducts(productsDao).execute();
    }

    private static class NukeProducts extends AsyncTask<Void, Void, Void> {
        private ProductsDao productsDao;

        NukeProducts(ProductsDao Dao) {
            productsDao = Dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            productsDao.deleteAll();

            return null;
        }
    }
}
