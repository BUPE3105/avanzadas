package com.uady.hadp.pcshop.DB.OrderAssemblies;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "order_assemblies")
public class OrderAssemblies {

    @PrimaryKey
    @ColumnInfo(name = "default_id")
    private int default_id;

    private  int id;

    @ColumnInfo(name = "assembly_id")
    private int assembly_id;

    @ColumnInfo(name = "qty")
    private int qty;

    public OrderAssemblies(int default_id, int id, int assembly_id, int qty) {
        this.default_id = default_id;
        this.id = id;
        this.assembly_id = assembly_id;
        this.qty = qty;
    }

    public int getDefault_id() {
        return default_id;
    }

    public void setDefault_id(int default_id) {
        this.default_id = default_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAssembly_id() {
        return assembly_id;
    }

    public void setAssembly_id(int assembly_id) {
        this.assembly_id = assembly_id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
