package com.uady.hadp.pcshop.DB.AssembliesProducts;

import android.support.annotation.NonNull;

public  class AssembliesAndProducts {
    public String description;

    public int id;



    public int qty;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    @Override
    public String toString() {
        return description;
    }
}
