package com.uady.hadp.pcshop.detailsactivitys;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.uady.hadp.pcshop.ViewModel.ProductModel;

import org.fabiomsr.moneytextview.MoneyTextView;

import com.uady.hadp.pcshop.DB.Products.ProductsAndCategories;
import com.uady.hadp.pcshop.ViewModel.ProductModel;

import com.uady.hadp.pcshop.R;

public class DetailsActitvity extends AppCompatActivity {

    private TextView textViewCategory;
    private TextView textViewProduct;
    private TextView textViewQty;
    private MoneyTextView textViewPrice;
    private ProductModel model;

    public static final String EXTRA_ID = "mx.com.uady.pruebas.EXTRA_CATEGORY_ID";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_actitvity);
        setTitle("Detalles");

        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        model = ViewModelProviders.of(this).get(ProductModel.class);

        textViewCategory = findViewById(R.id.tex_view_category);
        textViewProduct = findViewById(R.id.text_view_products);
        textViewQty = findViewById(R.id.text_view_qty);
        textViewPrice = findViewById(R.id.money_text_price);
        Intent intent = getIntent();
       if (intent.hasExtra(EXTRA_ID)){
           model.getProductAndCategory(intent.getIntExtra(EXTRA_ID, 0)).observe(this, new Observer<ProductsAndCategories>() {
               @Override
               public void onChanged(@Nullable ProductsAndCategories productsAndCategories) {
                   textViewCategory.setText(productsAndCategories.getCategorias());
                   textViewProduct.setText(productsAndCategories.getDescription());
                   int qty= productsAndCategories.getQty();
                   String Stock = Integer.toString(productsAndCategories.getQty());
                   if (qty<= 0){
                       textViewQty.setText(Stock);
                       textViewQty.setTextColor(Color.parseColor("#Ff6859"));
                   }else {
                       textViewQty.setText(Stock);
                       textViewQty.setTextColor(Color.parseColor("#72DEFF"));
                   }
                   textViewPrice.setAmount(productsAndCategories.getPrice()/100, "$");
               }
           });
       }

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }


}
