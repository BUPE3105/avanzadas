package com.uady.hadp.pcshop.DB.AssembliesProducts;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.uady.hadp.pcshop.DB.Assemblies.Assemblies;

@Entity(tableName = "assembly_products",foreignKeys = @ForeignKey(entity = Assemblies.class,
                                                                    parentColumns = "id",
                                                                    childColumns ="id" )
                                                                    ,indices = @Index(value = {"id","product_id"}, unique = false))
public class AssembliesProducts {
    @PrimaryKey
    private int default_id;

    private  int id;

    @ColumnInfo(name = "product_id")
    private  int product_id;

    @ColumnInfo(name = "qty")
    private int qty;

    public AssembliesProducts(int default_id, int id, int product_id, int qty) {
        this.default_id = default_id;
        this.id = id;
        this.product_id = product_id;
        this.qty = qty;
    }

    public int getDefault_id() {
        return default_id;
    }

    public void setDefault_id(int default_id) {
        this.default_id = default_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
