package com.uady.hadp.pcshop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.uady.hadp.pcshop.screens.AssembliesActivity;
import com.uady.hadp.pcshop.screens.ClientsActivity;
import com.uady.hadp.pcshop.screens.OrdersActivity;
import com.uady.hadp.pcshop.screens.ProductsActivity;
import com.uady.hadp.pcshop.screens.ReportsActivity;

public class MainActivity extends AppCompatActivity {

    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton button = findViewById(R.id.button_productos);
        ImageButton button2 = findViewById(R.id.button_paquetes);
        ImageButton button3 = findViewById(R.id.button_clientes);
        ImageButton button4 = findViewById(R.id.button_ordenes);
        ImageButton button5 = findViewById(R.id.button_reportes);
        ImageButton button6 = findViewById(R.id.button_info);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, ProductsActivity.class);
                startActivity(intent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, AssembliesActivity.class);
                startActivity(intent);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, ClientsActivity.class);
                startActivity(intent);
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, OrdersActivity.class);
                startActivity(intent);
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, ReportsActivity.class);
                startActivity(intent);
            }
        });


        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Información ", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
