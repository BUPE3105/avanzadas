package com.uady.hadp.pcshop.detailsactivitys;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import org.fabiomsr.moneytextview.MoneyTextView;

import com.uady.hadp.pcshop.DB.Products.ProductsAndCategories;
import com.uady.hadp.pcshop.R;
import com.uady.hadp.pcshop.ViewModel.ProductModel;

public class DetaislAssemblyProduct extends AppCompatActivity {

    public static String DATA_PASS_ID = "mx.com.uady.pc_shop.DATA_ID";

    private Intent intent;
    private ProductModel model;

    private TextView textViewCategory;
    private TextView textViewProduct;
    private TextView textViewQty;
    private MoneyTextView textViewPrice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_assembly_product);

        intent= getIntent();

        Toolbar toolbar= findViewById(R.id.toolbar_DPA);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Detalles");

        model = ViewModelProviders.of(this).get(ProductModel.class);

        textViewCategory = findViewById(R.id.tex_view_category_DPA);
        textViewProduct = findViewById(R.id.text_view_products_DPA);
        textViewQty = findViewById(R.id.text_view_qty_DPA);
        textViewPrice = findViewById(R.id.money_text_price_DPA);

        model.getProductAndCategory(intent.getIntExtra(DATA_PASS_ID,0)).observe(this, new Observer<ProductsAndCategories>() {
            @Override
            public void onChanged(@Nullable ProductsAndCategories productsAndCategories) {
                textViewCategory.setText(productsAndCategories.getCategorias());
                textViewProduct.setText(productsAndCategories.getDescription());
                int qty= productsAndCategories.getQty();
                String Stock = Integer.toString(productsAndCategories.getQty());
                if (qty<= 0){
                    textViewQty.setText(Stock);
                    textViewQty.setTextColor(Color.parseColor("#Ff6859"));
                }else {
                    textViewQty.setText(Stock);
                    textViewQty.setTextColor(Color.parseColor("#72DEFF"));
                }
                textViewPrice.setAmount(productsAndCategories.getPrice()/100, "$");
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
