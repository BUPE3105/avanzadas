package com.uady.hadp.pcshop.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import com.uady.hadp.pcshop.DB.AppRepository;
import com.uady.hadp.pcshop.DB.Products.Products;
import com.uady.hadp.pcshop.DB.Products.ProductsAndCategories;

public class ProductModel extends AndroidViewModel {

    private AppRepository productsrepository;
    private LiveData<List<Products>> getAllProdcuts;

    public ProductModel(@NonNull Application application) {
        super(application);
        productsrepository = new AppRepository(application);
        getAllProdcuts = productsrepository.getALLProducts();
    }

    public  LiveData<List<Products>> getAllProdcuts(){
        return  getAllProdcuts;
    }

    public  LiveData<List<Products>> getByCategoryId(int categoryId){
        return  productsrepository.getByCategoryId(categoryId);
    }


    public  LiveData<List<Products>> getByDescriptionAndCategoryId( String description,int categoryId){
        return productsrepository.getByDescriptionAndCategoryId(description,categoryId);
    }

    public  LiveData<List<Products>> getByDescription(String description){
        return productsrepository.getByDescription(description);
    }
    public  LiveData<ProductsAndCategories> getProductAndCategory(int id){
        return productsrepository.getproductAndCategory(id);
    }

    public  void  deleteAll (){
        productsrepository.nuke();
    }


}
