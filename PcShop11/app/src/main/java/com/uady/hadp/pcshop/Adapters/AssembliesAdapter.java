package com.uady.hadp.pcshop.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uady.hadp.pcshop.DB.Assemblies.Assemblies;
import com.uady.hadp.pcshop.R;

public class AssembliesAdapter extends ListAdapter<Assemblies, AssembliesAdapter.Holder> {

    private OnItemClicklistener mlistener;

    public AssembliesAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<Assemblies> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Assemblies>() {
                @Override
                public boolean areItemsTheSame(@NonNull Assemblies oldItem, @NonNull Assemblies newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull Assemblies oldItem, @NonNull Assemblies newItem) {
                    return oldItem.getId() == (newItem.getId()) &&
                            oldItem.getDescription().equals(newItem.getDescription());
                }
            };

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.assemblies_item,
                parent,false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Assemblies current =  getItem(position);
        holder.descripcion.setText(current.getDescription());

    }

    public Assemblies getAssembliesAt(int position) {
        return getItem(position);
    }

    class Holder extends RecyclerView.ViewHolder {

        private TextView descripcion;

        public Holder(@NonNull final View itemView) {
            super(itemView);
            descripcion = itemView.findViewById(R.id.recycler_text_view_assemblies);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mlistener != null && position != RecyclerView.NO_POSITION) {
                        mlistener.onItemClick(getItem(position), itemView);
                    }
                }
            });

        }

    }

    @Override
    public void onViewRecycled(@NonNull Holder holder) {
        super.onViewRecycled(holder);
    }


    public interface OnItemClicklistener {
        void onItemClick(Assemblies assemblies, View view);

    }

    public void setOnItemClickListener(AssembliesAdapter.OnItemClicklistener clicklistener) {
        this.mlistener = clicklistener;
    }



}
