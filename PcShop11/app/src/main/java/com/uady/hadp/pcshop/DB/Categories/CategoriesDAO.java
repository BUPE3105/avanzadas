package com.uady.hadp.pcshop.DB.Categories;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface CategoriesDAO {

    @Query("SELECT * from product_categories ORDER BY id desc")
    LiveData<List<Categories>> getAllProductCategories();


    @Insert
    void insert(Categories categories);

    @Query("DELETE FROM product_categories")
    void deleteAll();

    @Query("Select * from product_categories where description like (:query)")
    LiveData<List<Categories>> getByDescription(String query);


}
