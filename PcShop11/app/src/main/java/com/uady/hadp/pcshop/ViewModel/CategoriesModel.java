package com.uady.hadp.pcshop.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import com.uady.hadp.pcshop.DB.AppRepository;
import com.uady.hadp.pcshop.DB.Categories.Categories;

public class CategoriesModel extends AndroidViewModel {

    private AppRepository categoriesRepository;
    private LiveData<List<Categories>> getAll;

    public  CategoriesModel (Application application){
        super(application);

        categoriesRepository =  new AppRepository(application);
        getAll = categoriesRepository.getAllCategorias();
   }

   public LiveData<List<Categories>> getALLCategories (){
        return getAll;
   }


    public  void  deleteAll (){
        categoriesRepository.nuke();
    }


}
