package com.uady.hadp.pcshop.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uady.hadp.pcshop.DB.OrderAssemblies.MissingProducts;
import com.uady.hadp.pcshop.R;

public class AdapterMissingProducts extends ListAdapter<MissingProducts, AdapterMissingProducts.Holder> {

    public AdapterMissingProducts() {
        super(DIFF_CALLBACK);
    }


    private static final DiffUtil.ItemCallback<MissingProducts> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<MissingProducts>() {
                @Override
                public boolean areItemsTheSame(@NonNull MissingProducts oldItem, @NonNull MissingProducts newItem) {
                    return oldItem.getDescription() == newItem.getDescription();
                }

                @Override
                public boolean areContentsTheSame(@NonNull MissingProducts oldItem, @NonNull MissingProducts newItem) {
                    return oldItem.getQty() == (newItem.getQty()) &&
                            oldItem.getDescription().equals(newItem.getDescription())&&
                            oldItem.getStock() == (newItem.getStock())&&
                            oldItem.getProducts().equals(newItem.getProducts());
                }
            };

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.missing_items, parent, false);
        return new  AdapterMissingProducts.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        MissingProducts current = getItem(position);

        String qtyAssemblies = Integer.toString(current.getQty());
        String qtyProducts = Integer.toString(current.getStock());

        holder.descripcion.setText(current.getProducts());
        holder.qtyNow.setText("Se tienen: "+qtyProducts);
        holder.qtyMiss.setText("Se necesitan: "+qtyAssemblies);
        holder.category.setText(current.getDescription());

    }

    class  Holder extends RecyclerView.ViewHolder{
        private TextView descripcion;
        private TextView qtyNow;
        private  TextView qtyMiss;
        private  TextView category;

        public Holder(@NonNull View itemView) {
            super(itemView);
            category = itemView.findViewById(R.id.missing_category);
            descripcion = itemView.findViewById(R.id.missing_description);
            qtyNow = itemView.findViewById(R.id.missig_qty_now);
            qtyMiss = itemView.findViewById(R.id.missing_qty_miss);
        }
    }
}
