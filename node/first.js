"use strict";
let  y= 10;
const z = 100;
console.log(`y=${y}`);

console.log(5+8);
console.log(5-8);
console.log(5*8);
console.log(5/8);
console.log(5%8);
console.log(5**8);

console.log();

console.log(21-12);
console.log('21'-12);
console.log(21-'12');
console.log('21'-'12');

console.log();

console.log(21+21+3);

console.log();

console.log('5' ** '2');

console.log();

console.log(7 === 7);
console.log(7 === '7');
console.log('7' === '7');
console.log('7' === 7);

console.log(null == undefined);
console.log(null === undefined);
