package com.example.basiccontrolsapplication;

import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity /* implements CompoundButton.OnCheckedChangeListener */ {

    ToggleButton bluetoothButton;
    Switch wifiSwitch;

    EditText colorsText;

    CheckBox redCheckBox;
    CheckBox greenCheckBox;
    CheckBox blueCheckBox;
    CheckBox orangeCheckBox;

    RadioButton highRadio;
    RadioButton mediumRadio;
    RadioButton lowRadio;

    Spinner subjectsSpinner;
    Spinner planetsSpinner;

    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bluetoothButton = findViewById(R.id.bluetooth_button);
        bluetoothButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    bluetoothButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bluetooth_connected_black, 0, 0, 0);
                } else {
                    bluetoothButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bluetooth_disabled_black, 0, 0, 0);
                }
            }
        });

        wifiSwitch = findViewById(R.id.wifi_switch);
        wifiSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(MainActivity.this,
                            "Usando red WI-FI",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this,
                            "WI-FI disabled",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        redCheckBox = findViewById(R.id.red_checbox);
        greenCheckBox = findViewById(R.id.green_checbox);
        blueCheckBox = findViewById(R.id.blue_checbox);
        orangeCheckBox = findViewById(R.id.orange_checbox);
        colorsText = findViewById(R.id.colors_text);

        highRadio = findViewById(R.id.high_radio);
        mediumRadio = findViewById(R.id.medium_radio);
        lowRadio = findViewById(R.id.low_radio);

        /*highRadio.setOnCheckedChangeListener(this);
        mediumRadio.setOnCheckedChangeListener(this);
        lowRadio.setOnCheckedChangeListener(this);*/

        CompoundButton.OnCheckedChangeListener radioListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(MainActivity.this,
                            "Se presionó: " + buttonView.getText(),
                            Toast.LENGTH_SHORT).show();
                }
            }
        };

        highRadio.setOnCheckedChangeListener(radioListener);
        mediumRadio.setOnCheckedChangeListener(radioListener);
        lowRadio.setOnCheckedChangeListener(radioListener);

        subjectsSpinner = findViewById(R.id.subjects_spinner);
        subjectsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // parent.getItemAtPosition(position)
                Toast.makeText(MainActivity.this,
                        getResources().getStringArray(R.array.subjects_arrays)[position],
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        planetsSpinner = findViewById(R.id.planets_spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item);
        adapter.add("Mercurio");
        adapter.add("Venus");
        adapter.add("Tierra");
        adapter.add("Marte");
        adapter.add("Júpiter");
        adapter.add("Saturno");
        adapter.add("Urano");
        adapter.add("Neptuno");
        planetsSpinner.setAdapter(adapter);

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v,
                        "Floating button in action",
                        Snackbar.LENGTH_SHORT).setAction("ACTION",null).show();
            }
        });
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            Toast.makeText(this,
                    "Se presionó: " + buttonView.getText(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void onColorChanged(View v) {
        List<CheckBox> colors = new ArrayList<>();
        colors.add(redCheckBox);
        colors.add(greenCheckBox);
        colors.add(blueCheckBox);
        colors.add(orangeCheckBox);

        String text = "";
        for (CheckBox color : colors) {
            if (color.isChecked()) {
                text += "[" + color.getText() + "]";
            }
        }

        colorsText.setText(text);
    }
}
