package uady.hap.quizapppro;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import uady.hap.quizapppro.ListasPreguntas.Question;

public class CheatActivity extends AppCompatActivity {

    public static final int CHEAT_REQUEST_CODE = 1;
    private static final String CHEATED_FLAG_KEY = "CHEATED_FLAG_KEY";

    // ****
    private List<Question> questions;
    // ****

    private Button cheatButton;
    private TextView questionText;
    private TextView cheatAnswerText;

    private boolean respuestaTrampa;
    private String textoTrampa;
    private boolean cheated;

    private  MainActivityModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);
        cheated = false;
        if (savedInstanceState != null) {
            cheated = savedInstanceState.getBoolean(CHEATED_FLAG_KEY);
            if (cheated) {
                setResult(RESULT_OK);
            }
        }

        model = ViewModelProviders.of(this).get(MainActivityModel.class);
        model.loadGameQuestion();

        cheatButton = findViewById(R.id.cheat_button);
        cheatAnswerText = findViewById(R.id.cheat_answer_text);
        questionText = findViewById(R.id.question_text);


        Bundle bundle = getIntent().getExtras();
        respuestaTrampa = bundle.getBoolean("RespuestaTrampa");
        textoTrampa = getIntent().getStringExtra("PreguntaTrampa");

        questionText.setText(textoTrampa);


        cheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cheated = true;
                setResult(RESULT_OK);
                if (respuestaTrampa){
                    cheatAnswerText.setText("Verdadero");
                }else {
                    cheatAnswerText.setText("Falso");

                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(CHEATED_FLAG_KEY, cheated);
    }
}
