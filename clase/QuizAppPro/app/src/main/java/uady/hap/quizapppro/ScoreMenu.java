package uady.hap.quizapppro;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import android.widget.Toast;


public class ScoreMenu extends AppCompatActivity {

    private TextView gameplayer;
    private Button acceptButoon;
    private Button getAcceptButoon;
    private int puntajeValor;

   private TextView usuario1;
   private TextView puntaje1;
   private TextView usuario2;
   private TextView puntaje2;
   private TextView usuario3;
   private TextView puntaje3;
   private TextView usuario4;
   private TextView puntaje4;
   private TextView usuario5;
   private TextView puntaje5;
   private ImageView imageView;
   private TextView puntajeFinal;

   private boolean bool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score_menu);


        imageView = findViewById(R.id.imagen_score);

        getAcceptButoon = findViewById(R.id.pruebaButon);
         usuario1 = findViewById(R.id.usuario1);
         puntaje1 = findViewById(R.id.puntaje1);
        usuario2 = findViewById(R.id.usuario2);
         puntaje2 = findViewById(R.id.puntaje2);
        usuario3 = findViewById(R.id.usuario3);
         puntaje3 = findViewById(R.id.puntaje3);
        usuario4 = findViewById(R.id.usuario4);
         puntaje4 = findViewById(R.id.puntaje4);
        usuario5 = findViewById(R.id.usuario5);
         puntaje5 = findViewById(R.id.puntaje5);

         puntajeFinal = findViewById(R.id.puntaje_final);


        puntajeValor= getIntent().getIntExtra("PUNTAJE", 0);
         bool = getIntent().getExtras().getBoolean("TRAMPAS");

        getAcceptButoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder gameScoreBuilder = new AlertDialog.Builder(ScoreMenu.this);
                View messageView = getLayoutInflater().inflate(R.layout.name_user, null);
                gameScoreBuilder.setView(messageView);
                final AlertDialog dialog = gameScoreBuilder.create();
                dialog.setCancelable(false);

                gameplayer = messageView.findViewById(R.id.nombre_jugador);
                acceptButoon = messageView.findViewById(R.id.aceptar_button);

                acceptButoon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int n1 = puntajeValor;
                        String puntaje = Integer.toString(n1);
                        if (!bool){
                            Toast.makeText(ScoreMenu.this, "Haz hecho trampa!", Toast.LENGTH_SHORT).show();
                            if (puntajeValor > 160000 ){
                                usuario3.setText(gameplayer.getText().toString());
                                 n1 = puntajeValor;
                                 puntaje = Integer.toString(n1);
                                puntaje3.setText(puntaje);
                                imageView.setImageResource(R.drawable.four);
                            }else if (puntajeValor > 130000 && puntajeValor <160000){
                                usuario4.setText(gameplayer.getText().toString());
                                 n1 = puntajeValor;
                                 puntaje = Integer.toString(n1);
                                puntaje4.setText(puntaje);
                                imageView.setImageResource(R.drawable.three);
                            }else  if (puntajeValor > 25000 && puntajeValor < 130000){
                                usuario5.setText(gameplayer.getText().toString());
                                 n1 = puntajeValor;
                                 puntaje = Integer.toString(n1);
                                puntaje5.setText(puntaje);
                                imageView.setImageResource(R.drawable.two);
                            }
                        }else {
                            Toast.makeText(ScoreMenu.this, "Felicidades no haz hecho trampa!", Toast.LENGTH_SHORT).show();
                            if (puntajeValor > 160000){
                                usuario1.setText(gameplayer.getText().toString());
                                 n1 = puntajeValor;
                                 puntaje = Integer.toString(n1);
                                puntaje1.setText(puntaje);
                                imageView.setImageResource(R.drawable.six);

                            }else if(puntajeValor > 130000 && puntajeValor <160000){
                                usuario2.setText(gameplayer.getText().toString());
                                 n1 = puntajeValor;
                                 puntaje = Integer.toString(n1);
                                puntaje2.setText(puntaje);
                                imageView.setImageResource(R.drawable.five);

                            }else  if (puntajeValor > 100072 && puntajeValor < 130000){
                                usuario3.setText(gameplayer.getText().toString());
                                 n1 = puntajeValor;
                                 puntaje = Integer.toString(n1);
                                puntaje3.setText(puntaje);
                                imageView.setImageResource(R.drawable.four);
                            }else  if (puntajeValor > 80000 && puntajeValor <100072){
                                usuario4.setText(gameplayer.getText().toString());
                                 n1 = puntajeValor;
                                 puntaje = Integer.toString(n1);
                                puntaje4.setText(puntaje);
                                imageView.setImageResource(R.drawable.three);
                            }else if (puntajeValor > 25000 && puntajeValor < 80000){
                                usuario5.setText(gameplayer.getText().toString());
                                 n1 = puntajeValor;
                                 puntaje = Integer.toString(n1);
                                puntaje5.setText(puntaje);
                                imageView.setImageResource(R.drawable.two);
                            }

                            puntajeFinal.setText(puntaje);
                        }
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });







    }
}


