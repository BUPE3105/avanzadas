package uady.hap.quizapppro.ListasPreguntas;

public class Question {
    private int id;
    private String text;
    private String answertext;
    private boolean answer;
    private boolean estado;

    public Question(int id, String text, String answertext, boolean answer,  boolean estado) {
        this.id = id;
        this.text = text;
        this.answertext = answertext;
        this.answer = answer;

        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isAnswer() {
        return answer;
    }

    public void setAnswer(boolean answer) {
        this.answer = answer;
    }


    public String getAnswertext() {
        return answertext;
    }

    public void setAnswertext(String answertext) {
        this.answertext = answertext;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
