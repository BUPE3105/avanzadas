package uady.hap.quizapppro;

import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class GameMenu extends AppCompatActivity {


    private  MainActivityModel model;
    private   boolean backButton = false;
    private   boolean tramapsCheck = false;
    private  TextView puntajesText;

    ArrayList<Boolean> preguntasContestadas = new ArrayList<>();
    ArrayList<Boolean> cheats = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_menu);
        cheats.add(true);

        model = ViewModelProviders.of(this).get(MainActivityModel.class);
        model.loadGameQuestion();

        int difciultad = OptionsActivity.DIFICULTAD;
            switch (difciultad){
                case 10:
                    model.setPuntaje(1024);
                    break;
                case 50:
                    model.setPuntaje(2048);
                    break;
                case 100:
                    model.setPuntaje(4096);
                    break;
                    default:
                        model.setPuntaje(1024);
                        break;
            }
        SharedPreferences pref = getApplicationContext().getSharedPreferences("CONFIGURATION", MODE_PRIVATE);

        final String sesion = pref.getString("datoNumeroCheats","1");
        String showCheats = pref.getString("datoMostrarCheats","DONT_SHOW_CHEATS");
        final String  numberSelectedQuestions = pref.getString("datoNumeropreguntas", "15");

        final ImageButton preguntaAnterior = findViewById(R.id.pregunta_anterior);
        final ImageButton preguntaSiguiente = findViewById(R.id.pregunta_siguiente);
        final ImageButton respuestaVerdadera = findViewById(R.id.boton_verdadero);
        final ImageButton respuestaFalsa = findViewById(R.id.boton_falso);
        final Button botonTrampa = findViewById(R.id.boton_trampa);
        final TextView textNumeroPregunta = findViewById(R.id.numero_preguntas);

        final TextView textNumeroCheats = findViewById(R.id.text_num_tries);
        final FrameLayout showCheatsLayout = findViewById(R.id.cheat_layout);
           puntajesText = findViewById(R.id.puntaje_textview);

        showCheatsLayout.setVisibility(View.GONE);

        final TextView textoPregunta = findViewById(R.id.texto_pregunta);

         if (showCheats.equals("DONT_SHOW_CHEATS")){
             showCheatsLayout.setVisibility(View.GONE);
         } else if (showCheats.equals("SHOW_CHEATS")){
             showCheatsLayout.setVisibility(View.VISIBLE);
         }
        textNumeroCheats.setText("0"+ "/ " +sesion);

        textNumeroPregunta.setText(" 0 "  + " / "+ numberSelectedQuestions);

        textoPregunta.setText("Da clic a ala derecha para iniciar ");


        final int finalPregunta = Integer.parseInt(numberSelectedQuestions) ;
        respuestaFalsa.setEnabled(false);
        respuestaVerdadera.setEnabled(false);
        botonTrampa.setEnabled(false);


        preguntaAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.setAnteriorResultado(operaciones.restar(model.getAnteriorNumeroPregunta()));
                if (model.getSiguienteNumeroPregunta() >0 ) {
                    textNumeroPregunta.setText(" " + model.getAnteriorNumeroPregunta() + " / " + numberSelectedQuestions);
                    textoPregunta.setText(model.getPreviousQuestion().getText().toUpperCase());

                }
                    if (model.getCurrentQuestion().getEstado()){
                        textoPregunta.setText(model.getCurrentQuestion().getAnswertext().toUpperCase());
                        respuestaFalsa.setEnabled(false);
                        respuestaVerdadera.setEnabled(false);

                    }

            }
        });

        preguntaSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.setSiguienteResultado(operaciones.sumar(model.getSiguienteNumeroPregunta()));
                if(model.getSiguienteNumeroPregunta() <= finalPregunta){
                    textoPregunta.setText(model.getNextQuestion().getText().toUpperCase());
                    textNumeroPregunta.setText(" " +
                                               model.getSiguienteNumeroPregunta()
                                                + " / "+
                                                numberSelectedQuestions);
                    respuestaFalsa.setEnabled(true);
                    respuestaVerdadera.setEnabled(true);
                    botonTrampa.setEnabled(true);
                    if (model.getCurrentQuestion().getEstado()){
                        textoPregunta.setText(model.getCurrentQuestion().getAnswertext().toUpperCase());
                        respuestaFalsa.setEnabled(false);
                        respuestaVerdadera.setEnabled(false);
                    }

                    if (model.getSiguienteNumeroPregunta() == finalPregunta){
                        Toast.makeText(GameMenu.this, "Si has contestado todas las preguntas, cick en next",
                                Toast.LENGTH_LONG).show();
                    }

                    if (preguntasContestadas.size() == finalPregunta){
                        AlertDialog.Builder builder = new AlertDialog.Builder(GameMenu.this);
                        builder.setTitle("Deseas obtener tu puntaje?");
                        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(GameMenu.this, ScoreMenu.class);
                                for (boolean b : cheats){
                                    if (b){
                                        intent.putExtra("TRAMPAS", true);
                                    }else {
                                        intent.putExtra("TRAMPAS", false);
                                    }
                                }
                                model.setPuntaje(model.getPuntaje());
                                intent.putExtra("PUNTAJE",model.getPuntaje());
                                startActivity(intent);
                                finish();
                            }
                        });
                        builder.setNegativeButton("Denegar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();

                    }
               }



            }
        });




        respuestaVerdadera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model.getCurrentQuestion().isAnswer()) {
                    textoPregunta.setText(model.getCurrentQuestion().getAnswertext().toUpperCase());
                    respuestaFalsa.setEnabled(false);
                    respuestaVerdadera.setEnabled(false);
                    Toast.makeText(GameMenu.this,
                            "Correcto",
                            Toast.LENGTH_SHORT).show();
                    model.setPuntaje(operaciones.sumarPunatje(model.getPuntaje()));
                    String puntaje =  Integer.toString(model.getPuntaje());
                    puntajesText.setText(puntaje);

                    preguntasContestadas.add(true);

                } else {
                    textoPregunta.setText(model.getCurrentQuestion().getAnswertext().toUpperCase());
                    respuestaFalsa.setEnabled(false);
                    respuestaVerdadera.setEnabled(false);
                    Toast.makeText(GameMenu.this,
                            "Incorrecto",
                            Toast.LENGTH_SHORT).show();
                    model.setPuntaje(operaciones.restarPuntaje(model.getPuntaje()));
                    String puntaje =  Integer.toString(model.getPuntaje());
                    puntajesText.setText(puntaje);
                    preguntasContestadas.add(true);

                }
                textoPregunta.setText(model.getCurrentQuestion().getAnswertext());
                if (!model.getCurrentQuestion().getEstado()){
                    model.getCurrentQuestion().setEstado(true);
                }

            }
        });

        respuestaFalsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!model.getCurrentQuestion().isAnswer()) {
                    textoPregunta.setText(model.getCurrentQuestion().getAnswertext().toUpperCase());
                    respuestaFalsa.setEnabled(false);
                    respuestaVerdadera.setEnabled(false);
                    Toast.makeText(GameMenu.this,
                            "Correcto",
                            Toast.LENGTH_SHORT).show();
                    model.setPuntaje(operaciones.sumarPunatje(model.getPuntaje()));
                    String puntaje =  Integer.toString(model.getPuntaje());
                    puntajesText.setText(puntaje);
                    preguntasContestadas.add(true);


                } else {
                    textoPregunta.setText(model.getCurrentQuestion().getAnswertext().toUpperCase());
                    respuestaFalsa.setEnabled(false);
                    respuestaVerdadera.setEnabled(false);
                    Toast.makeText(GameMenu.this,
                            "Incorrecto",
                            Toast.LENGTH_SHORT).show();
                    model.setPuntaje(operaciones.restarPuntaje(model.getPuntaje()));
                    String puntaje =  Integer.toString(model.getPuntaje());
                    puntajesText.setText(puntaje);
                    preguntasContestadas.add(true);

                }
                if (!model.getCurrentQuestion().getEstado()){
                    model.getCurrentQuestion().setEstado(true);

                }

            }
        });

        botonTrampa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cheats.add(true);
                model.setSiguienteTrampa(operaciones.sumar(model.getTrampa()));
                textNumeroCheats.setText(model.getTrampa() +" / " + sesion);

                Intent intent = new Intent(GameMenu.this, CheatActivity.class);
                intent.putExtra("PreguntaTrampa", model.getCurrentQuestion().getText());
                intent.putExtra("RespuestaTrampa", model.getCurrentQuestion().isAnswer());
                startActivityForResult(intent, CheatActivity.CHEAT_REQUEST_CODE);

                int cheats = Integer.parseInt(sesion);
                if (model.getTrampa()==cheats){
                    botonTrampa.setEnabled(false);
                    botonTrampa.setText("Has usado todos tus cheats");
                    textNumeroCheats.setText("");
                }

            }
        });
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case CheatActivity.CHEAT_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Toast.makeText(this,
                            "¡¡Se ha hecho trampa!!",
                            Toast.LENGTH_SHORT).show();
                    cheats.add(false);
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this,
                            "¡¡No se ha hecho trampa!!",
                            Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                // other activities...
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (backButton) {
            super.onBackPressed();
            recreate();
            return;
        }

        this.backButton = true;
        Toast.makeText(this, "Presiona dos veces atras para salir", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                backButton =false;
            }
        }, 2000);
    }
}
