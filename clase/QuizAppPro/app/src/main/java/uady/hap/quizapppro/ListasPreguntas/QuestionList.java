package uady.hap.quizapppro.ListasPreguntas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uady.hap.quizapppro.MainActivity;
import uady.hap.quizapppro.OptionsActivity;

public class QuestionList {

    private List<Question> listaTodos;
    private List<Question> listaTodosfinal;
    private List<Question> Historia;
    private List<Question> Deportes;
    private List<Question> Fisica;
    private List<Question> Arte;
    private List<Question> Animales;
    private List<Question> Geografia;
    private List<Question> MediaHistoria;
    private List<Question> MediaDeportes;
    private List<Question> MediaFisica;
    private List<Question> MediaArte;
    private List<Question> MediaAnimales;
    private List<Question> MediaGeografia;
    private List<Question> DificilHistoria;
    private List<Question> DificilDeportes;
    private List<Question> DificilFisica;
    private List<Question> DificilArte;
    private List<Question> DificilAnimales;
    private List<Question> DificilGeografia;
    private  int HISTORIAVALOR;
    private  int ARTEAVALOR;
    private  int FISICAVALOR;
    private  int DEPORTESVALOR;
    private  int ANIMSLESVALOR;
    private  int GEOGRAFIAVALOR;
    private   int TODOSVALOR;
    private   int DIFICULTAD;

    public  List<Question>  getFinallist(){

        int n = OptionsActivity.preguntaTotal;
        listaTodosfinal = new ArrayList<>();
        getAllQuestions();

        listaTodosfinal.addAll(listaTodos.subList(0,n));
        return listaTodosfinal;
    }

    public void getAllQuestions() {

        if (listaTodos == null) {
            listaTodos = new ArrayList<>();
            Deportes = new ArrayList<>();
            Historia = new ArrayList<>();
            Fisica = new ArrayList<>();
            Arte = new ArrayList<>();
            Animales = new ArrayList<>();
            Geografia = new ArrayList<>();
            MediaDeportes = new ArrayList<>();
            MediaHistoria = new ArrayList<>();
            MediaFisica = new ArrayList<>();
            MediaArte = new ArrayList<>();
            MediaAnimales = new ArrayList<>();
            MediaGeografia = new ArrayList<>();
           DificilDeportes = new ArrayList<>();
           DificilHistoria = new ArrayList<>();
           DificilFisica = new ArrayList<>();
           DificilArte = new ArrayList<>();
           DificilAnimales = new ArrayList<>();
           DificilGeografia = new ArrayList<>();

            getValores();


            Deportes.add(new Question(0, "¿Han sido los malabares deporte olímpico? ", "Verdadero", true ,  false));
            Deportes.add(new Question(1, "Es el ajedrez un deporte ", "Verdadero", true   ,  false));
            Deportes.add(new Question(2, "Velocidad Máxima Ser Humano: ¿45 kilómetros por hora? ", "Verdadero", true ,  false));
            Deportes.add(new Question(3, "el baloncesto es el deporte femenino con más licencias deportivas", "Verdadero", true   ,  false));
            Deportes.add(new Question(4, "La última vez que en la Premier el campeón y el subcampeón empataron a puntos fue hace 9 años", " Falso", false   ,  false));
            MediaDeportes.add(new Question(0, "Lutz Pfannenstiel es el único futbolista que ha jugado en 20 equipos de futbol o más.", "Falso", false   ,  false));
            MediaDeportes.add(new Question(1, "Gary Lineker se cagó encima en un partido contra Irlanda.", "Verdadero", true   ,  false));
            MediaDeportes.add(new Question(2, "Futbol, Hungría fue subcampeona del mundo una vez.", "Falso", false ,  false));
            MediaDeportes.add(new Question(3, "Futbol, Ningún país tiene tantos subcampeonatos del mundo como Holanda.	", " Falso", false   ,  false));
            MediaDeportes.add(new Question(4, "El Inter fue creado por exmiembros del AC Milan.	", " Verdadero", true   ,  false));
            DificilDeportes.add(new Question(0, "El máximo goleador de la historia de la Copa Libertadores no es sudamericano. 	", "Falso", false  , false));
            DificilDeportes.add(new Question(1, "Futbol, El París Saint-Germain tiene menos de 50 años ", "Verdadero", true  , false));
            DificilDeportes.add(new Question(2, "Desde 1896 se han celebrado ininterrumpidamente los Juegos Olímpicos cada cuatro años.", "Falso", false, false));
            DificilDeportes.add(new Question(3, "Carl Lewis es un  famoso atleta olímpico es conocido como “El Hijo del Viento” 	", "Verdadero", true  , false));
            DificilDeportes.add(new Question(4, "Son deportes olímpicos el surf y el skateboard.", "Verdadero", true  , false));

            Historia.add(new Question(0, "La Segunda Guerra Mundial duró 5 años ", "Falso", false ,  false));
            Historia.add(new Question(1, "El primer hombre que salió al espacio fue Neil Armstrong ", "Falso", false  ,  false));
            Historia.add(new Question(2, "Japón participó en la Primera Guerra Mundial ", "Verdadero", true ,  false));
            Historia.add(new Question(3, "El británico Alexander Fleming descubrió la penicilina ", "Verdadero", true   ,  false));
            Historia.add(new Question(4, "La Revuelta de Tiananmén fue una manifestación pacífica a favor del nacionalismo en la cual no murió nadie", "Falso", false ,  false));
            MediaHistoria.add(new Question(0, "El Accidente de Chernóbil fue un accidente de tipo nuclear ", "Falso", false   ,  false));
            MediaHistoria.add(new Question(1, "La imprenta se inventó en el siglo XIV 	", "Falso", false   ,  false));
            MediaHistoria.add(new Question(2, "El levantamiento de EZLN se produjo en México y fue por los derechos de los indígentas y contra el TCLAN", "Verdadero", true ,  false));
            MediaHistoria.add(new Question(3, "El asesinato de Salvador Allende por las fuerzas del dictador Pinochet tuvo lugar en Perú 		", "Falso", false   ,  false));
            MediaHistoria.add(new Question(4, "Pregunta de regalo (Verdadero) ", "Verdadero", true ,  false));
            DificilHistoria.add(new Question(0, "La Revolución Francesa comenzó con el golpe de estado de Napoleón ", "Falso", false  ,  false));
            DificilHistoria.add(new Question(1, "157 países estuvieron involucrados en la segunda guerra mundial ", "Falso", false  ,  false));
            DificilHistoria.add(new Question(2, "La torre de londres solía ser un zoológico. ", "Verdadero", true,  false));
            DificilHistoria.add(new Question(3, "Sócrates se suicidó tomando veneno. ", "Verdadero", true  ,  false));
            DificilHistoria.add(new Question(4, "Napoleón no nació en francia. él nació en la isla mediterránea de córcega y sus padres eran italianos ", "Verdadero", true,  false));


            Fisica.add(new Question(0, "¿Las dimensionales de aceleración son m/s? ", "Falso", false , false));
            Fisica.add(new Question(1, "Es la relación que se establece entre el espacio o la distancia que recurre un objeto y el tiempo que invierte en el: es la velocidad. 		", "Verdadero", true   , false));
            Fisica.add(new Question(2, "Calor es el que posee un cuerpo ", "Verdadero", true , false));
            Fisica.add(new Question(3, "Las cuatro escalas termométricas para medir la energía térmica de una sustancia son: Celsius, Fahrenheit, Ranine , Kelvin. 		", "Verdadero", true , false));
            Fisica.add(new Question(4, "Mecánica es la rama de física que estudia y analiza el movimiento y reposo de los cuerpos y su evolución en el tiempo, bajo acción de fuerzas. ", " Verdadero", true , false));
            MediaFisica.add(new Question(0, "cinemática se encarga del estudio de las fuerzas tomando en cuenta las causas que lo producen. 			", "Falso", false   , false));
            MediaFisica.add(new Question(1, "Dinámica: rama de la física que estudia los cuerpos en reposo. ", "Falso", false , false));
            MediaFisica.add(new Question(2, "¿Es verdad que las naves se impulsan ‘apoyándose’ en planetas?", "Verdadero", true   , false));
            MediaFisica.add(new Question(3, "EL movimiento circular: es el que se basa en un eje de giro y un radio constante por lo cual la trayectoria en en forma lineal. ", "Falso", false , false));
            MediaFisica.add(new Question(4, "se conoce como caída libre cuando de cierta altura un cuerpos e deja caer para permitir que la fuerza de gravedad actué sobre el, siendo su velocidad inicial cero", "Verdadero", true , false));
            DificilFisica.add(new Question(0, "Cuando un auto recorre una curva está cambiando su velocidad. 	", "Verdadero", true,  false));
            DificilFisica.add(new Question(1, "Cuando un objeto cae libremente su aceleración cambia. 	", "Verdadero", true  ,  false));
            DificilFisica.add(new Question(2, "Los rayos beta están formados por electrones que salen de los núcleos", "Verdadero", true,  false));
            DificilFisica.add(new Question(3, "La mayor parte de la masa de un átomo está en su núcleo 	", "Falso", false,  false));
            DificilFisica.add(new Question(4, "Si la velocidad media de una partícula es diferente de cero en algún intervalo de tiempo, la velocidad instantánea de la partícula alguna vez es cero durante este intervalo. ", "Falso", false,  false));

            Arte.add(new Question(0, "La película con más Oscar en la historia es Titanic", "Verdadero ", true   , false ));
            Arte.add(new Question(1, "Eugenio Derbez prestó su voz para el personaje de Burro, en la película de Sherk", "Verdadero" , true     , false ));
            Arte.add(new Question(2, "The Beatles es el grupo que más discos ha vendido en la historia", "Verdadero", true   , false ));
            Arte.add(new Question(3, "Solo un Mexicano ha ganado el Oscar a Mejor película extranjera 	", "Verdadero", true   , false ));
            Arte.add(new Question(4, "Beethoven era sordo", "Verdadero", true  , false ));

            MediaArte.add(new Question(0, "El disco más vendido de la historia es Thriller de Michael Jackson ", "Verdadero", true   , false ));
            MediaArte.add(new Question(1, "The Beatles solo estuvo reunida durante la década de los 60’s", "Verdadero", true     , false ));
            MediaArte.add(new Question(2, "Les Paul fue el inventor de la guitarra eléctrica ", "Falso", false, false ));
            MediaArte.add(new Question(3, "Nirvana nació como grupo en 1990 ", "Falso", false   , false ));
            MediaArte.add(new Question(4, "El guitarrista Slash nació en Inglaterra el 23 de Julio de 1965", "Verdadero", true    , false));
            DificilArte.add(new Question(0, "Existe una versión desnuda de la Mona Lisa ", "Verdadero", true    ,false ));
            DificilArte.add(new Question(1, "Coliseo Romano. ¿Cabían 50.000 personas? ", " Verdadero", true  ,false ));
            DificilArte.add(new Question(2, "Columnas Griegas. Dentro del arte griego, los órdenes arquitectónicos son un clásico. Eran tres y marcados por sus columnas: Dóricas, Jónicas y Cirénicas.", "Falso", false  ,false ));
            DificilArte.add(new Question(3, "Cervantes y Shakespeare murieron el mismo día	", " Falso", false  ,false ));
            DificilArte.add(new Question(4, "Natalie Portman ganó el Óscar a Mejor Actriz por la película “Jackie”. ", " Falso", true    ,false ));

            Animales.add(new Question(0, "Los Cocodrilos lloran \"lágrimas de cocodrilo\" ", "Verdadero", true ,  false));
            Animales.add(new Question(1, "Los murciélagos son ciegos", "Falso", false   ,  false));
            Animales.add(new Question(2, "Cuando están en peligro, los avestruces enterrarán su cabeza en la arena.", "Falso", false ,  false));
            Animales.add(new Question(3, "Si está sano, un gato siempre aterrizará en sus pies.", "Falso", false ,  false));
            Animales.add(new Question(4, "Si tocas a un pájaro recién nacido, el olor hará que sus padres lo abandonen. ", "Falso", false ,  false));

            MediaAnimales.add(new Question(0, "Un año humano equivale a siete años de un perro 				", "Falso", false   , false));
            MediaAnimales.add(new Question(1, "La mantis religiosa femenina se come a la masculina después del apareamiento. 	", "Falso", false   , false));
            MediaAnimales.add(new Question(2, "Si la nariz de un perro es fría y húmeda, eso significa que el perro está en buena salud ", "Falso", false , false));
            MediaAnimales.add(new Question(3, "Los búhos son los pájaros más listos que existen. 				", "Falso", false , false));
            MediaAnimales.add(new Question(4, "El color que más le atrae a un mosquito es el Rojo ", "Falso", false , false));

            DificilAnimales.add(new Question(0, "Los simios son capaces de reconocerse en un espejo.", "Verdadero", true  ,  false));
            DificilAnimales.add(new Question(1, "Los camellos usan sus jorobas para almacenar agua.", "Falso", false  ,  false));
            DificilAnimales.add(new Question(2, "Los perros pueden reconocer las fotografías de sus dueños y otras personas conocidas. ", "Verdaderoe", true,  false));
            DificilAnimales.add(new Question(3, "El guepardo es el animal más veloz en tierra firma y también el más veloz de todo el reino animal   		", "Verdadero", true,  false));
            DificilAnimales.add(new Question(4, "Existe un animal conocido como el “Pez arquero”, el cual puede atrapar insectos, escupiendo desde los estanques.", "Verdadero", true,  false));

            Geografia.add(new Question(0, "La capital de República Checa es Budapest", "Falso", false  , false));
            Geografia.add(new Question(1, "Uzbekistán, al igual que todos sus vecinos, no tiene salida al mar: ", "Verdadero", true    , false));
            Geografia.add(new Question(2, "Canadá posee más kilómetros de costa que todos los países de Europa juntos ", "Verdadero", true  , false));
            Geografia.add(new Question(3, "El país de América del Norte que tiene un territorio más cercano a África es Canadá ", " Falso", false  , false));
            Geografia.add(new Question(4, "Las Tundras tienen climas fríos-polares ", "Verdadero", true  , false));
            MediaGeografia.add(new Question(0, "Arabia Saudí es el país que tiene más ríos y lagos de su continente ", "Falso", false    , false));
            MediaGeografia.add(new Question(1, "Uruguay es el país latinoamericano hispanohablante más pequeño ", "Falso", false    , false));
            MediaGeografia.add(new Question(2, "Durante dos horas cada día en el planeta Tierra conviven cuatro fechas del calendario diferentes 	", "Falso", false  , false));
            MediaGeografia.add(new Question(3, "el país hispanohablante más extenso es Argentina ", "Verdadero", true  , false));
            MediaGeografia.add(new Question(4, "Estados unidos tiene 51 estados y un distrito federal.", "Falso ", false  , false));
            DificilGeografia.add(new Question(0, "La capital de Sudafrica es Johannesburgo	", "Verdadero", true   , false));
            DificilGeografia.add(new Question(1, "México es el país numero 13° en superficie territorial ", "Verdadero", true   , false));
            DificilGeografia.add(new Question(2, "El Mar Amarillo se encuentra en el continente Norteamérica ", "Falso", false , false));
            DificilGeografia.add(new Question(3, "El país con litoral que sufre la mayor desproporción entre su tamaño y su longitud costera es la República Democrática del Congo. 2,3 millones de kilómetros cuadrados y apenas 37 kilómetros de costa", "Verdadero", true , false));
            DificilGeografia.add(new Question(4, "Alaska es el estado de EE.UU. más septentrional, más occidental y más oriental, simultáneamente. También es el estado de EEUU más cercano a Somalia", "Verdadero", true , false));



            if (DIFICULTAD == 10){
                if (HISTORIAVALOR == 20){
                    listaTodos.addAll(Historia);
                }
                if (ARTEAVALOR == 21){
                    listaTodos.addAll(Arte);
                }
                if (DEPORTESVALOR == 22){
                    listaTodos.addAll(Deportes);
                }
                if (FISICAVALOR == 23){
                    listaTodos.addAll(Fisica);
                }
                if (GEOGRAFIAVALOR == 24){
                    listaTodos.addAll(Geografia);
                }
                if (ANIMSLESVALOR == 25){
                    listaTodos.addAll(Animales);
                }
                if ( TODOSVALOR== 26){
                    listaTodos.addAll(Arte);
                    listaTodos.addAll(Animales);
                    listaTodos.addAll(Geografia);
                    listaTodos.addAll(Fisica);
                    listaTodos.addAll(Deportes);
                    listaTodos.addAll(Historia);
                }
            }else if (DIFICULTAD == 50){
                if (HISTORIAVALOR == 20){
                    listaTodos.addAll(MediaHistoria);
                }
                if (ARTEAVALOR == 21){
                    listaTodos.addAll(MediaArte);
                }
                if (DEPORTESVALOR == 22){
                    listaTodos.addAll(MediaDeportes);
                }
                if (FISICAVALOR == 23){
                    listaTodos.addAll(MediaFisica);
                }
                if (GEOGRAFIAVALOR == 24){
                    listaTodos.addAll(MediaGeografia);
                }
                if (ANIMSLESVALOR == 25){
                    listaTodos.addAll(MediaAnimales);
                }
                if ( TODOSVALOR== 26){
                    listaTodos.addAll(MediaArte);
                    listaTodos.addAll(MediaAnimales);
                    listaTodos.addAll(MediaGeografia);
                    listaTodos.addAll(MediaFisica);
                    listaTodos.addAll(MediaDeportes);
                    listaTodos.addAll(MediaHistoria);
                }
            }else  if (DIFICULTAD == 100){
                if (HISTORIAVALOR == 20){
                    listaTodos.addAll(DificilHistoria);
                }
                if (ARTEAVALOR == 21){
                    listaTodos.addAll(DificilArte);
                }
                if (DEPORTESVALOR == 22){
                    listaTodos.addAll(DificilDeportes);
                }
                if (FISICAVALOR == 23){
                    listaTodos.addAll(DificilFisica);
                }
                if (GEOGRAFIAVALOR == 24){
                    listaTodos.addAll(DificilGeografia);
                }
                if (ANIMSLESVALOR == 25){
                    listaTodos.addAll(DificilAnimales);
                }
                if ( TODOSVALOR== 26){
                    listaTodos.addAll(DificilArte);
                    listaTodos.addAll(DificilAnimales);
                    listaTodos.addAll(DificilGeografia);
                    listaTodos.addAll(DificilFisica);
                    listaTodos.addAll(DificilDeportes);
                    listaTodos.addAll(DificilHistoria);
                }
            }
            Collections.shuffle(listaTodos);

        }
    }


    public  void getValores(){
        listaTodos.clear();
        HISTORIAVALOR = OptionsActivity.HISTORIAVALOR;
        ARTEAVALOR = OptionsActivity.ARTEAVALOR;
        FISICAVALOR = OptionsActivity.FISICAVALOR;
        DEPORTESVALOR = OptionsActivity.DEPORTESVALOR;
        ANIMSLESVALOR = OptionsActivity.ANIMSLESVALOR;
        GEOGRAFIAVALOR = OptionsActivity.GEOGRAFIAVALOR;
        TODOSVALOR = OptionsActivity.TODOSVALOR;
        DIFICULTAD = OptionsActivity.DIFICULTAD;
    }



}


