package uady.hap.quizapppro;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class OptionsActivity extends AppCompatActivity {

    private TextView numbercheat_text;

    private Spinner number_cheats;
    private Spinner number_questions;

    private RadioGroup radioGroupDificultades;
    private RadioButton alto_radio;
    private RadioButton medio_radio;
    private RadioButton bajo_radio;

    private Switch switch_cheat;

    private  String cheats;
    private String showCheats ;
    private String NumberSelectedQuestions ;

    private CheckBox checkBoxTodos;
    private  Button buttonStartBack;
    private RelativeLayout cheatsLayoutOptions;

    String[] listCategorias;
    boolean[] checkedItems;
    ArrayList<Integer> listaPosicionesCategorias = new ArrayList<>(6);

   public   static  int HISTORIAVALOR;
   public   static  int ARTEAVALOR;
   public   static  int FISICAVALOR;
   public   static  int DEPORTESVALOR;
   public   static  int ANIMSLESVALOR;
   public   static  int GEOGRAFIAVALOR;
   public   static  int TODOSVALOR;
   public   static  int preguntaTotal;

    public static int DIFICULTAD;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        SharedPreferences.Editor editor = getSharedPreferences("CONFIGURATION",MODE_PRIVATE).edit();
        editor.clear();

        TODOSVALOR = 26;

        number_questions = findViewById(R.id.no_preguntas);
        number_cheats = findViewById(R.id.no_cheats);

        switch_cheat = findViewById(R.id.switch_cheat);
        numbercheat_text = findViewById(R.id.NumberCheat_text);

        checkBoxTodos = findViewById(R.id.todos_chechbox);
        buttonStartBack = findViewById(R.id.start_game);
        cheatsLayoutOptions = findViewById(R.id.options_cheats_layout);

        radioGroupDificultades = findViewById(R.id.radioGroup_dificultades);

        DIFICULTAD = 10;

            radioGroupDificultades.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.radio_bajo){
                        Toast.makeText(OptionsActivity.this, "Ha funcionado bajo", Toast.LENGTH_SHORT).show();
                        DIFICULTAD =10;
                    }else  if (checkedId == R.id.radio_media){
                        Toast.makeText(OptionsActivity.this, "Ha funcionado medio", Toast.LENGTH_SHORT).show();
                        DIFICULTAD= 50;
                    }else  if (checkedId == R.id.radio_alto){
                        Toast.makeText(OptionsActivity.this, "Ha funcionado alto", Toast.LENGTH_SHORT).show();
                        DIFICULTAD = 100;
                    }
                }
            });

        ArrayAdapter  questionsAdapter = ArrayAdapter.createFromResource(
                OptionsActivity.this,
                R.array.number_questions,
                R.layout.array_adapter
        );
        number_questions.setAdapter(questionsAdapter);

        ArrayAdapter cheatsAdapter = ArrayAdapter.createFromResource(
                OptionsActivity.this,
                R.array.number_cheats,
                R.layout.array_adapter
        );

        number_cheats.setAdapter(cheatsAdapter);
        showCheats = "DONT_SHOW_CHEATS";

        listCategorias = getResources().getStringArray(R.array.categorias_questions);
        checkedItems = new boolean[listCategorias.length];
        checkBoxTodos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        final int aIndex = 0;
        final int bIndex = 1;
        final int cIndex = 2;
        final int dIndex = 3;
        final int eIndex = 4;
        final int fIndex = 5;

        final CharSequence[] items = {"Historia " ,
                "Arte     " ,
                "Deportes " ,
                "Fisica   " ,
                "Geografia" ,
                "Animales "};

        final Boolean[] state = new Boolean[listCategorias.length];
        state[aIndex] = false;
        state[bIndex] = false;
        state[cIndex] = false;
        state[dIndex] = false;
        state[eIndex] = false;
        state[fIndex] = false;


        checkBoxTodos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(OptionsActivity.this);
                    builder.setTitle("Confirmar");
                    builder.setMultiChoiceItems(items, new boolean[]{false, false, false, false, false, false},
                            new DialogInterface.OnMultiChoiceClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                    state[which] = isChecked;
                                    if (isChecked) {
                                        if (!listaPosicionesCategorias.contains(which)) {
                                            listaPosicionesCategorias.add(which);
                                        }
                                    } else if (listaPosicionesCategorias.contains(which)) {
                                        listaPosicionesCategorias.remove((Integer) which);
                                    }

                                    if (state[0]) {
                                        Toast.makeText(OptionsActivity.this, "Ha seleccionado Historia", Toast.LENGTH_SHORT).show();
                                        HISTORIAVALOR = 20;
                                    }
                                    if (state[1]) {
                                        Toast.makeText(OptionsActivity.this, "Ha seleccionado Arte", Toast.LENGTH_SHORT).show();
                                        ARTEAVALOR = 21;
                                    }
                                    if (state[2]) {
                                        Toast.makeText(OptionsActivity.this, "Ha seleccionado Deporte", Toast.LENGTH_SHORT).show();
                                        DEPORTESVALOR = 22;
                                    }
                                    if (state[3]) {
                                        Toast.makeText(OptionsActivity.this, "Ha seleccionado Fisica", Toast.LENGTH_SHORT).show();
                                        FISICAVALOR = 23;
                                    }
                                    if (state[4]) {
                                        Toast.makeText(OptionsActivity.this, "Ha seleccionado Geografia", Toast.LENGTH_SHORT).show();
                                        GEOGRAFIAVALOR = 24;
                                    }
                                    if (state[5]) {
                                        Toast.makeText(OptionsActivity.this, "Ha seleccionado Animales", Toast.LENGTH_SHORT).show();
                                        ANIMSLESVALOR = 25;
                                    }
                                }
                            });
                    builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TODOSVALOR = 50;
                            if (listaPosicionesCategorias.size() <= 2) {
                                buttonStartBack.setEnabled(false);
                                buttonStartBack.setText("Disable");
                            } else {
                                buttonStartBack.setEnabled(true);
                                buttonStartBack.setText("Confirmar");
                            }



                        }
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                } else if (isChecked) {
                    buttonStartBack.setText("Confirmar");
                    buttonStartBack.setEnabled(true);
                }
            }
        });

        switch_cheat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(switch_cheat.isChecked()) {
                    cheatsLayoutOptions.setVisibility(View.VISIBLE);
                    showCheats= "SHOW_CHEATS";
                }
                else {
                    cheatsLayoutOptions.setVisibility(View.GONE);
                }
            }
        });

        number_questions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                NumberSelectedQuestions = parent.getItemAtPosition(position).toString();
                preguntaTotal = Integer.parseInt(NumberSelectedQuestions);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                NumberSelectedQuestions = "15";
            }
        });


        number_cheats.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                  cheats = parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                cheats = "0";
            }
        });

        buttonStartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(OptionsActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
                SharedPreferences.Editor editor = getSharedPreferences("CONFIGURATION",MODE_PRIVATE).edit();
                editor.putString("datoNumeroCheats",cheats);
                editor.putString("datoMostrarCheats",showCheats);
                editor.putString("datoNumeropreguntas", NumberSelectedQuestions);
                editor.apply();
            }
        });
    }
}



