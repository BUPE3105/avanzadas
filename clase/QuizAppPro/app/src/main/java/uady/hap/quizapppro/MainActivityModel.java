package uady.hap.quizapppro;

import android.arch.lifecycle.ViewModel;

import java.util.Collections;
import java.util.List;

import uady.hap.quizapppro.ListasPreguntas.Question;
import uady.hap.quizapppro.ListasPreguntas.QuestionList;

public class MainActivityModel extends ViewModel {

    private QuestionList questionBank;
    private List<Question> gameQuestions;
    private  int currentQuestionIndex;
    private int resultado;
    private int trampa;
    private int puntaje;


    public  int getCurrentQuestionIndex (){
        return  currentQuestionIndex;
    }

    public  void  incrementCurrentQuestionIndex(){
        currentQuestionIndex = (currentQuestionIndex + 1) % gameQuestions.size();
    }

    public  void  decrementCurrentQuestionIndex(){
        currentQuestionIndex = (currentQuestionIndex == 0) ? gameQuestions.size()-1: currentQuestionIndex-1;
    }

    public  void  loadGameQuestion(){

        if (questionBank == null) {
            currentQuestionIndex = 0;
            questionBank = new QuestionList();
            gameQuestions = questionBank.getFinallist();
        }
    }



    public Question getCurrentQuestion() {
        return gameQuestions.get(currentQuestionIndex);
    }

    public Question getNextQuestion() {
        incrementCurrentQuestionIndex();
        return gameQuestions.get(currentQuestionIndex);
    }

    public Question getPreviousQuestion() {
        decrementCurrentQuestionIndex();
        return gameQuestions.get(currentQuestionIndex);
    }

    public int getSiguienteNumeroPregunta() {
        return resultado;
    }

    public void setSiguienteResultado(int resultado) {
        this.resultado = resultado;
    }


    public int getAnteriorNumeroPregunta() {
        return resultado;
    }

    public void setAnteriorResultado(int resultado) {
        this.resultado =  resultado;
    }

    public void setSiguienteTrampa(int trampa){
       this.trampa = trampa;
    }

    public int getTrampa(){
        return  trampa;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }
}
