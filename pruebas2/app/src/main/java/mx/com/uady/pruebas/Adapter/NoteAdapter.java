package mx.com.uady.pruebas.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.AsyncDifferConfig;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mx.com.uady.pruebas.DB.Note;
import mx.com.uady.pruebas.R;

public class NoteAdapter extends ListAdapter<Note,NoteAdapter.NoteHolder> {

    private OnItemClicklistener clicklistener;

    public NoteAdapter() {
        super(DIFF_CALLBACK);
    }

    private  static final DiffUtil.ItemCallback<Note> DIFF_CALLBACK = new DiffUtil.ItemCallback<Note>() {
        @Override
        public boolean areItemsTheSame(@NonNull Note oldItem, @NonNull Note newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Note oldItem, @NonNull Note newItem) {
            return oldItem.getTitle().equals(newItem.getTitle()) &&
                    oldItem.getDescription().equals(newItem.getDescription()) &&
                    oldItem.getPriority() == newItem.getPriority();
        }
    };

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_item, parent, false);


        return new NoteHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder noteHolder, int position) {

        Note currentNote = getItem(position);
        noteHolder.textView_Title.setText(currentNote.getTitle());
        noteHolder.textView_Priority.setText(String.valueOf(currentNote.getPriority()));
        noteHolder.textView_Description.setText(currentNote.getDescription());

    }

    public Note getNoteAt(int position) {
        return getItem(position);
    }

    class NoteHolder extends RecyclerView.ViewHolder {

        private TextView textView_Title;
        private TextView textView_Description;
        private TextView textView_Priority;

        public NoteHolder(View itemView) {
            super(itemView);

            textView_Title = itemView.findViewById(R.id.note_title);
            textView_Description = itemView.findViewById(R.id.note_description);
            textView_Priority = itemView.findViewById(R.id.note_priority);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (clicklistener != null && position != RecyclerView.NO_POSITION) {
                        clicklistener.onItemClick(getItem(position));
                    }
                }
            });
        }

    }

    public interface OnItemClicklistener {
        void onItemClick(Note note);

    }

    public void setonItemClickListner(OnItemClicklistener clicklistener) {
        this.clicklistener = clicklistener;
    }
}
