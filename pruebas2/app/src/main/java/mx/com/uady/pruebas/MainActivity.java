package mx.com.uady.pruebas;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.List;

import mx.com.uady.pruebas.Adapter.NoteAdapter;
import mx.com.uady.pruebas.DB.Note;
import mx.com.uady.pruebas.ViewModel.NoteViewModel;

public class MainActivity extends AppCompatActivity {

    private NoteViewModel noteViewModel;
    public static final int ADD_NOTE_REQUEST = 1;
    public static final int EDDIT_NOTE_REQUEST = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton floatingActionButtonAddNote = findViewById(R.id.button_add_note);
        floatingActionButtonAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddEdditNotesActivity.class);
                startActivityForResult(intent, ADD_NOTE_REQUEST);


            }
        });

        RecyclerView recyclerView = findViewById(R.id.recyclerView_notes);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final NoteAdapter adapter = new NoteAdapter();
        recyclerView.setAdapter(adapter);

        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
        noteViewModel.getAllNotes().observe(this, new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> notes) {

                adapter.submitList(notes);

            }
        });


        Stetho.initializeWithDefaults(this);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT
        ) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

                noteViewModel.delete(adapter.getNoteAt(viewHolder.getAdapterPosition()));
                adapter.getNoteAt(viewHolder.getAdapterPosition()).getDescription();

                Toast.makeText(MainActivity.this, "Nota eliminada", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);

        adapter.setonItemClickListner(new NoteAdapter.OnItemClicklistener() {
            @Override
            public void onItemClick(Note note) {
                Intent intent = new Intent(MainActivity.this, AddEdditNotesActivity.class);
                intent.putExtra(AddEdditNotesActivity.EXTRA_ID, note.getId());
                intent.putExtra(AddEdditNotesActivity.EXTRA_TITLE, note.getTitle());
                intent.putExtra(AddEdditNotesActivity.EXTRA_DESCRIPTION, note.getDescription());
                intent.putExtra(AddEdditNotesActivity.EXTRA_PRIORITY, note.getPriority());
                startActivityForResult(intent,EDDIT_NOTE_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_NOTE_REQUEST && resultCode == RESULT_OK){

            String title = data.getStringExtra(AddEdditNotesActivity.EXTRA_TITLE);
            String description = data.getStringExtra(AddEdditNotesActivity.EXTRA_DESCRIPTION);
            int priority = data.getIntExtra(AddEdditNotesActivity.EXTRA_PRIORITY, 1);

            Note note = new Note(title, description, priority);
            noteViewModel.insert(note);

            Toast.makeText(this, "Guardado correcto", Toast.LENGTH_SHORT).show();
        }else if (requestCode == EDDIT_NOTE_REQUEST && resultCode == RESULT_OK){
            int id = data.getIntExtra(AddEdditNotesActivity.EXTRA_ID, -1);

            if (id == -1){
                Toast.makeText(this, "No se puede Actualizar", Toast.LENGTH_SHORT).show();
                return;
            }
            String title = data.getStringExtra(AddEdditNotesActivity.EXTRA_TITLE);
            String description = data.getStringExtra(AddEdditNotesActivity.EXTRA_DESCRIPTION);
            int priority = data.getIntExtra(AddEdditNotesActivity.EXTRA_PRIORITY, 1);

            Note note = new Note(title, description, priority);
            note.setId(id);
            noteViewModel.update(note);
            Toast.makeText(this, "Guardado correcto", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, "Guardado incorrecto", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.mainactivity_acttion_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_all_notes:
                noteViewModel.deleteAll();

                Toast.makeText(this, "Todas las notas han sido eliminadas", Toast.LENGTH_SHORT).show();


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
