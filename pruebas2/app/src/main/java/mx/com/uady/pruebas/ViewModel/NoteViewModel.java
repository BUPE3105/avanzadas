package mx.com.uady.pruebas.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;


import mx.com.uady.pruebas.DB.Note;
import mx.com.uady.pruebas.DB.NoteRepository;

public class NoteViewModel extends AndroidViewModel {

    private NoteRepository noteRepository;
    private LiveData<List<Note>> allNotes;

    public NoteViewModel(@NonNull Application application) {
        super(application);

        noteRepository = new NoteRepository(application);
        allNotes = noteRepository.getAllNotes();

    }

    public  void  insert (Note note){
        noteRepository.insert(note);
    }
    public  void  delete (Note note){
        noteRepository.delete(note);
    }
    public  void  update (Note note){
        noteRepository.update(note);
    }
    public  void  deleteAll (){
        noteRepository.deleteAllNotes();
    }

    public  LiveData<List<Note>> getAllNotes(){
        return  allNotes;
    }

}
