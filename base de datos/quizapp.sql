﻿
DROP TABLE IF EXISTS [score_images];
DROP TABLE IF EXISTS [user_scores];
DROP TABLE IF EXISTS [game];
DROP TABLE IF EXISTS [question_bank];
DROP TABLE IF EXISTS [game_options];
DROP TABLE IF EXISTS [question_categories];
DROP TABLE IF EXISTS [game_difficulties];

CREATE TABLE [game_difficulties](
    [id] INTEGER PRIMARY KEY,
    [description] TEXT NOT NULL,
    [points] INTEGER NOT NULL,
    CHECK(points > 0));

CREATE TABLE [question_categories](
    [id] INTEGER PRIMARY KEY,
    [description] TEXT NOT NULL,
    [status] INTEGER NOT NULL DEFAULT 0,
    CHECK(status = 0 OR status = 1));

CREATE TABLE [game_options](
    [cheats_enabled] INTEGER NOT NULL,
    [num_of_cheats] INTEGER NOT NULL,
    [difficulty_id] INTEGER NOT NULL REFERENCES game_difficulties([id]),
    [num_of_questions] INTEGER NOT NULL,
    CHECK(cheats_enabled = 0 OR cheats_enabled = 1),
    CHECK(num_of_cheats >= 0),
    CHECK(num_of_questions >= 5));

CREATE TABLE [question_bank](
    [id] INTEGER PRIMARY KEY,
    [description] TEXT NOT NULL,
    [difficulty_id] INTEGER NOT NULL REFERENCES game_difficulties([id]),
    [category_id] INTEGER NOT NULL REFERENCES question_categories([id]),
    [answer] INTEGER NOT NULL,
    CHECK(answer = 0 OR answer = 1));

CREATE TABLE [game](
    [question_id] INTEGER PRIMARY KEY REFERENCES question_bank([id]),
    [answered] INTEGER NOT NULL,
    [cheated] INTEGER NOT NULL,
    [user_answer] INTEGER NOT NULL,
    CHECK(answered = 0 OR answered = 1),
    CHECK(cheated = 0 OR cheated = 1),
    CHECK(user_answer = 0 OR user_answer = 1));

CREATE TABLE [user_scores](
    [id] INTEGER PRIMARY KEY,
    [initials] TEXT NOT NULL,
    [cheated] INTEGER NOT NULL,
    [total_points] INTEGER NOT NULL,
    [difficulty_id] INTEGER NOT NULL REFERENCES game_difficulties([id]),
    CHECK(cheated = 0 OR cheated = 1),
    CHECK(total_points >= 0));

CREATE TABLE [score_images](
    [id] INTEGER PRIMARY KEY,
    [points] INTEGER NOT NULL,
    [src] TEXT NOT NULL,
    CHECK(points > 0));


-- **********
-- DATA
-- **********

INSERT INTO game_difficulties (id, description, points) VALUES (1, 'Easy', 1);
INSERT INTO game_difficulties (id, description, points) VALUES (2, 'Normal', 2);
INSERT INTO game_difficulties (id, description, points) VALUES (3, 'Hard', 3);

INSERT INTO question_categories (id, description) VALUES (1, 'Videojuegos');
INSERT INTO question_categories (id, description) VALUES (2, 'Tecnología');
INSERT INTO question_categories (id, description) VALUES (3, 'Ciencia');
INSERT INTO question_categories (id, description) VALUES (4, 'Historia');
INSERT INTO question_categories (id, description) VALUES (5, 'Deportes');
INSERT INTO question_categories (id, description) VALUES (6, 'Música');

INSERT INTO game_options (cheats_enabled, num_of_cheats, difficulty_id, num_of_questions)
       VALUES (0, 0, 1, 10);
       
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (0,  'El primer nombre pensado para el Pokémon Eevee fue Eon', 3, 1, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (2,  'Atari significa ''que una ficha o un grupo de fichas está en peligro de ser capturadas por tu oponente''', 3, 1, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (5,  'Fornite permite la conexión multiplataforma entre todas las consolas', 2, 1, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (6,  'La serie de videojuegos denominada “Halo” se centra en una guerra interestelar entre la humanidad y una alianza de alienígenas conocida como el ''Covennant''', 2, 1, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (10,  'Donkey Kong es una serie de videojuegos que presenta las aventuras de un personaje parecido a un mono', 1, 1, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (11,  'El objetivo del juego conocido como PAC-MAN es acumular tantos puntos como sea posible al evitar obstáculos en forma de bolas a través de un laberinto y comer fantasmas', 1, 1, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (13,  'Resident Evil es un juego de misterio que consiste en escapar de una nave espacial', 1, 1, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (14,  'Garen es pareja de lux en league of legends', 1, 1, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (15,  'La Unión Soviética fue encabezada por Joseph Stalin', 3, 2, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (16,  'El emperador Calígula nombró senador a su caballo', 3, 2, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (17,  'Enrique VIII se casó 8 veces', 3, 2, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (18,  'Juana de Arco participo en las guerras napoleónicas', 3, 2, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (19,  'El padre de Alejandro Magno se llamó Filipo II de Macedonia ', 3, 2, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (20,  'La Carta Magna fue originada en Inglaterra', 2, 2, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (21,  'La peste bubónica arrasó Europa en el siglo XIV ', 2, 2, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (22,  'Marco Polo fue el primer explorador occidental en llegar a ‘las indias’', 2, 2, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (23,  'En el antiguo Egipto se rasuraban las cejas cuando un perro moría', 2, 2, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (24,  'Julio César fue el primer emperador Romano', 2, 2, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (25,  'Cristóbal Colón descubrió América en 1592', 1, 2, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (26,  'Estados unidos se formó en un principio con 13 colonias inglesas', 1, 2, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (27,  'El alunizaje del Apolo 11 con el que se asegura que el hombre llegó a luna se produjo en 1968', 1, 2, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (28,  'Los mayas son una civilización indígena que se asentó durante su florecimiento en la actual península de Yucatán', 1, 2, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (29,  'Adolf Hitler nación en Suiza', 1, 2, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (30,  'Roy Orbison era conocido como The Big O', 3, 3, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (31,  '''Can´t help falling in love'' es una canción de Elvis Preley', 3, 3, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (32,  'Jimmy Page fundó la banda de rock llamada Black Sabbath', 3, 3, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (33,  'Madama Butterfly es una obra de Mozart', 3, 3, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (34,  'Bed of Roses es una canción de Bon Jovi', 3, 3, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (35,  'La exitosa banda sueca conocida como Europe aún sigue despertando emociones con su famosa canción ´The Final Countdown''', 2, 3, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (37,  'Freddie Mercury fue cantautor de la banda Scorpions', 2, 3, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (39,  'Bad Romance es una canción de Miley Cirus', 2, 3, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (41,  '‘Hey Jude’ es una canción original de la famosa banda británica conocida como The Beatles', 1, 3, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (42,  'MTV significa Music ‘Television’', 1, 3, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (44,  'Beyonce es considerada por muchos la reina del pop', 1, 3, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (45,  'La máxima velocidad en una conexión a internet aún no supera el Tb', 3, 4, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (46,  'Actualmente una Intel Core i9 contiene entre 1300 y 1600 millones de transistores', 3, 4, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (47,  'La UNIVAC I (Computadora Automática Universal I) fue la primera computadora comercial fabricada en Estados Unidos', 3, 4, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (48,  'El nokia DynaTAC 8000x fue el primer móvil que se vendió al público', 3, 4, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (49,  'El Falcon Heavy es el único cohete que ha despegado y aterrizado en una plataforma sin destruirse por completo', 3, 4, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (50,  'Tanzania es un país cuya mayor parte de su población actualmente no tiene acceso a Internet', 2, 4, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (51,  'Un virus tipo worm o gusano se trata de una aplicación que está camuflada dentro de otra que parece inofensiva', 2, 4, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (52,  'El auto Tesla de Elon Musk que envió al espacio se llama Tesla Roadster', 2, 4, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (53,  'El primer satélite artificial de la Tierra es conocido como el Sputnik 2', 2, 4, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (54,  'El LHC es el acelerador de partículas más grande y energético del mundo', 2, 4, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (56,  'Actualmente (en 2019) estamos en la quinta generación de computadoras', 1, 4, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (57,  'FPGA significa Field Programmable Gate Array', 1, 4, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (58,  'El dueño de la empresa Space X es Elon Musk', 1, 4, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (61,  'Yogi Berra es considerado por muchos como el padre del béisbol', 3, 5, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (62,  'El hilo rojo de la pelota de béisbol tiene 108 costuras', 3, 5, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (63,  'Michael Jordán es el máximo anotador de la historia de la NBA', 3, 5, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (64,  'Fernando Alonso ha ganado 2 veces el GP de Mónaco', 3, 5, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (66,  'En un juego profesional de la NBA no existen anotaciones de 3 puntos', 2, 5, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (67,  'Finlandia nunca ha celebrado un Gran premio de Fórmula 1', 2, 5, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (68,  'Los primeros juegos olímpicos se celebraron en 1904', 2, 5, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (69,  'La antorcha olímpica recorrerá 137 kilómetros antes de llegar a su cede', 2, 5, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (70,  'En futbol Soccer si un jugador defensor hace falta dentro del área chica de su propio equipo es gol', 1, 5, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (72,  'En la NBA juegan 28 equipos', 1, 5, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (73,  'Los juegos olímpicos de 2020 se celebrarán en Tokio', 1, 5, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (74,  'El Real Madrid se impone como el máximo ganador de Champions League de todos los tiempos', 1, 5, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (77,  'La aguja de una brújula en el polo sur apunta hacia el norte', 3, 6, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (78,  'Una jirafa toma más agua en febrero que en Marzo', 3, 6, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (79,  'Gregor Mendeliev es considerado el padre de la genética', 3, 6, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (80,  'La meiosis es un proceso en el cual la célula se divide para formar dos células hijas', 2, 6, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (81,  'Para los botánicos el tomate es una fruta ', 2, 6, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (82,  'La teoría de Darwin considera que todos los organismos descendemos del mismo ancestro', 2, 6, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (83,  'La columna más a la derecha de la tabla periódica está compuesto por metales de transición', 2, 6, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (84,  'Los Rayos X son un tipo de radiación que produce quemaduras', 2, 6, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (85,  'La partícula es el componente mínimo que forma a los seres vivos', 1, 6, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (86,  'Las ballenas respiran por pulmones', 1, 6, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (87,  'La gravedad es la fuerza física que la Tierra ejerce sobre los cuerpos hacia su centro', 1, 6, 1);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (88,  'Un miligramo son diez mil microgramos', 1, 6, 0);
INSERT INTO question_bank (id, description, difficulty_id, category_id, answer) VALUES (89,  'El sol es una estrella', 1, 6, 1);


-- **********
-- GAME SIMULATION
-- **********

-- Load game configuration on Options-Activity

SELECT *
FROM game_options;

SELECT *
FROM game_difficulties;

SELECT *
FROM question_categories;

UPDATE game_options
SET cheats_enabled = 1, num_of_cheats = 3, difficulty_id = 2, num_of_questions = 15;

UPDATE game_options
SET num_of_questions = num_of_questions + 1;

UPDATE question_categories
SET status = 1
WHERE id = 1;
--WHERE id = 1 OR id = 3;
--WHERE id IN (1, 3, 5, 6);

SELECT random()
ORDER BY random()

