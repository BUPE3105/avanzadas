package mx.com.uady.pc_shop.DB.Products;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import mx.com.uady.pc_shop.DB.Categories.Categories;

@Dao
public interface ProductsDao {

    @Query("SELECT * from products ORDER BY description Asc")
    LiveData<List<Products>> getAllProduct();

    @Insert
    void insert(Products products);

    @Query("DELETE FROM products")
    void deleteAll();

    @Query("Select * from products where description like (:query) and category_id = (:category) order by description Asc")
    LiveData<List<Products>> getByDescriptionAndCategoryId(String query, int category);

    @Query("Select * from products where description like (:query) order by description Asc ")
    LiveData<List<Products>> getByDescription(String query);

    @Query("Select * from products where category_id = (:category) order by description Asc")
    LiveData<List<Products>> getByCategoryId(int category);

    @Query("Select products.id, products.description, products.qty,products.price ,product_categories.description as categoria  from products \n" +
            "inner join product_categories   on (product_categories.id = products.category_id)\n" +
            "where products.id =(:id)")
    LiveData<ProductsAndCategories> ProductItem(int id);
}
