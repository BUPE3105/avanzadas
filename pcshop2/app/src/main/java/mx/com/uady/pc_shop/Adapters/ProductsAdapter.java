package mx.com.uady.pc_shop.Adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import mx.com.uady.pc_shop.DB.Products.Products;
import mx.com.uady.pc_shop.R;

public class ProductsAdapter extends ListAdapter<Products, ProductsAdapter.Holder> {

    private OnItemClicklistener mlistener;

    public ProductsAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<Products> DIFF_CALLBACK = new DiffUtil.ItemCallback<Products>() {
        @Override
        public boolean areItemsTheSame(@NonNull Products oldItem, @NonNull Products newItem) {
            return oldItem.getProductId() == newItem.getProductId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Products oldItem, @NonNull Products newItem) {
            return oldItem.getCategoryId() == (newItem.getCategoryId()) &&
                    oldItem.getDescription().equals(newItem.getDescription()) &&
                    oldItem.getPrice() == newItem.getPrice()
                    && oldItem.getQty() == newItem.getQty();
        }
    };


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_products
                , parent, false);
        return new Holder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Products current = getItem(position);
        holder.categories.setText(current.getDescription());
        int number = current.getPrice()/100;
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator('\'');
        symbols.setDecimalSeparator(',');

        DecimalFormat decimalFormat = new DecimalFormat("$ #,###.00", symbols);
        String precio  = decimalFormat.format(number);
        holder.precio.setText("Precio: "+precio);
        String stock = Integer.toString(current.getQty());
        int qty = current.getQty();
        if (qty<=0){
            holder.stock.setTextColor(Color.parseColor("#ff8f00"));
            holder.stock.setText("Stock: "+stock);
        }else {
            holder.stock.setText("Stock: "+stock);
            holder.stock.setTextColor(Color.parseColor("#80d8ff"));
        }

    }



    public Products getproductAt(int position) {
        return getItem(position);
    }


    class Holder extends RecyclerView.ViewHolder {

        private TextView categories;
        private TextView precio;
        private TextView stock;

        public Holder(@NonNull final View itemView) {
            super(itemView);
            categories = itemView.findViewById(R.id.recycler_text_view);
            precio = itemView.findViewById(R.id.price_item);
            stock = itemView.findViewById(R.id.stock_item);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mlistener != null && position != RecyclerView.NO_POSITION) {
                        mlistener.onItemClick(getItem(position), itemView);
                    }
                }
            });

        }
    }

    public interface OnItemClicklistener {
        void onItemClick(Products products, View view);

    }

    public void setOnItemClickListener(OnItemClicklistener clicklistener) {
        this.mlistener = clicklistener;
    }

}
