package mx.com.uady.pc_shop.DB.OrderStatus;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "order_status")
public class OrderStatus {


    @PrimaryKey
    @ColumnInfo(name = "id")
    private int orderstatusID;

    @ColumnInfo (name = "description")
    private String statusDescription;

    @ColumnInfo (name = "editable")
    private int statusEditable;

    @ColumnInfo (name ="previous")
    private String statusPrevious;

    @ColumnInfo (name = "next")
    private String statusNext;

    public OrderStatus(int orderstatusID, String statusDescription, int statusEditable, String statusPrevious, String statusNext) {
        this.orderstatusID = orderstatusID;
        this.statusDescription = statusDescription;
        this.statusEditable = statusEditable;
        this.statusPrevious = statusPrevious;
        this.statusNext = statusNext;
    }

    public int getOrderstatusID() {
        return orderstatusID;
    }

    public void setOrderstatusID(int orderstatusID) {
        this.orderstatusID = orderstatusID;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public int getStatusEditable() {
        return statusEditable;
    }

    public void setStatusEditable(int statusEditable) {
        this.statusEditable = statusEditable;
    }

    public String getStatusPrevious() {
        return statusPrevious;
    }

    public void setStatusPrevious(String statusPrevious) {
        this.statusPrevious = statusPrevious;
    }

    public String getStatusNext() {
        return statusNext;
    }

    public void setStatusNext(String statusNext) {
        this.statusNext = statusNext;
    }
}
