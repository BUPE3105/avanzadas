package mx.com.uady.pc_shop.Report;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.List;

import mx.com.uady.pc_shop.Adapters.AdapterMissingProducts;
import mx.com.uady.pc_shop.DB.OrderAssemblies.MissingProducts;
import mx.com.uady.pc_shop.R;
import mx.com.uady.pc_shop.ViewModel.AssembliesModel;

public class missingActivity extends AppCompatActivity {

    private AssembliesModel model;
    private RecyclerView missingProducts;
    private AdapterMissingProducts adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_missing);

        Toolbar toolbar= findViewById(R.id.toolbar_report_missing);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Productos faltantes");
        toolbar.setTitleTextColor(Color.WHITE);

        model = ViewModelProviders.of(this).get(AssembliesModel.class);

        setAdapter();

        model.getMissiproducts().observe(this, new Observer<List<MissingProducts>>() {
            @Override
            public void onChanged(@Nullable List<MissingProducts> missingProducts) {
                adapter.submitList(missingProducts);
            }
        });

    }

    void setAdapter() {
        adapter = new AdapterMissingProducts();

        missingProducts = findViewById(R.id.missing_recycler_view);
        missingProducts.setLayoutManager(new LinearLayoutManager(this));
        missingProducts.setAdapter(adapter);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
