package mx.com.uady.pc_shop.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.R;

public class ClientsAdapter extends ListAdapter<Clients, ClientsAdapter.Holder> {

    private ClientsAdapter.OnItemClicklistener mlistener;
    private OnLongItemClickListner listener;


    public ClientsAdapter() {
        super(DIFF_CALLBACK);
    }


    private static final DiffUtil.ItemCallback<Clients> DIFF_CALLBACK = new DiffUtil.ItemCallback<Clients>() {
        @Override
        public boolean areItemsTheSame(@NonNull Clients oldItem, @NonNull Clients newItem) {
            return oldItem.getClientId() == newItem.getClientId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Clients oldItem, @NonNull Clients newItem) {
            return oldItem.getClientId() == (newItem.getClientId()) &&
                    oldItem.getFirst_name().equals(newItem.getFirst_name()) &&
                    oldItem.getLast_name().equals(newItem.getLast_name())
                    && oldItem.getEmail().equals(newItem.getEmail())
                    && oldItem.getPhone1() .equals (newItem.getPhone1())
                    && oldItem.getPhone2().equals(newItem.getPhone2())
                    && oldItem.getPhone3().equals(newItem.getPhone3());
        }
    };

    @NonNull
    @Override
    public ClientsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_client
                , parent, false);
        return new ClientsAdapter.Holder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Clients current = getItem(position);
        holder.fullname.setText(current.getFirst_name() + " " + current.getLast_name());
        holder.number.setText("Number: "+ current.getPhone1() );
        holder.id.setText("Id: " + current.getClientId());

    }




    class Holder extends RecyclerView.ViewHolder {

        private TextView fullname;
        private TextView number;
        private TextView id;

        public Holder(@NonNull final View itemView) {
            super(itemView);
            fullname = itemView.findViewById(R.id.recycler_text_view_fullname);
            number = itemView.findViewById(R.id.phone_item);
            id = itemView.findViewById(R.id.id_item);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mlistener != null && position != RecyclerView.NO_POSITION) {
                        mlistener.onItemClick(getItem(position), itemView);
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.OnLongClick(getItem(position), itemView);
                    }
                    return true;
                }
            });

        }
    }

    public interface OnItemClicklistener {
        void onItemClick(Clients clients, View view);

    }

    public void setOnItemClickListener(OnItemClicklistener clicklistener) {
        this.mlistener = clicklistener;
    }
    public  interface  OnLongItemClickListner{
        void OnLongClick(Clients clients, View view);
    }

    public void setOnLongItemClickListner(OnLongItemClickListner Llistner){
        this.listener = Llistner;
    }
}
