package mx.com.uady.pc_shop.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import mx.com.uady.pc_shop.DB.Orders.OrdersAndCustomers;
import mx.com.uady.pc_shop.R;


public class OrdersAndClientsAdapter extends ListAdapter<OrdersAndCustomers, OrdersAndClientsAdapter.Holder> {


    public OrdersAndClientsAdapter() {
        super(DIFF_CALLBACK);
    }

    private OrdersAndClientsAdapter.OnItemClicklistener mlistener;
    private OrdersAndClientsAdapter.OnLongItemClickListner listener;


    private static final DiffUtil.ItemCallback<OrdersAndCustomers> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<OrdersAndCustomers>() {
                @Override
                public boolean areItemsTheSame(@NonNull OrdersAndCustomers oldItem, @NonNull OrdersAndCustomers newItem) {
                    return oldItem.getOrderId() == newItem.getOrderId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull OrdersAndCustomers oldItem, @NonNull OrdersAndCustomers newItem) {
                    return oldItem.getFirst_name().equals(newItem.getFirst_name()) &&
                            oldItem.getLast_name().equals(newItem.getLast_name()) &&
                            oldItem.getDate() == newItem.getDate() &&
                            oldItem.getQty_assemblies() == newItem.getQty_assemblies() &&
                            oldItem.getStatus().equals(newItem.getStatus()) &&
                            oldItem.getTotalprice() == newItem.getTotalprice();
                }
            };

    @NonNull
    @Override
    public OrdersAndClientsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_orders,
                parent,false);
        return new OrdersAndClientsAdapter.Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        OrdersAndCustomers current =  getItem(position);
        holder.first_name.setText(current.getFirst_name());
        holder.last_name.setText(current.getLast_name());
        holder.status.setText(current.getStatus());
        holder.date.setText(current.getDate());
        holder.total_cost.setText(current.getTotalprice());
        holder.qty.setText(current.getQty_assemblies());

    }

    class Holder extends RecyclerView.ViewHolder{
        private TextView first_name;
        private TextView last_name;
        private TextView status;
        private TextView date;
        private TextView qty;
        private TextView total_cost;


        public Holder(@NonNull final View itemView) {
            super(itemView);
            first_name = itemView.findViewById(R.id.order_first);
            last_name = itemView.findViewById(R.id.order_last);
            status = itemView.findViewById(R.id.order_status);
            date = itemView.findViewById(R.id.order_date);
            total_cost = itemView.findViewById(R.id.order_totalcost);
            qty = itemView.findViewById(R.id.order_qty);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mlistener != null && position != RecyclerView.NO_POSITION) {
                        mlistener.onItemClick(getItem(position), itemView);
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.OnLongClick(getItem(position), itemView);
                    }
                    return true;
                }
            });

        }
    }


    public interface OnItemClicklistener {
        void onItemClick(OrdersAndCustomers ordersAndCustomers, View view);

    }

    public  interface  OnLongItemClickListner{
        void OnLongClick(OrdersAndCustomers ordersAndCustomers, View view);
    }

    public void setOnLongItemClickListner(OrdersAndClientsAdapter.OnLongItemClickListner Llistner){
        this.listener = Llistner;
    }

    public void setOnItemClickListener(OrdersAndClientsAdapter.OnItemClicklistener clicklistener) {
        this.mlistener = clicklistener;
    }
}
