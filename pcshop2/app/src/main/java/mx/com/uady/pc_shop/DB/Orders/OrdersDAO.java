package mx.com.uady.pc_shop.DB.Orders;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface OrdersDAO {

    @Query("SELECT * FROM orders ORDER BY date ")
    LiveData<List<Orders>> getAllOrders();

    @Query("DELETE FROM orders")
    void deleteAllOrders();


    @Query("SELECT * FROM orders WHERE id = (:id) ORDER BY date")
    LiveData<List<Orders>> getOrdersById(int id);

    @Query("SELECT o.id,c.first_name, c.last_name, os.description as status, o.date, tmp1.qty as qty_assemblies, tmp1.suma as totalprice"+
            " FROM orders o"+
            " INNER JOIN clients c ON (o.customer_id = c.id)"+
            " INNER JOIN order_status os ON (o.status_id = os.id)"+
            " INNER JOIN (\n" +
            "           SELECT oa.id, sum(oa.qty) as qty, tmp2.suma"+
            " FROM order_assemblies oa \n" +
            "           INNER JOIN (\n" +
            "                 SELECT ap.id, sum(p.price) as suma\n" +
            "                 FROM assembly_products ap\n" +
            "                 INNER JOIN products p ON (ap.product_id = p.id) GROUP BY ap.id) as tmp2 ON (oa.assembly_id = tmp2.id) GROUP BY oa.id )\n" +
            "as tmp1 ON ( o.id = tmp1.id) ORDER BY o.date DESC")
    LiveData<List<OrdersAndCustomers>> getOrdersByCostumers();


}
