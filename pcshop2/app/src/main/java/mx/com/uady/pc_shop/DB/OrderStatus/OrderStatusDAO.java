package mx.com.uady.pc_shop.DB.OrderStatus;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface OrderStatusDAO {


    @Query("SELECT * from order_status ORDER BY description Asc")
    LiveData<List<OrderStatus>> getAllOrderStatus();

    @Query("DELETE FROM order_status")
    void deleteAllOrderStatus();

    @Query("Select * from order_status where description like (:query) order by description Asc ")
    LiveData<List<OrderStatus>> getOrderStatusByDescription(String query);
}
