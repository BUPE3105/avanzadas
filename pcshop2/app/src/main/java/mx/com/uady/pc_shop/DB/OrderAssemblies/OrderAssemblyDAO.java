package mx.com.uady.pc_shop.DB.OrderAssemblies;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface OrderAssemblyDAO {

    @Query("SELECT  assemblies.description,products.description as products, products.qty as stock," +
            " sum (assembly_products.qty*order_assemblies.qty)-products.qty as qty  FROM order_assemblies " +
            "inner join assemblies on (order_assemblies.assembly_id = assemblies.id ) " +
            "inner join assembly_products  on ( assemblies.id = assembly_products.id)" +
            "inner join products  on (assembly_products.product_id = products.id)" +
            "where (products.qty < assembly_products.qty*order_assemblies.qty)" +
            "group by products.description " +
            "order by products.description ASc")
    LiveData<List<MissingProducts>> missingProducts();
}
