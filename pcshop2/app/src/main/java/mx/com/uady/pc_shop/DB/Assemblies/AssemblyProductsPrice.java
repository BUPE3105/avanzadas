package mx.com.uady.pc_shop.DB.Assemblies;

import android.arch.persistence.room.ColumnInfo;

public class AssemblyProductsPrice {
    private  int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String descripcion;
    @ColumnInfo(name = "precio")
    public int precio;
@ColumnInfo(name = "contador")
    public int contador;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }
}
