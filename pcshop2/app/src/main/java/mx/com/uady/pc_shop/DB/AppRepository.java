package mx.com.uady.pc_shop.DB;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

import mx.com.uady.pc_shop.DB.Assemblies.Assemblies;
import mx.com.uady.pc_shop.DB.Assemblies.AssembliesDao;
import mx.com.uady.pc_shop.DB.Assemblies.AssemblyProductsPrice;
import mx.com.uady.pc_shop.DB.AssembliesProducts.AssembliesAndProducts;
import mx.com.uady.pc_shop.DB.AssembliesProducts.AssembliesProductPrice;
import mx.com.uady.pc_shop.DB.AssembliesProducts.AssembliesProductsDao;
import mx.com.uady.pc_shop.DB.Clients.ClientDAO;
import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.DB.Clients.ClientsCheckBox;
import mx.com.uady.pc_shop.DB.Clients.ClientsCheckBoxDAO;
import mx.com.uady.pc_shop.DB.OrderAssemblies.MissingProducts;
import mx.com.uady.pc_shop.DB.Categories.Categories;
import mx.com.uady.pc_shop.DB.Categories.CategoriesDAO;
import mx.com.uady.pc_shop.DB.OrderAssemblies.OrderAssemblies;
import mx.com.uady.pc_shop.DB.OrderAssemblies.OrderAssemblyDAO;
import mx.com.uady.pc_shop.DB.Products.Products;
import mx.com.uady.pc_shop.DB.Products.ProductsAndCategories;
import mx.com.uady.pc_shop.DB.Products.ProductsDao;

public class AppRepository {

    private CategoriesDAO categoriesDAO;
    private LiveData<List<Categories>> getAllCategorias;

    private ProductsDao productsDao;
    private LiveData<List<Products>> getALLProducts;

    private ClientDAO clientDAO;
    private LiveData<List<Clients>>getAllClients;

    private ClientsCheckBoxDAO clientsCheckBoxDAO;
    private LiveData<List<ClientsCheckBox>>getAllClientsCheckBox;

    private AssembliesDao assembliesDao;

    private AssembliesProductsDao assembliesProductsDao;

    private OrderAssemblyDAO orderAssemblyDAO;
    public AppRepository(Application application) {
        AppDatabase db = AppDatabase.getInstance(application);
        productsDao = db.productsDao();
        categoriesDAO = db.categoriesDAO();
        assembliesDao = db.assembliesDao();
        assembliesProductsDao = db.assembliesProductsDao();
        orderAssemblyDAO = db.orderAssemblyDAO();
        clientDAO = db.clientDAO();
        clientsCheckBoxDAO = db.clientsCheckBoxDAO();

        getAllClientsCheckBox = clientsCheckBoxDAO.getAllClientsCheckBox();
        getAllClients = clientDAO.getAllClients();
        getALLProducts = productsDao.getAllProduct();
        getAllCategorias = categoriesDAO.getAllProductCategories();

    }

    public LiveData<List<Clients>>getAllClients(){return getAllClients;}
    public LiveData<List<ClientsCheckBox>>getGetAllClientsCheckBox(){return getAllClientsCheckBox;}

    public LiveData<List<Categories>> getAllCategorias() {
        return getAllCategorias;
    }

    public LiveData<List<Products>> getALLProducts() {
        return getALLProducts;
    }

    public LiveData<List<Products>> getByCategoryId(int categoryId) {
        return productsDao.getByCategoryId(categoryId);
    }

    public LiveData<List<Products>> getByDescription(String description) {
        return productsDao.getByDescription(description);
    }

    public LiveData<List<Products>> getByDescriptionAndCategoryId(String description, int categoryId) {
        return productsDao.getByDescriptionAndCategoryId(description, categoryId);
    }

    public LiveData<List<ClientsCheckBox>>getAllCLientCheckboxById(Integer id)
    {return clientsCheckBoxDAO.getClientCheckBoxById(id);}

    public LiveData<Clients>getClientsById(int id){
        return clientDAO.getClientById(id);
    }
    public LiveData<List<Clients>>getClientsByFirstName(String first_name){
        return clientDAO.getClientsByFirstName(first_name);
    }
    public LiveData<List<Clients>>getClientsByLastName(String last_name){
        return clientDAO.getClientsByLastName(last_name);
    }
    public LiveData<List<Clients>>getClientsByEmail(String email){
        return clientDAO.getClientsByemail(email);
    }

    public LiveData<List<Clients>> getClientsByNumber(String phone) {
        return clientDAO.getClientsByphone(phone);
    }

    public LiveData<ProductsAndCategories> getproductAndCategory(int id) {
        return productsDao.ProductItem(id);
    }



    public LiveData<List<AssembliesAndProducts>> getProductsAndQty(int id) {
        return assembliesProductsDao.findProductsAndQty(id);
    }

    public LiveData<AssembliesProductPrice> getPriceTotal(int id) {
        return assembliesProductsDao.findPrice(id);
    }

    public LiveData<List<Assemblies>> getAssembliesDescription(String query) {
        return assembliesDao.getByDescription(query);
    }

    public  LiveData<List<MissingProducts>> getMissingProducts(){
        return orderAssemblyDAO.missingProducts();
    }
    public  LiveData<List<AssemblyProductsPrice>> getDescriptionAndPrice(){
        return assembliesDao.DescriptionAndPrice();
    }
    public  LiveData<List<AssemblyProductsPrice>> getDescriptionAndPriceByString(String query){
        return assembliesDao.DescriptionAndPriceByString(query);
    }



    public void nuke() {
        new AppRepository.Nuke(categoriesDAO).execute();
    }
    private static class Nuke extends AsyncTask<Void, Void, Void> {
        private CategoriesDAO categoriesDAO;

        Nuke(CategoriesDAO Dao) {
            categoriesDAO = Dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            categoriesDAO.deleteAll();

            return null;
        }
    }

    public void NukeProducts() {
        new AppRepository.NukeProducts(productsDao).execute();
    }

    private static class NukeProducts extends AsyncTask<Void, Void, Void> {
        private ProductsDao productsDao;

        NukeProducts(ProductsDao Dao) {
            productsDao = Dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            productsDao.deleteAll();

            return null;
        }
    }
}
