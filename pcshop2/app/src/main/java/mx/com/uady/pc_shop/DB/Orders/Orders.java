package mx.com.uady.pc_shop.DB.Orders;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;



import java.io.Serializable;

import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.DB.OrderStatus.OrderStatus;

@Entity (tableName = "orders", foreignKeys = {@ForeignKey(entity = Clients.class,
                                                            parentColumns = "id",
                                                            childColumns = "customer_id"),
                                            @ForeignKey(entity = OrderStatus.class,
                                                            parentColumns = "id",
                                                            childColumns = "status_id")},
                                                        indices = {@Index(value = {"customer_id", "status_id"}),
                                                        @Index(value = "status_id")})

public class Orders implements Serializable {


    @PrimaryKey (autoGenerate =  true)
    @ColumnInfo (name = "id")
    private int orderId;

    @ColumnInfo (name = "status_id")
    private int statusId;

    @ColumnInfo (name = "customer_id")
    private int customerID;

    @ColumnInfo (name = "date")
    private int date;

    @ColumnInfo (name = "changelog")
    private String changelog;

    public Orders(int orderId, int statusId, int customerID, int date, String changelog) {
        this.orderId = orderId;
        this.statusId = statusId;
        this.customerID = customerID;
        this.date = date;
        this.changelog = changelog;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getStatusId() {
        return statusId;
    }

    public int getCustomerID() {
        return customerID;
    }

    public int getDate() {
        return date;
    }

    public String getChangelog() {
        return changelog;
    }


    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setChangelog(String changelog) {
        this.changelog = changelog;
    }
}
