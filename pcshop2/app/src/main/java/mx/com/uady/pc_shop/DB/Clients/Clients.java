package mx.com.uady.pc_shop.DB.Clients;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "clients")
public class Clients {

    @PrimaryKey
    @ColumnInfo(name = "id")
    @NonNull
    private  int clientId;
    @ColumnInfo(name = "first_name")
    @NonNull
    private  String first_name;
    @ColumnInfo(name = "last_name")
    @NonNull
    private  String last_name;
    @ColumnInfo(name = "phone1")
    @NonNull
    private String phone1;
    @ColumnInfo(name = "phone2")
    private String phone2;
    @ColumnInfo(name = "phone3")
    private String phone3;
    @ColumnInfo(name = "email")
    @NonNull
    private String email;

    public Clients(int clientId, @NonNull String first_name, @NonNull String last_name, @NonNull String phone1, String phone2, String phone3, @NonNull String email) {
        this.clientId = clientId;
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.phone3 = phone3;
        this.email = email;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    @NonNull
    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(@NonNull String first_name) {
        this.first_name = first_name;
    }

    @NonNull
    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(@NonNull String last_name) {
        this.last_name = last_name;
    }

    @NonNull
    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(@NonNull String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getPhone3() {
        return phone3;
    }

    public void setPhone3(String phone3) {
        this.phone3 = phone3;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NonNull String email) {
        this.email = email;
    }
}


