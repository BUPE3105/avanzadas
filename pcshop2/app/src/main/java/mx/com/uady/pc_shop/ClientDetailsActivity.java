package mx.com.uady.pc_shop;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import org.fabiomsr.moneytextview.MoneyTextView;

import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.ViewModel.ClientModel;
import mx.com.uady.pc_shop.ViewModel.ProductModel;

public class ClientDetailsActivity extends AppCompatActivity {

    private EditText firstedit;
    private EditText lastedit;
    private EditText emailedit;
    private EditText phone1edit;
    private EditText phone2edit;
    private EditText phone3edit;
    private CheckBox phone2check;
    private CheckBox phone3check;
    private ClientModel model;
    public static final String EXTRA_ID = "mx.com.uady.EXTRA_CLIENTS_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_details);


        firstedit = findViewById(R.id.first_name_details);
        lastedit = findViewById(R.id.last_name_details);
        emailedit = findViewById(R.id.email_details);
        phone1edit = findViewById(R.id.phone1_details);
        phone2edit = findViewById(R.id.phone2_details);
        phone3edit = findViewById(R.id.phone3_details);




        model = ViewModelProviders.of(this).get(ClientModel.class);
        Intent intent = getIntent();
        if(intent.hasExtra(EXTRA_ID)){
            model.getClientById(intent.getIntExtra(EXTRA_ID, 0)).observe(this, new Observer<Clients>() {
                @Override
                public void onChanged(@Nullable Clients clients) {

                    firstedit.setText(clients.getFirst_name());
                    lastedit.setText(clients.getLast_name());
                    emailedit.setText(clients.getEmail());
                    phone1edit.setText(clients.getPhone1());
                    phone2edit.setText(clients.getPhone2());
                    phone3edit.setText(clients.getPhone3());

                }
            });


        }

    }
}
