package mx.com.uady.pc_shop;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.ViewModel.ClientModel;

public class EditClientActivity extends AppCompatActivity {

    private static String number2;
    private static  String number3;
    private static String email2;
    public static  int id1 ;
    public  static final String id = "id";
    public static final String first_name = "first";
    public static final String last_name = "last";
    public static final String email1 = "email";
    public static final String phone1 = "phone1";
    public static final String phone2 = "phone2";
    public static final String phone3 = "phone3";
    public static final String EXTRA_ID = "mx.com.uady.EXTRA_CLIENTS_ID";
    private EditText firstedit;
    private EditText lastedit;
    private EditText emailedit;
    private EditText phone1edit;
    private EditText phone2edit;
    private EditText phone3edit;
    private CheckBox phone2check;
    private CheckBox phone3check;
    private CheckBox emailcheck;
    private ClientModel model;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_client);
        Toolbar toolbar = findViewById(R.id.toolbar_edit_client);

        firstedit = findViewById(R.id.first_name_edit);
        lastedit = findViewById(R.id.last_name_edit);
        emailedit = findViewById(R.id.email_edit);
        phone1edit = findViewById(R.id.phone1_edit);
        phone2edit = findViewById(R.id.phone2_edit);
        phone3edit = findViewById(R.id.phone3_edit);
        phone2check = findViewById(R.id.phone2_check_Edit);
        phone3check = findViewById(R.id.phone3_check_Edit);
        emailcheck = findViewById(R.id.email_edit_checkbox);


        setTitle("Editar Cliente");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        phone2edit.setEnabled(false);
        phone3edit.setEnabled(false);
        emailedit.setEnabled(false);



        phone2check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    phone2edit.setEnabled(true);
                }else{

                    phone2edit.setEnabled(false);
                }
            }
        });
        phone3check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    phone3edit.setEnabled(true);
                }else{

                    phone3edit.setEnabled(false);
                }
            }
        });

        emailcheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    emailedit.setEnabled(true);
                }else{

                    emailedit.setEnabled(false);
                }
            }
        });
        model = ViewModelProviders.of(this).get(ClientModel.class);
        Intent intent = getIntent();
        if(intent.hasExtra(EXTRA_ID)){
            model.getClientById(intent.getIntExtra(EXTRA_ID, 0)).observe(this, new Observer<Clients>() {
                @Override
                public void onChanged(@Nullable Clients clients) {

                    firstedit.setText(clients.getFirst_name());
                    lastedit.setText(clients.getLast_name());
                    emailedit.setText(clients.getEmail());
                    phone1edit.setText(clients.getPhone1());
                    phone2edit.setText(clients.getPhone2());
                    phone3edit.setText(clients.getPhone3());
                    id1 = clients.getClientId();
                }
            });


        }

    }
    public void updateClient(View view){
        String updatedfirstname = firstedit.getText().toString();
        String updatedlasttname = lastedit.getText().toString();
        String updatedemail = emailedit.getText().toString();
        String updatedphone1 = phone1edit.getText().toString();
        String updatedphone2 = phone2edit.getText().toString();
        String updatedphone3= phone3edit.getText().toString();
        Intent resultIntent = new Intent();
        resultIntent.putExtra(id, id1);
        resultIntent.putExtra(first_name, updatedfirstname);
        resultIntent.putExtra(last_name, updatedlasttname);
        resultIntent.putExtra(email1, updatedemail);
        resultIntent.putExtra(phone1, updatedphone1);
        resultIntent.putExtra(phone2, updatedphone2);
        resultIntent.putExtra(phone3, updatedphone3);
        setResult(RESULT_OK, resultIntent);
        finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
