package mx.com.uady.pc_shop;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import mx.com.uady.pc_shop.Adapters.ClientsAdapter;
import mx.com.uady.pc_shop.Adapters.OrdersAndClientsAdapter;
import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.DB.Orders.Orders;
import mx.com.uady.pc_shop.DB.Orders.OrdersAndCustomers;
import mx.com.uady.pc_shop.ViewModel.ClientModel;

public class OrdersActivity extends AppCompatActivity {

    private static final int NEW_NOTE_ACTIVITY_REQUEST_CODE = 1;
    public static final int UPDATE_NOTE_ACTIVITY_REQUEST_CODE = 2;
    private MultiSelectionSpinner multiSpinner;
    private Spinner spinnerClients;
    private Spinner spinnerStatus;
    private EditText editText;
    private ClientModel model;
    private OrdersAndClientsAdapter adapter;
    private int clients_id;
    private int order_id;
    private String full_name_client;
    private RecyclerView recyclerView;
    private Editable Query;
    private static  String button;
    private static  String code;
    private FloatingActionButton add_button;
    private static boolean state1;
    private static boolean state2;
    private static boolean state3;
    private static boolean state4 ;
    private Intent intent;
    private Intent intent2;
    private Intent intent3;
    private Context mContext;
    private ClientActivity.OnLongItemClickListner listener;
    private List<OrdersAndCustomers> orders;
    private Orders order1;
    private CheckBox chk_dateFirst;
    private CheckBox chk_dateFinal;

    private int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setTitle("Ordenes");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setAdapter();
        spinnerClients = findViewById(R.id.client_choose);
        spinnerStatus = findViewById(R.id.status_choose);
        chk_dateFirst = findViewById(R.id.chk_1);
        chk_dateFinal = findViewById(R.id.chk_2);
        model = ViewModelProviders.of(this).get(ClientModel.class);




        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(OrdersActivity.this, AddClientActivity.class);
                startActivity(intent);

            }
        });

        Query = editText.getText();
        registerForContextMenu(recyclerView);



    }


    void setAdapter() {
        adapter = new OrdersAndClientsAdapter();

        recyclerView = findViewById(R.id.recyler_view_orders);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);


    }
}
