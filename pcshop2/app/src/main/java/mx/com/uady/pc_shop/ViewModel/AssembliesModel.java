package mx.com.uady.pc_shop.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import mx.com.uady.pc_shop.DB.AppRepository;
import mx.com.uady.pc_shop.DB.Assemblies.Assemblies;
import mx.com.uady.pc_shop.DB.Assemblies.AssemblyProductsPrice;
import mx.com.uady.pc_shop.DB.AssembliesProducts.AssembliesAndProducts;
import mx.com.uady.pc_shop.DB.AssembliesProducts.AssembliesProductPrice;
import mx.com.uady.pc_shop.DB.OrderAssemblies.MissingProducts;

public class AssembliesModel extends AndroidViewModel {

    private AppRepository assembliesRepository;
    private AppRepository assembliesProductsRepository;

    public AssembliesModel(@NonNull Application application) {
        super(application);
        assembliesRepository = new AppRepository(application);
        assembliesProductsRepository = new AppRepository(application);
    }

    public  LiveData<List<AssemblyProductsPrice>> getDescriptionAndPrice(){
        return assembliesProductsRepository.getDescriptionAndPrice();
    }
    public  LiveData<List<AssemblyProductsPrice>> getDescriptionAndPriceByString(String query){
        return assembliesProductsRepository.getDescriptionAndPriceByString(query);
    }


    public LiveData<List<AssembliesAndProducts>> getProductsAndQty(int id) {
        return assembliesProductsRepository.getProductsAndQty(id);
    }

    public LiveData<AssembliesProductPrice> getPriceTotal(int id) {
        return assembliesProductsRepository.getPriceTotal(id);
    }

    public LiveData<List<MissingProducts>> getMissiproducts(){
        return assembliesProductsRepository.getMissingProducts();
    }

}
