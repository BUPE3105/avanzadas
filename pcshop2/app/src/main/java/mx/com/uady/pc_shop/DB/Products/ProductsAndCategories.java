package mx.com.uady.pc_shop.DB.Products;

import android.arch.persistence.room.ColumnInfo;
import android.support.annotation.NonNull;

public class ProductsAndCategories {
    private  int id;
    private String description;
    private int qty;
    private  int price;
    @ColumnInfo(name = "categoria")
    private String categorias;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCategorias() {
        return categorias;
    }

    public void setCategorias(String categorias) {
        this.categorias = categorias;
    }

    @NonNull
    @Override
    public String toString() {
        return description;
    }
}
