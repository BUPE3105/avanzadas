package mx.com.uady.pc_shop.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;

import java.util.List;

import mx.com.uady.pc_shop.DB.AppDatabase;
import mx.com.uady.pc_shop.DB.AppRepository;
import mx.com.uady.pc_shop.DB.Clients.ClientDAO;
import mx.com.uady.pc_shop.DB.Clients.Clients;
import mx.com.uady.pc_shop.DB.Clients.ClientsCheckBox;

public class ClientModel extends AndroidViewModel {

    private AppRepository clientsrepository;
    private LiveData<List<Clients>> getAllClients;
    private LiveData<List<ClientsCheckBox>>getAlClientsCheckBox;
    private ClientDAO clientDAO;
    private AppDatabase ClientDB;


    public ClientModel(@NonNull Application application) {
        super(application);
        clientsrepository = new AppRepository(application);
        ClientDB = AppDatabase.getInstance(application);
        clientDAO = ClientDB.clientDAO();
        getAllClients = clientsrepository.getAllClients();
        getAlClientsCheckBox  = clientsrepository.getGetAllClientsCheckBox();
    }

    public void insert(Clients clients) { new InsertAsyncTask(clientDAO).execute(clients);
    }

    public void update(Clients clients) {
        new UpdateAsyncTask(clientDAO).execute(clients);
    }

    public void delete(Clients clients) {
        new DeleteAsyncTask(clientDAO).execute(clients);
    }

    public LiveData<Clients>getClientById(int  id){
        return  clientsrepository.getClientsById(id);
    }
    public  LiveData<List<Clients>> getAllClients(){
        return  getAllClients;
    }
    public LiveData<List<ClientsCheckBox>>getGetAlClientsCheckBox(){return getAlClientsCheckBox;}

    public LiveData<List<ClientsCheckBox>>getClientsCheckboxById(Integer Id)
    {return clientsrepository.getAllCLientCheckboxById(Id);}

    public LiveData<List<Clients>>getClientesByFirstName(String first_name){
        return clientsrepository.getClientsByFirstName(first_name);
    }
    public LiveData<List<Clients>>getClientesByLastName(String last_name){
        return clientsrepository.getClientsByLastName(last_name);
    }
    public LiveData<List<Clients>>getClientesByEmail(String email){
        return clientsrepository.getClientsByEmail(email);
    }
    public LiveData<List<Clients>>getCLientesByNumber(String number){
        return clientsrepository.getClientsByNumber(number);
    }


    private class OperationsAsyncTask extends AsyncTask<Clients, Void, Void> {

        ClientDAO mAsyncTaskDao;

        OperationsAsyncTask(ClientDAO dao) {
            this.mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Clients... clients) {
            return null;
        }
    }

    private class UpdateAsyncTask extends OperationsAsyncTask {

        UpdateAsyncTask(ClientDAO clientDAO) {
            super(clientDAO);
        }

        @Override
        protected Void doInBackground(Clients ... clients) {
            mAsyncTaskDao.update(clients[0]);
            return null;
        }
    }

    private class DeleteAsyncTask extends OperationsAsyncTask {

        public DeleteAsyncTask(ClientDAO clientDAO) {
            super(clientDAO);
        }

        @Override
        protected Void doInBackground(Clients... clients) {
            mAsyncTaskDao.delete(clients[0]);
            return null;
        }
    }
    private class InsertAsyncTask extends OperationsAsyncTask {

        InsertAsyncTask(ClientDAO clientDAO) {
            super(clientDAO);
        }

        @Override
        protected Void doInBackground(Clients... clients) {
            mAsyncTaskDao.insert(clients[0]);
            return null;
        }
    }


}
