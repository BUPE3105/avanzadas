package mx.com.uady.pc_shop.DB.Orders;


public class OrdersAndCustomers {

    private int orderId;
    private String first_name;
    private String last_name;
    private String status;
    private int date;
    private int qty_assemblies;
    private int totalprice;

    public int getOrderId(){return orderId;}

    public void setOrderId(int orderId){this.orderId = orderId;}

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getQty_assemblies() {
        return qty_assemblies;
    }

    public void setQty_assemblies(int qty_assemblies) {
        this.qty_assemblies = qty_assemblies;
    }

    public int getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(int totalprice) {
        this.totalprice = totalprice;
    }
}
